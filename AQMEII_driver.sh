#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# description
# ----------------------------------------------------------------------

# A top-level driver for creating an inventory of N2O emissions from agricultural soils over the AQMEII-NA domain by

# * 2D-regridding EDGAR data from global/unprojected to AQMEII-NA/LCC
# * overlaying EPIC data for CONUS
# * retemporalizing from monthly to hourly
# * output-ing in form consumable by CMAQ (hopefully)
# * ... with visualizations and conservation checks

# See https://bitbucket.org/epa_n2o_project_team/aqmeii_ag_soil

# * definitely

# ** bash: required to run this script. Known to work with bash --version==3.2.25

# * potentially initially. If you have not already retrieved regrid_utils (see `get_regrid_utils`), you will need

# ** git: to clone their repo
# ** web access (HTTP currently)

#   These must be available to the script when initially run.

# * ultimately

# ** basename, dirname
# ** cp
# ** curl or wget (latter {preferred, coded} for ability to deal with redirects)
# ** gunzip, unzip
# ** ncdump
# ** NCL
# ** R

# TODO: failing functions should fail this (entire) driver!

# Configure as needed for your platform.

# ----------------------------------------------------------------------
# constants with some simple manipulations
# ----------------------------------------------------------------------

# TODO: take switches for help, debugging, no/eval, target drive
THIS="$0"
THIS_FN="$(basename $0)"
THIS_DIR="$(dirname $0)"

### workspace
# note: following will fail if `source`ing!
export WORK_DIR="${THIS_DIR}" # where we expect to find stuff

### downloading
WGET_TO_FILE='wget --no-check-certificate -c -O'
WGET_TO_STREAM="${WGET_TO_FILE} -"
CURL_TO_FILE='curl -C - -o'
CURL_TO_STREAM='curl'

### model/inventory constants
export MODEL_YEAR='2008'
export MONTHS_PER_YEAR='12'
export HOURS_PER_DAY='24'
export SECONDS_PER_HOUR='3600'
export HOURS_PER_SECOND='2.778e-4' # 1/3600
## where this is hosted
PROJECT_WEBSITE='https://bitbucket.org/epa_n2o_project_team/aqmeii_ag_soil'
PROJECT_HISTORY="see ${PROJECT_WEBSITE}"
## canonical extensions
NETCDF_EXT_NCL='nc'   # NCL prefers the "canonical" netCDF extension
NETCDF_EXT_CMAQ='ncf' # CMAQ, as is so often the case, is nonstandard :-(
RDS_EXT='rds'

### numerical and math constants
export ZERO_MAX='0.0001' # any value less than this is probably 0
export ONE_MIN='0.9999'  # any value more than this is probably 1
export CMAQ_RADIUS='6370000' # of spherical earth, in meters
# pi, etc

### empirical constants
export MOLAR_MASS_N2O='44.0128' # grams per mole of N2O, per wolframalpha.com

### AQMEII-NA constants

## datavar dims and attributes
# TODO: get from template file or from EPIC
# "real" data
AQMEIINA_DV_NAME='N2O'
# IOAPI pads varattr=long_name to length=16 with trailing spaces
AQMEIINA_DV_LONG_NAME="$(printf '%-16s' ${AQMEIINA_DV_NAME})"
# IOAPI pads varattr=units to length=16 with trailing spaces
AQMEIINA_DV_UNITS="$(printf '%-16s' 'moles/s')"
# IOAPI pads varattr=var_desc to length=80 with trailing spaces
# Don't single-quote the payload: double-quote it (OK inside parens inside double-quotes)
AQMEIINA_DV_VAR_DESC="$(printf '%-80s' "Model species ${AQMEIINA_DV_NAME}")"

# "fake" datavar=TFLAG, required by IOAPI (and more to the point (IIUC), VERDI)
AQMEIINA_TFLAG_NAME='TFLAG'
# IOAPI pads varattr=long_name to length=16 with trailing spaces
AQMEIINA_TFLAG_LONG_NAME="$(printf '%-16s' ${AQMEIINA_TFLAG_NAME})"
# IOAPI pads varattr=units to length=16 with trailing spaces
AQMEIINA_TFLAG_UNITS="$(printf '%-16s' '<YYYYDDD,HHMMSS>')"
# IOAPI pads varattr=var_desc to length=80 with trailing spaces
AQMEIINA_TFLAG_VAR_DESC="$(printf '%-80s' 'Timestep-valid flags:  (1) YYYYDDD or (2) HHMMSS')"

AQMEIINA_DIM_LAYER_NAME='LAY'
AQMEIINA_DIM_LAYER_LONG_NAME='index of layers above surface'
AQMEIINA_DIM_LAYER_UNITS='unitless'

AQMEIINA_DIM_TSTEP_NAME='TSTEP'
AQMEIINA_DIM_TSTEP_UNITS='' # they vary!
AQMEIINA_DIM_TSTEP_LONG_NAME='timestep'

AQMEIINA_DIM_X_N='459'
AQMEIINA_DIM_X_NAME='COL'
# AQMEIINA_DIM_X_UNITS='unitless' # my invention
AQMEIINA_DIM_X_UNITS='m'
# AQMEIINA_DIM_X_LONG_NAME='Fortran-style index to grid columns from lower-left origin' # my invention (TODO: CHECK it's not from top)
AQMEIINA_DIM_X_LONG_NAME='grid-center offset from center of projection'

AQMEIINA_DIM_Y_N='299'
AQMEIINA_DIM_Y_NAME='ROW'
# AQMEIINA_DIM_Y_UNITS='unitless' # my invention
AQMEIINA_DIM_Y_UNITS='m'
# AQMEIINA_DIM_Y_LONG_NAME='Fortran-style index to grid columns from lower-left origin' # my invention (TODO: CHECK it's not from top)
AQMEIINA_DIM_Y_LONG_NAME="${AQMEIINA_DIM_X_LONG_NAME}"

# dimensions we don't really need, but VERDI does
AQMEIINA_DIM_DATETIME_NAME='DATE-TIME'
AQMEIINA_DIM_LAYER_NAME='LAY'
AQMEIINA_DIM_TSTEP_NAME='TSTEP'
AQMEIINA_DIM_VAR_NAME='VAR'

### artifact visualization

## for visualization (generally)
export OUTPUT_SIGNIFICANT_DIGITS='3' # see conservation report below
# PROJ.4 string for unprojected data
export GLOBAL_PROJ4='+proj=longlat +ellps=WGS84'

## for plotting (specifically)
# PDF_VIEWER='xpdf' # set this in bash_utilities::setup_apps
# temporally disaggregate multiple plots
DATE_FORMAT='%Y%m%d_%H%M'
export PDF_DIR="${WORK_DIR}"
# dimensions (for R plots--units?) for single-frame plots
export SINGLE_FRAME_PLOT_HEIGHT='10'
export SINGLE_FRAME_PLOT_WIDTH='15'
# dimensions for multi-frame plots
export TWELVE_MONTH_PLOT_HEIGHT='120'
export TWELVE_MONTH_PLOT_WIDTH='20'

## conservation report constants
# need 5 additional digits for float output, e.g., "4.85e+04"
# bash arithmetic gotcha: allow no spaces around '='!
# export CONSERV_REPORT_FIELD_WIDTH=$((OUTPUT_SIGNIFICANT_DIGITS + 5))
export CONSERV_REPORT_FIELD_WIDTH='9' # width of 'AQMEII-NA'
export CONSERV_REPORT_FLOAT_FORMAT="%${CONSERV_REPORT_FIELD_WIDTH}.$((OUTPUT_SIGNIFICANT_DIGITS - 1))e"
# echo -e "${THIS_FN}: CONSERV_REPORT_FLOAT_FORMAT=${CONSERV_REPORT_FLOAT_FORMAT}" # debugging
export CONSERV_REPORT_INT_FORMAT="%${CONSERV_REPORT_FIELD_WIDTH}i"
export CONSERV_REPORT_COLUMN_SEPARATOR="  "
export CONSERV_REPORT_TITLE="Is N2O conserved from input to output? units=kg N2O"
export CONSERV_REPORT_SUBTITLE="(note (US land area)/(earth land area) ~= 6.15e-02)"

# ----------------------------------------------------------------------
# helpers
# ----------------------------------------------------------------------

### helpers in this repo

export REUNIT_VIS_EDGAR_GLOBAL_FN='reunit_vis_EDGAR_global.r'
export REUNIT_VIS_EDGAR_GLOBAL_FP="${WORK_DIR}/${REUNIT_VIS_EDGAR_GLOBAL_FN}"
export REGRID_VIS_EDGAR_FN='regrid_vis.r'
export REGRID_VIS_EDGAR_FP="${WORK_DIR}/${REGRID_VIS_EDGAR_FN}"
export BELD_READ_FN='read_BELD_CSV.r'
export BELD_READ_FP="${WORK_DIR}/${BELD_READ_FN}"
export BELD_CONVERT_FN='BELD_CSV_to_RDS.r'
export BELD_CONVERT_FP="${WORK_DIR}/${BELD_CONVERT_FN}"
export DISTILL_EPIC_FN='distill_EPIC_emittivities.ncl'
export DISTILL_EPIC_FP="${WORK_DIR}/${DISTILL_EPIC_FN}"
export COMPUTE_EPIC_FN='compute_EPIC_emissions.r'
export COMPUTE_EPIC_FP="${WORK_DIR}/${COMPUTE_EPIC_FN}"
export COMBINE_EMIS_FN='combine_EDGAR_and_EPIC_emissions.r'
export COMBINE_EMIS_FP="${WORK_DIR}/${COMBINE_EMIS_FN}"
export MAKE_HOURLIES_FN='make_hourlies.ncl'
export MAKE_HOURLIES_FP="${WORK_DIR}/${MAKE_HOURLIES_FN}"
export CHECK_CONSERVATION_FN='check_conservation.ncl'
export CHECK_CONSERVATION_FP="${WORK_DIR}/${CHECK_CONSERVATION_FN}"

### helpers retrieved from elsewhere. TODO: create NCL and R packages

# path to a project, not a .git
REGRID_UTILS_URI='https://bitbucket.org/epa_n2o_project_team/regrid_utils'
REGRID_UTILS_PN="$(basename ${REGRID_UTILS_URI})" # project name
# can also use   'git@bitbucket.org:tlroche/regrid_utils' if supported
# path to a folder, not a file: needed by NCL to get to initial helpers
export REGRID_UTILS_DIR="${WORK_DIR}/${REGRID_UTILS_PN}" # folder, not file

export BASH_UTILS_FN='bash_utilities.sh'
# export BASH_UTILS_FP="${REGRID_UTILS_DIR}/${BASH_UTILS_FN}"
# no, allow user to override repo code: see `get_bash_utils`
export BASH_UTILS_FP="${WORK_DIR}/${BASH_UTILS_FN}"

export GET_AQMEIINA_AREAS_FN='get_output_areas.ncl'
export GET_AQMEIINA_AREAS_FP="${WORK_DIR}/${GET_AQMEIINA_AREAS_FN}"

export IOAPI_FUNCS_FN='IOAPI.ncl'
export IOAPI_FUNCS_FP="${WORK_DIR}/${IOAPI_FUNCS_FN}"

export R_UTILS_FN='R_utilities.r'
export R_UTILS_FP="${WORK_DIR}/${R_UTILS_FN}"

export STATS_FUNCS_FN='netCDF.stats.to.stdout.r'
export STATS_FUNCS_FP="${WORK_DIR}/${STATS_FUNCS_FN}"

export STRING_FUNCS_FN='string.ncl'
export STRING_FUNCS_FP="${WORK_DIR}/${STRING_FUNCS_FN}"

export SUMMARIZE_FUNCS_FN='summarize.ncl'
export SUMMARIZE_FUNCS_FP="${WORK_DIR}/${SUMMARIZE_FUNCS_FN}"

export TIME_FUNCS_FN='time.ncl'
export TIME_FUNCS_FP="${WORK_DIR}/${TIME_FUNCS_FN}"

export VIS_FUNCS_FN='visualization.r'
export VIS_FUNCS_FP="${WORK_DIR}/${VIS_FUNCS_FN}"

# ----------------------------------------------------------------------
# inputs
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# raw inputs
# ----------------------------------------------------------------------

### input: raw EDGAR global N2O emissions

AQMEII_EDGAR_N2O_GLOBAL_ZIP_HOMEPAGE='http://edgar.jrc.ec.europa.eu/datasets_grid_list.php?v=42&edgar_compound=N2O' # unfortunately require GUI manipulation, so instead use ...
AQMEII_EDGAR_N2O_GLOBAL_ZIP_URI='https://bitbucket.org/epa_n2o_project_team/aqmeii_ag_soil/downloads/v42_N2O_2008_IPCC_4C_4D.0.1x0.1.zip'
AQMEII_EDGAR_N2O_GLOBAL_ZIP_FN="$(basename ${AQMEII_EDGAR_N2O_GLOBAL_ZIP_URI})"
AQMEII_EDGAR_N2O_GLOBAL_FN_ROOT="${AQMEII_EDGAR_N2O_GLOBAL_ZIP_FN%.*}" # everything left of the LAST dot
AQMEII_EDGAR_N2O_GLOBAL_ZIP_FP="${WORK_DIR}/${AQMEII_EDGAR_N2O_GLOBAL_ZIP_FN}"
AQMEII_EDGAR_N2O_GLOBAL_FN="${AQMEII_EDGAR_N2O_GLOBAL_FN_ROOT}.${NETCDF_EXT_NCL}"
export AQMEII_EDGAR_N2O_GLOBAL_FP="${WORK_DIR}/${AQMEII_EDGAR_N2O_GLOBAL_FN}"
# do I want to rotate its longitudes? i.e. from 0-360 -> -180-+180
export AQMEII_EDGAR_N2O_GLOBAL_ROTATE_LONS='TRUE'
export AQMEII_EDGAR_N2O_GLOBAL_DV_NAME='emi_n2o' # emi_n2o(lat, lon)
export AQMEII_EDGAR_N2O_GLOBAL_DV_TYPE='FLT4S' # == float: see raster.pdf::dataType. TODO: get from file
export AQMEII_EDGAR_N2O_GLOBAL_DV_LONG_NAME='Emissions of N2O - ' # TODO: get from file
export AQMEII_EDGAR_N2O_GLOBAL_DV_COORD_X_NAME='lon' # TODO: get from file
export AQMEII_EDGAR_N2O_GLOBAL_DV_COORD_Y_NAME='lat' # TODO: get from file

### input: EDGAR gridcell areas

# v4.0 reused for subsequent versions
AQMEII_EDGAR_AREAS_GLOBAL_URI='http://edgar.jrc.ec.europa.eu/dataset/edgarv4_0/info/area_0.1x0.1.nc'
AQMEII_EDGAR_AREAS_GLOBAL_FN="$(basename ${AQMEII_EDGAR_AREAS_GLOBAL_URI})"
export AQMEII_EDGAR_AREAS_GLOBAL_FP="${WORK_DIR}/${AQMEII_EDGAR_AREAS_GLOBAL_FN}"
export AQMEII_EDGAR_AREAS_GLOBAL_DV_NAME='cell_area' # cell_area(lat, lon)

### input: "raw" EPIC N2O emittivities

# too big to download. TODO: stash something somewhere
EPIC_RAW_EMITT_FN='5yravg_20111219.nc' # previously demonontonicized? no
# EPIC_RAW_EMITT_FN='5yravg_20130605.nc' # previously demonontonicized?
EPIC_RAW_EMITT_ROOT="${EPIC_RAW_EMITT_FN%.*}" # everything left of the LAST dot
export EPIC_RAW_EMITT_FP="${WORK_DIR}/${EPIC_RAW_EMITT_FN}"
##### NOTE: datavar name != 'DN'!
export EPIC_RAW_EMITT_DATAVAR_NAME='DN2' # dim=(TSTEP, LAY, ROW, COL)
# export EPIC_RAW_EMITT_ATTR_=''

### input: CMAQ template for regrids (TODO: just use EPIC input)

## CMAQ/AQMEII-NA template: copy of some meteorological data (MCIP? raw WRF?),
## with 52 "real" datavars, in the IOAPI format, which here is basically a wrapper around netCDF.
# (IOAPI can be used with other data, and similar wrappers exist for other models.)
# I removed all but one of the datavars (with NCO 'ncks'). TODO: script that!
TEMPLATE_INPUT_GZ_URI='https://bitbucket.org/epa_n2o_project_team/geia_regrid/downloads/emis_mole_all_20080101_12US1_cmaq_cb05_soa_2008ab_08c.EXTENTS_INPUT.nc.gz'
TEMPLATE_INPUT_GZ_FN="$(basename ${TEMPLATE_INPUT_GZ_URI})"
TEMPLATE_INPUT_GZ_FP="${WORK_DIR}/${TEMPLATE_INPUT_GZ_FN}"
TEMPLATE_INPUT_ROOT="${TEMPLATE_INPUT_GZ_FN%.*}" # everything left of the LAST dot
TEMPLATE_INPUT_FN="${TEMPLATE_INPUT_ROOT}"
export TEMPLATE_INPUT_FP="${WORK_DIR}/${TEMPLATE_INPUT_FN}"
export TEMPLATE_DATAVAR_NAME='emi_n2o'

### input: raw BELD CSV crop coverage data
BELD_CSV_GZ_URI='https://bitbucket.org/epa_n2o_project_team/aqmeii_ag_soil/downloads/epic_site_crops_0529_USA_2Ellen.csv.gz'
BELD_CSV_GZ_FN="$(basename ${BELD_CSV_GZ_URI})"
BELD_CSV_GZ_FP="${WORK_DIR}/${BELD_CSV_GZ_FN}"
BELD_CSV_ROOT="${BELD_CSV_GZ_FN%.*}" # everything left of the LAST dot
BELD_CSV_FN="${BELD_CSV_ROOT}"
export BELD_CSV_FP="${WORK_DIR}/${BELD_CSV_FN}"

# ----------------------------------------------------------------------
# intermediate products
# ----------------------------------------------------------------------

### intermediate product: reunit-ed global EDGAR N2O

AQMEII_EDGAR_N2O_GLOBAL_REUNIT_FN_ROOT="${AQMEII_EDGAR_N2O_GLOBAL_FN_ROOT}_reunit"
AQMEII_EDGAR_N2O_GLOBAL_REUNIT_FN="${AQMEII_EDGAR_N2O_GLOBAL_REUNIT_FN_ROOT}.${NETCDF_EXT_NCL}"
export AQMEII_EDGAR_N2O_GLOBAL_REUNIT_FP="${WORK_DIR}/${AQMEII_EDGAR_N2O_GLOBAL_REUNIT_FN}"

## file/global attributes
export AQMEII_EDGAR_N2O_GLOBAL_REUNIT_HISTORY="${PROJECT_HISTORY}"
export AQMEII_EDGAR_N2O_GLOBAL_REUNIT_SOURCE_FILE="EDGAR N2O emissions from agricultural soils for ${MODEL_YEAR} from ${AQMEII_EDGAR_N2O_GLOBAL_ZIP_HOMEPAGE}"
export AQMEII_EDGAR_N2O_GLOBAL_REUNIT_TITLE="global EDGAR N2O emissions from agricultural soils for ${MODEL_YEAR} in ${AQMEIINA_DV_UNITS}"

## datavar and attributes
export AQMEII_EDGAR_N2O_GLOBAL_REUNIT_DV_NAME="${AQMEII_EDGAR_N2O_GLOBAL_DV_NAME}"
export AQMEII_EDGAR_N2O_GLOBAL_REUNIT_DV_TYPE="${AQMEII_EDGAR_N2O_GLOBAL_DV_TYPE}"
export AQMEII_EDGAR_N2O_GLOBAL_REUNIT_DV_UNITS="${AQMEIINA_DV_UNITS}" # hopefully :-)
export AQMEII_EDGAR_N2O_GLOBAL_REUNIT_DV_LONG_NAME="${AQMEII_EDGAR_N2O_GLOBAL_DV_LONG_NAME}"
export AQMEII_EDGAR_N2O_GLOBAL_REUNIT_DV_COORD_X_NAME="${AQMEII_EDGAR_N2O_GLOBAL_DV_COORD_X_NAME}"
export AQMEII_EDGAR_N2O_GLOBAL_REUNIT_DV_COORD_Y_NAME="${AQMEII_EDGAR_N2O_GLOBAL_DV_COORD_Y_NAME}"
# do I want to rotate its longitudes? i.e. from 0-360 -> -180-+180
export AQMEII_EDGAR_N2O_GLOBAL_REUNIT_ROTATE_LONS='FALSE'

### intermediate product: output/AQMEII-NA gridcell areas

export AQMEIINA_NOMINAL_GRIDCELL_AREA='1.440e8' # for 12-km grid, in meter^2; make float
## gridcell map scale factors (MSFs) from MCIP for CDC PHASE run
AQMEIINA_MSFs_GZ_URI='https://bitbucket.org/epa_n2o_project_team/clm_cn_global_to_aqmeii-na/downloads/GRIDCRO2D_080101.gz'
AQMEIINA_MSFs_GZ_URI='https://bitbucket.org/epa_n2o_project_team/clm_cn_global_to_aqmeii-na/downloads/GRIDCRO2D_080101.gz'
AQMEIINA_MSFs_GZ_FN="$(basename ${AQMEIINA_MSFs_GZ_URI})"
AQMEIINA_MSFs_FN_ROOT="${AQMEIINA_MSFs_GZ_FN%.*}" # everything left of the dot
AQMEIINA_MSFs_FN="${AQMEIINA_MSFs_FN_ROOT}"       # does not normally have an extension
AQMEIINA_MSFs_FN_NCL="${AQMEIINA_MSFs_FN}.${NETCDF_EXT_NCL}" # ... which makes NCL vomit
AQMEIINA_MSFs_GZ_FP="${WORK_DIR}/${AQMEIINA_MSFs_GZ_FN}"
export AQMEIINA_MSFs_FP="${WORK_DIR}/${AQMEIINA_MSFs_FN}"
export AQMEIINA_MSFs_FP_NCL="${WORK_DIR}/${AQMEIINA_MSFs_FN_NCL}"
export AQMEIINA_MSFs_DATAVAR_NAME='MSFX2'
export AQMEIINA_MSFs_DIM_ROW_NAME='ROW' # long_name, units like input
export AQMEIINA_MSFs_DIM_COL_NAME='COL' # long_name, units like input
## output areas file
AQMEIINA_AREAS_FN="areas_AQMEII-NA.${NETCDF_EXT_NCL}"
AQMEIINA_AREAS_FN="areas_AQMEII-NA.${NETCDF_EXT_NCL}"
export AQMEIINA_AREAS_FP="${WORK_DIR}/${AQMEIINA_AREAS_FN}" # must not exist @ write-time
export AQMEIINA_AREAS_DATAVAR_NAME='cell_area'              # like EDGAR
export AQMEIINA_AREAS_DATAVAR_LONG_NAME='area of grid cell' # like EDGAR
export AQMEIINA_AREAS_DATAVAR_STANDARD_NAME='area'          # like EDGAR
export AQMEIINA_AREAS_DATAVAR_UNITS='m2'                    # like EDGAR
export AQMEIINA_AREAS_DIM_ROW_NAME='ROW' # long_name, units like input
export AQMEIINA_AREAS_DIM_COL_NAME='COL' # long_name, units like input
export AQMEIINA_AREAS_HISTORY="${PROJECT_HISTORY}"

### intermediate product: plot of reunit-ed global EDGAR N2O

AQMEII_EDGAR_N2O_GLOBAL_REUNIT_PDF_FN_ROOT="${AQMEII_EDGAR_N2O_GLOBAL_REUNIT_FN%.*}" # everything left of the LAST dot
AQMEII_EDGAR_N2O_GLOBAL_REUNIT_PDF_FN="${AQMEII_EDGAR_N2O_GLOBAL_REUNIT_PDF_FN_ROOT}.pdf"
export AQMEII_EDGAR_N2O_GLOBAL_REUNIT_PDF_FP="${WORK_DIR}/${AQMEII_EDGAR_N2O_GLOBAL_REUNIT_PDF_FN}"

### intermediate product: global->AQMEII regridding of re-united EDGAR emissions

AQMEII_EDGAR_N2O_REGRID_FN_ROOT="${AQMEII_EDGAR_N2O_GLOBAL_REUNIT_FN_ROOT}_regrid"
AQMEII_EDGAR_N2O_REGRID_FN="${AQMEII_EDGAR_N2O_REGRID_FN_ROOT}.${NETCDF_EXT_NCL}"
export AQMEII_EDGAR_N2O_REGRID_FP="${WORK_DIR}/${AQMEII_EDGAR_N2O_REGRID_FN}"

## file/global attributes
export AQMEII_EDGAR_N2O_REGRID_HISTORY="${PROJECT_HISTORY}"
export AQMEII_EDGAR_N2O_REGRID_SOURCE_FILE="${AQMEII_EDGAR_N2O_GLOBAL_REUNIT_FP}"
export AQMEII_EDGAR_N2O_REGRID_TITLE="EDGAR N2O emissions from agricultural soils for ${MODEL_YEAR} in ${AQMEIINA_DV_UNITS} 2D-regridded to AQMEII-NA"

## datavar and attributes

# datavar attributes
export AQMEII_EDGAR_N2O_REGRID_DV_NAME="${AQMEIINA_DV_NAME}"
export AQMEII_EDGAR_N2O_REGRID_DV_UNITS="${AQMEIINA_DV_UNITS}"
export AQMEII_EDGAR_N2O_REGRID_DV_LONG_NAME="${AQMEIINA_DV_LONG_NAME}"

# datavar dims=(TSTEP, LAY, ROW, COL)

export AQMEII_EDGAR_N2O_REGRID_DIM_LAYER_NAME="${AQMEIINA_DIM_LAYER_NAME}"
export AQMEII_EDGAR_N2O_REGRID_DIM_LAYER_UNITS="${AQMEIINA_DIM_LAYER_UNITS}"
export AQMEII_EDGAR_N2O_REGRID_DIM_LAYER_LONG_NAME="${AQMEIINA_DIM_LAYER_LONG_NAME}"
export AQMEII_EDGAR_N2O_REGRID_DIM_LAYER_N='1'

export AQMEII_EDGAR_N2O_REGRID_DIM_TSTEP_NAME="${AQMEIINA_DIM_TSTEP_NAME}"
export AQMEII_EDGAR_N2O_REGRID_DIM_TSTEP_UNITS="${AQMEIINA_DIM_TSTEP_UNITS}"
export AQMEII_EDGAR_N2O_REGRID_DIM_TSTEP_LONG_NAME="${AQMEIINA_DIM_TSTEP_LONG_NAME}"
export AQMEII_EDGAR_N2O_REGRID_DIM_TSTEP_N='1'

export AQMEII_EDGAR_N2O_REGRID_DIM_X_NAME="${AQMEIINA_DIM_X_NAME}"
export AQMEII_EDGAR_N2O_REGRID_DIM_X_UNITS="${AQMEIINA_DIM_X_UNITS}"
export AQMEII_EDGAR_N2O_REGRID_DIM_X_LONG_NAME="${AQMEIINA_DIM_X_LONG_NAME}"
export AQMEII_EDGAR_N2O_REGRID_DIM_X_N="${AQMEIINA_DIM_X_N}"

export AQMEII_EDGAR_N2O_REGRID_DIM_Y_NAME="${AQMEIINA_DIM_Y_NAME}"
export AQMEII_EDGAR_N2O_REGRID_DIM_Y_UNITS="${AQMEIINA_DIM_Y_UNITS}"
export AQMEII_EDGAR_N2O_REGRID_DIM_Y_LONG_NAME="${AQMEIINA_DIM_Y_LONG_NAME}"
export AQMEII_EDGAR_N2O_REGRID_DIM_Y_N="${AQMEIINA_DIM_Y_N}"

### intermediate product: plot of regridded EDGAR N2O

AQMEII_EDGAR_N2O_REGRID_PDF_FN_ROOT="${AQMEII_EDGAR_N2O_REGRID_FN%.*}" # everything left of the LAST dot
AQMEII_EDGAR_N2O_REGRID_PDF_FN="${AQMEII_EDGAR_N2O_REGRID_PDF_FN_ROOT}.pdf"
export AQMEII_EDGAR_N2O_REGRID_PDF_FP="${WORK_DIR}/${AQMEII_EDGAR_N2O_REGRID_PDF_FN}"

### intermediate product: BELD CSV crop coverage data as RDS

BELD_RDS_ROOT="${BELD_CSV_FN%.*}" # everything left of the LAST dot
BELD_RDS_FN="${BELD_RDS_ROOT}.${RDS_EXT}"
export BELD_RDS_FP="${WORK_DIR}/${BELD_RDS_FN}"

### intermediate product: "distilled" or "purified" EPIC N2O emittivities

AQMEII_EPIC_N2O_EMITT_ROOT="${EPIC_RAW_EMITT_ROOT}_pure"
AQMEII_EPIC_N2O_EMITT_FN="${AQMEII_EPIC_N2O_EMITT_ROOT}.${NETCDF_EXT_NCL}"
export AQMEII_EPIC_N2O_EMITT_FP="${WORK_DIR}/${AQMEII_EPIC_N2O_EMITT_FN}"
export AQMEII_EPIC_N2O_EMITT_ATTR_HISTORY="${PROJECT_HISTORY}"
export AQMEII_EPIC_N2O_EMITT_ATTR_FILEDESC="$(printf '%-60s' 'EPIC yearly average N2O emittivities in mol/s')" # raw EPIC attribute text is incorrect
# export AQMEII_EPIC_N2O_EMITT_ATTR_=''

## dimensions
# names: unfortunately ncdf4 appears to need these defined to walk its ADSs :-(
export AQMEII_EPIC_N2O_EMITT_DIM_X_NAME="${AQMEIINA_DIM_X_NAME}"
export AQMEII_EPIC_N2O_EMITT_DIM_Y_NAME="${AQMEIINA_DIM_Y_NAME}"
export AQMEII_EPIC_N2O_EMITT_DIM_LAYERS_NAME="${AQMEIINA_DIM_LAYER_NAME}"
export AQMEII_EPIC_N2O_EMITT_DIM_TSTEPS_NAME="${AQMEIINA_DIM_TSTEP_NAME}"

## "real" datavar, with information we actually want
export AQMEII_EPIC_N2O_EMITT_DATAVAR_NAME='N2Oemittivities' # dim=(LAY, ROW, COL)
# IOAPI pads varattr=long_name to length=16 with trailing spaces
export AQMEII_EPIC_N2O_EMITT_DATAVAR_LONG_NAME="$(printf '%-16s' 'N2O emittivities')"
# IOAPI pads varattr=units to length=16 with trailing spaces
export AQMEII_EPIC_N2O_EMITT_DATAVAR_UNITS="${AQMEIINA_DV_UNITS}"
# IOAPI pads varattr=var_desc to length=80 with trailing spaces
export AQMEII_EPIC_N2O_EMITT_DATAVAR_VAR_DESC="$(printf '%-80s' 'N2O emittivities')"

## "fake" datavar=TFLAG, required by IOAPI (and more to the point (IIUC), VERDI)
export AQMEII_EPIC_N2O_EMITT_TFLAG_NAME="${AQMEIINA_TFLAG_NAME}"
# with its own special dimensions
export AQMEII_EPIC_N2O_EMITT_TFLAG_DIM_DATETIME_NAME="${AQMEIINA_DIM_DATETIME_NAME}"
export AQMEII_EPIC_N2O_EMITT_TFLAG_DIM_VAR_NAME="${AQMEIINA_DIM_VAR_NAME}"

# get these from driver
# input.epic.TFLAG.units <- input.epic.TFLAG$units
# input.epic.TFLAG.long_name <- input.epic.TFLAG$long_name
# input.epic.TFLAG.var_desc <- input.epic.TFLAG$var_desc
# input_epic_dim_datetimes_name]]
# input_epic_dim_layers_name]]
# input_epic_dim_vars_name]]

### intermediate product: EPIC N2O emissions over AQMEII in mol/s

AQMEII_EPIC_N2O_EMISS_ROOT="${AQMEII_EPIC_N2O_EMITT_ROOT}_emis"
AQMEII_EPIC_N2O_EMISS_FN="${AQMEII_EPIC_N2O_EMISS_ROOT}.${NETCDF_EXT_NCL}"
export AQMEII_EPIC_N2O_EMISS_FP="${WORK_DIR}/${AQMEII_EPIC_N2O_EMISS_FN}"

## dimensions
# names: unfortunately ncdf4 appears to need these defined to walk its ADSs :-(
export AQMEII_EPIC_N2O_EMISS_DIM_X_NAME="${AQMEIINA_DIM_X_NAME}"
# might as well make these explicit, if we're gonna create coordvars
export AQMEII_EPIC_N2O_EMISS_DIM_X_ATTR_UNITS="${AQMEIINA_DIM_X_UNITS}"
export AQMEII_EPIC_N2O_EMISS_DIM_X_ATTR_LONG_NAME="${AQMEIINA_DIM_X_LONG_NAME}"

export AQMEII_EPIC_N2O_EMISS_DIM_Y_NAME="${AQMEIINA_DIM_Y_NAME}"
export AQMEII_EPIC_N2O_EMISS_DIM_Y_ATTR_UNITS="${AQMEIINA_DIM_Y_UNITS}"
export AQMEII_EPIC_N2O_EMISS_DIM_Y_ATTR_LONG_NAME="${AQMEIINA_DIM_Y_LONG_NAME}"

export AQMEII_EPIC_N2O_EMISS_DIM_DATETIME_NAME="${AQMEIINA_DIM_DATETIME_NAME}"
export AQMEII_EPIC_N2O_EMISS_DIM_DATETIME_ATTR_UNITS="${AQMEIINA_DIM_DATETIME_UNITS}"
export AQMEII_EPIC_N2O_EMISS_DIM_DATETIME_ATTR_LONG_NAME="${AQMEIINA_DIM_DATETIME_LONG_NAME}"

export AQMEII_EPIC_N2O_EMISS_DIM_LAYER_NAME="${AQMEIINA_DIM_LAYER_NAME}"
export AQMEII_EPIC_N2O_EMISS_DIM_LAYER_ATTR_UNITS="${AQMEIINA_DIM_LAYER_UNITS}"
export AQMEII_EPIC_N2O_EMISS_DIM_LAYER_ATTR_LONG_NAME="${AQMEIINA_DIM_LAYER_LONG_NAME}"

export AQMEII_EPIC_N2O_EMISS_DIM_TSTEP_NAME="${AQMEIINA_DIM_TSTEP_NAME}"
export AQMEII_EPIC_N2O_EMISS_DIM_TSTEP_ATTR_UNITS="${AQMEIINA_DIM_TSTEP_UNITS}"
export AQMEII_EPIC_N2O_EMISS_DIM_TSTEP_ATTR_LONG_NAME="${AQMEIINA_DIM_TSTEP_LONG_NAME}"

export AQMEII_EPIC_N2O_EMISS_DIM_VAR_NAME="${AQMEIINA_DIM_VAR_NAME}"
export AQMEII_EPIC_N2O_EMISS_DIM_VAR_ATTR_UNITS="${AQMEIINA_DIM_VAR_UNITS}"
export AQMEII_EPIC_N2O_EMISS_DIM_VAR_ATTR_LONG_NAME="${AQMEIINA_DIM_VAR_LONG_NAME}"

## datavar
export AQMEII_EPIC_N2O_EMISS_DATAVAR_NAME="${AQMEIINA_DV_NAME}"
export AQMEII_EPIC_N2O_EMISS_DATAVAR_ATTR_LONG_NAME="${AQMEIINA_DV_LONG_NAME}"
export AQMEII_EPIC_N2O_EMISS_DATAVAR_ATTR_UNITS="${AQMEIINA_DV_UNITS}"
export AQMEII_EPIC_N2O_EMISS_DATAVAR_ATTR_VAR_DESC="${AQMEIINA_DV_VAR_DESC}"
## "fake" datavar=TFLAG, required by IOAPI (and more to the point (IIUC), VERDI)
export AQMEII_EPIC_N2O_EMISS_TFLAG_NAME="${AQMEIINA_TFLAG_NAME}"
export AQMEII_EPIC_N2O_EMISS_TFLAG_ATTR_LONG_NAME="${AQMEIINA_TFLAG_LONG_NAME}"
export AQMEII_EPIC_N2O_EMISS_TFLAG_ATTR_UNITS="${AQMEIINA_TFLAG_UNITS}"
export AQMEII_EPIC_N2O_EMISS_TFLAG_ATTR_VAR_DESC="${AQMEIINA_TFLAG_VAR_DESC}"

## file/global attributes
export AQMEII_EPIC_N2O_EMISS_ATTR_NLAYS='1' # output semantics: "layer" == vertical, not crop
export AQMEII_EPIC_N2O_EMISS_ATTR_NLAYS_TYPE='integer' # 'short' -> `:NLAYS = 1s ;`
export AQMEII_EPIC_N2O_EMISS_ATTR_VARLIST="${AQMEIINA_DV_LONG_NAME}"
export AQMEII_EPIC_N2O_EMISS_ATTR_VARLIST_TYPE='character'
# IOAPI pads fileattr=filedesc to length=60 with trailing spaces
export AQMEII_EPIC_N2O_EMISS_ATTR_FILEDESC="$(printf '%-16s' 'EPIC yearly average N2O emissions in mol/s')"
export AQMEII_EPIC_N2O_EMISS_ATTR_FILEDESC_TYPE='character'

## visualization
AQMEII_EPIC_N2O_EMISS_PDF_FN="${AQMEII_EPIC_N2O_EMISS_ROOT}.pdf"
export AQMEII_EPIC_N2O_EMISS_PDF_FP="${WORK_DIR}/${AQMEII_EPIC_N2O_EMISS_PDF_FN}"

### intermediate product: combined EPIC and EDGAR N2O emissions over AQMEII-NA

AQMEII_BOTH_N2O_EMISS_ROOT="${AQMEII_EPIC_N2O_EMISS_ROOT}_both"
AQMEII_BOTH_N2O_EMISS_FN="${AQMEII_BOTH_N2O_EMISS_ROOT}.${NETCDF_EXT_NCL}"
export AQMEII_BOTH_N2O_EMISS_FP="${WORK_DIR}/${AQMEII_BOTH_N2O_EMISS_FN}"

## dimensions
# names: unfortunately ncdf4 appears to need these defined to walk its ADSs :-(
export AQMEII_BOTH_N2O_EMISS_DIM_X_NAME="${AQMEIINA_DIM_X_NAME}"
export AQMEII_BOTH_N2O_EMISS_DIM_Y_NAME="${AQMEIINA_DIM_Y_NAME}"
# might as well make these explicit, if we're gonna create coordvars
export AQMEII_BOTH_N2O_EMISS_DIM_X_ATTR_UNITS="${AQMEIINA_DIM_X_UNITS}"
export AQMEII_BOTH_N2O_EMISS_DIM_X_ATTR_LONG_NAME="${AQMEIINA_DIM_X_LONG_NAME}"
export AQMEII_BOTH_N2O_EMISS_DIM_Y_ATTR_UNITS="${AQMEIINA_DIM_Y_UNITS}"
export AQMEII_BOTH_N2O_EMISS_DIM_Y_ATTR_LONG_NAME="${AQMEIINA_DIM_Y_LONG_NAME}"

## datavar
export AQMEII_BOTH_N2O_EMISS_DATAVAR_NAME="${AQMEIINA_DV_NAME}" # dim=(ROW, COL)
export AQMEII_BOTH_N2O_EMISS_DATAVAR_ATTR_LONG_NAME="${AQMEIINA_DV_LONG_NAME}"
export AQMEII_BOTH_N2O_EMISS_DATAVAR_ATTR_UNITS="${AQMEIINA_DV_UNITS}"
export AQMEII_BOTH_N2O_EMISS_DATAVAR_ATTR_VAR_DESC="${AQMEIINA_DV_VAR_DESC}"
# "fake" datavar=TFLAG, required by IOAPI (and more to the point (IIUC), VERDI)
export AQMEII_BOTH_N2O_EMISS_TFLAG_NAME="${AQMEIINA_TFLAG_NAME}"
export AQMEII_BOTH_N2O_EMISS_TFLAG_ATTR_LONG_NAME="${AQMEIINA_TFLAG_LONG_NAME}"
export AQMEII_BOTH_N2O_EMISS_TFLAG_ATTR_UNITS="${AQMEIINA_TFLAG_UNITS}"
export AQMEII_BOTH_N2O_EMISS_TFLAG_ATTR_VAR_DESC="${AQMEIINA_TFLAG_VAR_DESC}"

## file/global attributes
# export AQMEII_BOTH_N2O_EMISS_ATTR_NLAYS='1' # output semantics: "layer" == vertical, not crop
# export AQMEII_BOTH_N2O_EMISS_ATTR_NLAYS_TYPE='integer' # 'short' -> `:NLAYS = 1s ;`
export AQMEII_BOTH_N2O_EMISS_ATTR_VARLIST="${AQMEIINA_DV_LONG_NAME}"
export AQMEII_BOTH_N2O_EMISS_ATTR_VARLIST_TYPE='text' # or 'character': ncdf4 types
# IOAPI pads fileattr=filedesc to length=60 with trailing spaces
export AQMEII_BOTH_N2O_EMISS_ATTR_FILEDESC="$(printf '%-16s' 'EPIC+EDGAR N2O mol/s')"
export AQMEII_BOTH_N2O_EMISS_ATTR_FILEDESC_TYPE='character'

## visualization
AQMEII_BOTH_N2O_EMISS_PDF_FN="${AQMEII_BOTH_N2O_EMISS_ROOT}.pdf"
export AQMEII_BOTH_N2O_EMISS_PDF_FP="${WORK_DIR}/${AQMEII_BOTH_N2O_EMISS_PDF_FN}"

# ----------------------------------------------------------------------
# final output: hourly combined emissions over AQMEII
# ----------------------------------------------------------------------

# We output annual values, so make output for no specific date
AQMEII_HOURLY_N2O_EMISS_ROOT="emis_mole_N2O_${MODEL_YEAR}_12US1_cmaq_cb05_soa_2008ab_08c"
AQMEII_HOURLY_N2O_EMISS_FN_NCL="${AQMEII_HOURLY_N2O_EMISS_ROOT}.${NETCDF_EXT_NCL}"
export AQMEII_HOURLY_N2O_EMISS_FN_CMAQ="${AQMEII_HOURLY_N2O_EMISS_ROOT}.${NETCDF_EXT_CMAQ}"
# generally I'll work with the NCL-friendly filename ... because I hafta :-(
export AQMEII_HOURLY_N2O_EMISS_FP_NCL="${WORK_DIR}/${AQMEII_HOURLY_N2O_EMISS_FN_NCL}"
export AQMEII_HOURLY_N2O_EMISS_FP="${AQMEII_HOURLY_N2O_EMISS_FN_NCL}"
# ... but this driver will create this ultimate output
export AQMEII_HOURLY_N2O_EMISS_FP_CMAQ="${WORK_DIR}/${AQMEII_HOURLY_N2O_EMISS_FN_CMAQ}"

## dimensions
export AQMEII_HOURLY_N2O_EMISS_DIM_TSTEP_N='25'
# names: unfortunately ncdf4 appears to need these defined to walk its ADSs :-(
export AQMEII_HOURLY_N2O_EMISS_DIM_X_NAME="${AQMEIINA_DIM_X_NAME}"
export AQMEII_HOURLY_N2O_EMISS_DIM_Y_NAME="${AQMEIINA_DIM_Y_NAME}"
# might as well make these explicit, if we're gonna create coordvars
export AQMEII_HOURLY_N2O_EMISS_DIM_X_ATTR_UNITS="${AQMEIINA_DIM_X_UNITS}"
export AQMEII_HOURLY_N2O_EMISS_DIM_X_ATTR_LONG_NAME="${AQMEIINA_DIM_X_LONG_NAME}"
export AQMEII_HOURLY_N2O_EMISS_DIM_Y_ATTR_UNITS="${AQMEIINA_DIM_Y_UNITS}"
export AQMEII_HOURLY_N2O_EMISS_DIM_Y_ATTR_LONG_NAME="${AQMEIINA_DIM_Y_LONG_NAME}"

## datavar
export AQMEII_HOURLY_N2O_EMISS_DATAVAR_NAME="${AQMEIINA_DV_NAME}" # dim=(ROW, COL)
export AQMEII_HOURLY_N2O_EMISS_DATAVAR_ATTR_LONG_NAME="${AQMEIINA_DV_LONG_NAME}"
export AQMEII_HOURLY_N2O_EMISS_DATAVAR_ATTR_UNITS="${AQMEIINA_DV_UNITS}"
export AQMEII_HOURLY_N2O_EMISS_DATAVAR_ATTR_VAR_DESC="${AQMEIINA_DV_VAR_DESC}"
# "fake" datavar=TFLAG, required by IOAPI (and more to the point (IIUC), VERDI)
export AQMEII_HOURLY_N2O_EMISS_TFLAG_NAME="${AQMEIINA_TFLAG_NAME}"
export AQMEII_HOURLY_N2O_EMISS_TFLAG_ATTR_LONG_NAME="${AQMEIINA_TFLAG_LONG_NAME}"
export AQMEII_HOURLY_N2O_EMISS_TFLAG_ATTR_UNITS="${AQMEIINA_TFLAG_UNITS}"
export AQMEII_HOURLY_N2O_EMISS_TFLAG_ATTR_VAR_DESC="${AQMEIINA_TFLAG_VAR_DESC}"

## file/global attributes
# copy these from input
export AQMEII_HOURLY_N2O_EMISS_ATTR_NLAYS='1' # output semantics: "layer" == vertical, not crop
export AQMEII_HOURLY_N2O_EMISS_ATTR_NLAYS_TYPE='integer' # 'short' -> `:NLAYS = 1s ;`
export AQMEII_HOURLY_N2O_EMISS_ATTR_VARLIST="${AQMEIINA_DV_LONG_NAME}"
export AQMEII_HOURLY_N2O_EMISS_ATTR_VARLIST_TYPE='text' # or 'character': ncdf4 types
# IOAPI pads fileattr=filedesc to length=60 with trailing spaces
export AQMEII_HOURLY_N2O_EMISS_ATTR_FILEDESC="${AQMEII_BOTH_N2O_EMISS_ATTR_FILEDESC}"
export AQMEII_HOURLY_N2O_EMISS_ATTR_FILEDESC_TYPE="${AQMEII_BOTH_N2O_EMISS_ATTR_FILEDESC_TYPE}"

export AQMEII_HOURLY_N2O_EMISS_ATTR_HISTORY="see ${PROJECT_WEBSITE}"
export AQMEII_HOURLY_N2O_EMISS_ATTR_FILEDESC='hourly N2O emissions from EPIC+EDGAR sources over AQMEII-NA in mol/s, IOAPI-zed'
# IOAPI metadata: see http://www.baronams.com/products/ioapi/INCLUDE.html#fdesc
export IOAPI_TIMESTEP_LENGTH='10000' # :TSTEP
export IOAPI_START_OF_TIMESTEPS='0'  # :STIME

## no visualization: will do some in check_conservation

# ----------------------------------------------------------------------
# functions
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# setup functions
# ----------------------------------------------------------------------

# # fails
# function command_not_found_handle {
#   CMD=$1
#   echo -e "\n$ ${THIS_FN}: command=${CMD} not found\n"
#   exit 1
# }

# # fails
# # see Tedzdog Jan 23 '12 at 6:11 @ http://unix.stackexchange.com/questions/29689/how-do-i-redefine-a-bash-function-in-terms-of-old-definition
# eval 'command_not_found_handle () {
#   CMD=$1
#   echo -e "\n$ ${THIS_FN}: command=${CMD} not found\n"
#   exit 1
# }'

# TODO: test for resources, reuse if available
# `setup_paths` isa bash util, so `get_helpers` first
# `setup_apps` ditto
function setup {
  for CMD in \
    "mkdir -p ${WORK_DIR}" \
    'get_helpers' \
    'setup_paths' \
    'setup_apps' \
    'setup_resources' \
  ; do
    echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
    eval "${CMD}" # comment this out for NOPing, e.g., to `source`
    if [[ $? -ne 0 ]] ; then
      echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
      exit 1
    fi
  done
#  echo -e "\n$ ${THIS_FN}: PDF_VIEWER='${PDF_VIEWER}'" # debugging
} # end function setup

function get_helpers {
  for CMD in \
    'get_regrid_utils' \
    'get_bash_utils' \
    'get_reunit_vis_EDGAR_global' \
    'get_R_utils' \
    'get_stats_funcs' \
    'get_vis_funcs' \
    'get_regrid_vis_EDGAR_AQMEII' \
    'get_BELD_read' \
    'get_BELD_convert' \
    'get_get_AQMEIINA_areas' \
    'get_time_funcs' \
    'get_distill_EPIC' \
    'get_string_funcs' \
    'get_compute_EPIC' \
    'get_combine_emis' \
    'get_IOAPI_funcs' \
    'get_make_hourlies' \
    'get_summarize' \
    'get_check_conservation' \
  ; do
    echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
    eval "${CMD}" # comment this out for NOPing, e.g., to `source`
    if [[ $? -ne 0 ]] ; then
      echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
      exit 2
    fi
  done
} # end function get_helpers

# This gets "package"=regrid_utils from which the remaining helpers are (at least potentially) drawn.
function get_regrid_utils {
  if [[ -z "${REGRID_UTILS_DIR}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: REGRID_UTILS_DIR not defined"
    exit 3
  fi

  # Noticed a problem where code committed to regrid_utils was not replacing code in existing ${REGRID_UTILS_DIR}, so ...
  if [[ -r "${REGRID_UTILS_DIR}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: removing existing regrid_utils folder='${REGRID_UTILS_DIR}'"
    for CMD in \
      "rm -fr ${REGRID_UTILS_DIR}/" \
     ; do
      echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
      eval "${CMD}" # comment this out for NOPing, e.g., to `source`
      if [[ $? -ne 0 ]] ; then
        echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
        exit 4
      fi
    done
  fi

  if [[ ! -r "${REGRID_UTILS_DIR}" ]] ; then
  # assumes GIT_CLONE_PREFIX set in environment by, e.g., uber_driver
    for CMD in \
      "${GIT_CLONE_PREFIX} git clone ${REGRID_UTILS_URI}.git" \
    ; do
      echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
      eval "${CMD}" # comment this out for NOPing, e.g., to `source`
      if [[ $? -ne 0 ]] ; then
        echo # newline
        echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found"
        echo -e "\t(suggestion: check that GIT_CLONE_PREFIX is set appropriately in calling environment)"
        echo # newline
        exit 5
      fi
    done
  fi

  if [[ ! -r "${REGRID_UTILS_DIR}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: cannot download regrid_utils to '${REGRID_UTILS_DIR}'"
    exit 6
  fi  
} # end function get_regrid_utils

# isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
# To override, copy/mod to ${IOAPI_FUNCS_FP} before running this script.
function get_IOAPI_funcs {
  if [[ -z "${IOAPI_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: IOAPI_FUNCS_FP not defined"
    exit 24
  fi
  if [[ ! -r "${IOAPI_FUNCS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${IOAPI_FUNCS_FN} ${IOAPI_FUNCS_FP}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
  fi
  if [[ ! -r "${IOAPI_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: IOAPI_FUNCS_FP=='${IOAPI_FUNCS_FP}' not readable"
    exit 25
  fi
} # end function get_IOAPI_funcs

# isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
# To override, copy/mod to ${BASH_UTILS_FP} before running this script.
function get_bash_utils {
  if [[ -z "${BASH_UTILS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: BASH_UTILS_FP not defined"
    exit 7
  fi
  if [[ ! -r "${BASH_UTILS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${BASH_UTILS_FN} ${BASH_UTILS_FP}" \
    ; do
      echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
      eval "${CMD}" # comment this out for NOPing, e.g., to `source`
      if [[ $? -ne 0 ]] ; then
        echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
        exit 8
      fi
    done
  fi
  if [[ ! -r "${BASH_UTILS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: BASH_UTILS_FP=='${BASH_UTILS_FP}' not readable"
    exit 9
  fi
  # This is bash, so gotta ...
  source "${BASH_UTILS_FP}"
  # ... for its functions to be available later in this script
} # end function get_bash_utils

function get_reunit_vis_EDGAR_global {
  if [[ -z "${REUNIT_VIS_EDGAR_GLOBAL_FP}" ]] ; then
    echo -e "${THIS_FN}: ERROR: REUNIT_VIS_EDGAR_GLOBAL_FP not defined"
    exit 10
  fi
  # is in this repo
#  if [[ ! -r "${REUNIT_VIS_EDGAR_GLOBAL_FP}" ]] ; then
#    for CMD in \
#      "${WGET_TO_FILE} ${REUNIT_VIS_EDGAR_GLOBAL_FP} ${REUNIT_VIS_EDGAR_GLOBAL_URI}" \
#    ; do
#      echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
#      eval "${CMD}"
#    done
#  fi
  if [[ ! -r "${REUNIT_VIS_EDGAR_GLOBAL_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: REUNIT_VIS_EDGAR_GLOBAL_FP=='${REUNIT_VIS_EDGAR_GLOBAL_FP}' not readable"
    exit 11
  fi
} # end function get_reunit_vis_EDGAR_global

# isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
# To override, copy/mod to ${VIS_FUNCS_FP} before running this script.
function get_vis_funcs {
  if [[ -z "${VIS_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: VIS_FUNCS_FP not defined"
    exit 12
  fi
  if [[ ! -r "${VIS_FUNCS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${VIS_FUNCS_FN} ${VIS_FUNCS_FP}" \
    ; do
      echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
        exit 13
      fi
    done
  fi
  if [[ ! -r "${VIS_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: VIS_FUNCS_FP=='${VIS_FUNCS_FP}' not readable"
    exit 14
  fi
} # end function get_vis_funcs

# isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
# To override, copy/mod to ${R_UTILS_FP} before running this script.
function get_R_utils {
  if [[ -z "${R_UTILS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: R_UTILS_FP not defined"
    exit 15
  fi
  if [[ ! -r "${R_UTILS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${R_UTILS_FN} ${R_UTILS_FP}" \
    ; do
      echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
        exit 16
      fi
    done
  fi
  if [[ ! -r "${R_UTILS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: R_UTILS_FP=='${R_UTILS_FP}' not readable"
    exit 17
  fi
} # end function get_R_utils

# isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
# To override, copy/mod to ${STATS_FUNCS_FP} before running this script.
function get_stats_funcs {
  if [[ -z "${STATS_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: STATS_FUNCS_FP not defined"
    exit 18
  fi
  if [[ ! -r "${STATS_FUNCS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${STATS_FUNCS_FN} ${STATS_FUNCS_FP}" \
    ; do
      echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
        exit 19
      fi
    done
  fi
  if [[ ! -r "${STATS_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: STATS_FUNCS_FP=='${STATS_FUNCS_FP}' not readable"
    exit 20
  fi
} # end function get_stats_funcs

# isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
# To override, copy/mod to ${STRING_FUNCS_FP} before running this script.
function get_string_funcs {
  if [[ -z "${STRING_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: STRING_FUNCS_FP not defined"
    exit 21
  fi
  if [[ ! -r "${STRING_FUNCS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${STRING_FUNCS_FN} ${STRING_FUNCS_FP}" \
    ; do
      echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
        exit 22
      fi
    done
  fi
  if [[ ! -r "${STRING_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: STRING_FUNCS_FP=='${STRING_FUNCS_FP}' not readable"
    exit 23
  fi
} # end function get_string_funcs

function get_regrid_vis_EDGAR_AQMEII {
  if [[ -z "${REGRID_VIS_EDGAR_FP}" ]] ; then
    echo -e "${THIS_FN}: ERROR: REGRID_VIS_EDGAR_FP not defined"
    exit 24
  fi
  # is in this repo
#  if [[ ! -r "${REGRID_VIS_EDGAR_FP}" ]] ; then
#    for CMD in \
#      "${WGET_TO_FILE} ${REGRID_VIS_EDGAR_FP} ${REGRID_VIS_EDGAR_URI}" \
#    ; do
#      echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
#      eval "${CMD}"
#    done
#  fi
  if [[ ! -r "${REGRID_VIS_EDGAR_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: REGRID_VIS_EDGAR_FP=='${REGRID_VIS_EDGAR_FP}' not readable"
    exit 25
  fi
} # end function get_regrid_vis

function get_BELD_read {
  if [[ -z "${BELD_READ_FP}" ]] ; then
    echo -e "${THIS_FN}: ERROR: BELD_READ_FP not defined"
    exit 26
  fi
  # is in this repo
#  if [[ ! -r "${BELD_READ_FP}" ]] ; then
#    for CMD in \
#      "${WGET_TO_FILE} ${BELD_READ_FP} ${BELD_READ_URI}" \
#    ; do
#      echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
#      eval "${CMD}"
#    done
#  fi
  if [[ ! -r "${BELD_READ_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: BELD_READ_FP=='${BELD_READ_FP}' not readable"
    exit 27
  fi
} # end function get_BELD_read

function get_BELD_convert {
  if [[ -z "${BELD_CONVERT_FP}" ]] ; then
    echo -e "${THIS_FN}: ERROR: BELD_CONVERT_FP not defined"
    exit 28
  fi
  # is in this repo
#  if [[ ! -r "${BELD_CONVERT_FP}" ]] ; then
#    for CMD in \
#      "${WGET_TO_FILE} ${BELD_CONVERT_FP} ${BELD_CONVERT_URI}" \
#    ; do
#      echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
#      eval "${CMD}"
#    done
#  fi
  if [[ ! -r "${BELD_CONVERT_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: BELD_CONVERT_FP=='${BELD_CONVERT_FP}' not readable"
    exit 29
  fi
} # end function get_BELD_convert

# isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
# To override, copy/mod to ${GET_AQMEIINA_AREAS_FP} before running this script.
function get_get_AQMEIINA_areas {
  if [[ -z "${GET_AQMEIINA_AREAS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: GET_AQMEIINA_AREAS_FP not defined"
    exit 30
  fi
  if [[ ! -r "${GET_AQMEIINA_AREAS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${GET_AQMEIINA_AREAS_FN} ${GET_AQMEIINA_AREAS_FP}" \
    ; do
      echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
        exit 31
      fi
    done
  fi
  if [[ ! -r "${GET_AQMEIINA_AREAS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: GET_AQMEIINA_AREAS_FP=='${GET_AQMEIINA_AREAS_FP}' not readable"
    exit 32
  fi
} # end function get_get_AQMEIINA_areas

# isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
# To override, copy/mod to ${TIME_FUNCS_FP} before running this script.
function get_time_funcs {
  if [[ -z "${TIME_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: TIME_FUNCS_FP not defined"
    exit 33
  fi
  if [[ ! -r "${TIME_FUNCS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${TIME_FUNCS_FN} ${TIME_FUNCS_FP}" \
    ; do
      echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
        exit 34
      fi
    done
  fi
  if [[ ! -r "${TIME_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: TIME_FUNCS_FP=='${TIME_FUNCS_FP}' not readable"
    exit 35
  fi
} # end function get_time_funcs

function get_distill_EPIC {
  if [[ -z "${DISTILL_EPIC_FP}" ]] ; then
    echo -e "${THIS_FN}: ERROR: DISTILL_EPIC_FP not defined"
    exit 36
  fi
  # is in this repo
#  if [[ ! -r "${DISTILL_EPIC_FP}" ]] ; then
#    for CMD in \
#      "${WGET_TO_FILE} ${DISTILL_EPIC_FP} ${DISTILL_EPIC_URI}" \
#    ; do
#      echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
#      eval "${CMD}"
#    done
#  fi
  if [[ ! -r "${DISTILL_EPIC_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: DISTILL_EPIC_FP=='${DISTILL_EPIC_FP}' not readable"
    exit 37
  fi
} # end function get_distill_EPIC

function get_compute_EPIC {
  if [[ -z "${COMPUTE_EPIC_FP}" ]] ; then
    echo -e "${THIS_FN}: ERROR: COMPUTE_EPIC_FP not defined"
    exit 38
  fi
  # is in this repo
#  if [[ ! -r "${COMPUTE_EPIC_FP}" ]] ; then
#    for CMD in \
#      "${WGET_TO_FILE} ${COMPUTE_EPIC_FP} ${COMPUTE_EPIC_URI}" \
#    ; do
#      echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
#      eval "${CMD}"
#    done
#  fi
  if [[ ! -r "${COMPUTE_EPIC_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: COMPUTE_EPIC_FP=='${COMPUTE_EPIC_FP}' not readable"
    exit 39
  fi
} # end function get_compute_EPIC

function get_combine_emis {
  if [[ -z "${COMBINE_EMIS_FP}" ]] ; then
    echo -e "${THIS_FN}: ERROR: COMBINE_EMIS_FP not defined"
    exit 40
  fi
  # is in this repo
#  if [[ ! -r "${COMBINE_EMIS_FP}" ]] ; then
#    for CMD in \
#      "${WGET_TO_FILE} ${COMBINE_EMIS_FP} ${COMBINE_EMIS_URI}" \
#    ; do
#      echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
#      eval "${CMD}"
#    done
#  fi
  if [[ ! -r "${COMBINE_EMIS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: COMBINE_EMIS_FP=='${COMBINE_EMIS_FP}' not readable"
    exit 41
  fi
} # end function get_combine_emis

function get_make_hourlies {
  if [[ -z "${MAKE_HOURLIES_FP}" ]] ; then
    echo -e "${THIS_FN}: ERROR: MAKE_HOURLIES_FP not defined"
    exit 1
  fi
  # is in this repo
#  if [[ ! -r "${MAKE_HOURLIES_FP}" ]] ; then
#    for CMD in \
#      "${WGET_TO_FILE} ${MAKE_HOURLIES_FP} ${MAKE_HOURLIES_URI}" \
#    ; do
#      echo -e "$ ${CMD}"
#      eval "${CMD}"
#    done
#  fi
  if [[ ! -r "${MAKE_HOURLIES_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: MAKE_HOURLIES_FP=='${MAKE_HOURLIES_FP}' not readable"
    exit 1
  fi
} # end function get_make_hourlies

# isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
# To override, copy/mod to ${SUMMARIZE_FUNCS_FP} before running this script.
function get_summarize {
  if [[ -z "${SUMMARIZE_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: SUMMARIZE_FUNCS_FP not defined"
    exit 21
  fi
  if [[ ! -r "${SUMMARIZE_FUNCS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${SUMMARIZE_FUNCS_FN} ${SUMMARIZE_FUNCS_FP}" \
    ; do
      echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
        exit 22
      fi
    done
  fi
  if [[ ! -r "${SUMMARIZE_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: SUMMARIZE_FUNCS_FP=='${SUMMARIZE_FUNCS_FP}' not readable"
    exit 23
  fi
} # end function get_summarize

function get_check_conservation {
  if [[ -z "${CHECK_CONSERVATION_FP}" ]] ; then
    echo -e "${THIS_FN}: ERROR: CHECK_CONSERVATION_FP not defined"
    exit 1
  fi
  # is in this repo
#  if [[ ! -r "${CHECK_CONSERVATION_FP}" ]] ; then
#    for CMD in \
#      "${WGET_TO_FILE} ${CHECK_CONSERVATION_FP} ${CHECK_CONSERVATION_URI}" \
#    ; do
#      echo -e "$ ${CMD}"
#      eval "${CMD}"
#    done
#  fi
  if [[ ! -r "${CHECK_CONSERVATION_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: CHECK_CONSERVATION_FP=='${CHECK_CONSERVATION_FP}' not readable"
    exit 1
  fi
} # end function get_check_conservation

function setup_resources {
  for CMD in \
    'get_EDGAR_N2O' \
    'get_EDGAR_areas' \
    'get_EPIC' \
    'get_template' \
    'get_BELD_CSV' \
    'get_AQMEIINA_areas' \
  ; do
    echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
    eval "${CMD}" # comment this out for NOPing, e.g., to `source`
    if [[ $? -ne 0 ]] ; then
      echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
      exit 42
    fi
  done
} # end function setup_resources

function get_EDGAR_N2O {
  if [[
    -z "${CURL_TO_FILE}"
    || -z "${AQMEII_EDGAR_N2O_GLOBAL_ZIP_URI}"
    || -z "${AQMEII_EDGAR_N2O_GLOBAL_ZIP_FP}"
    || -z "${AQMEII_EDGAR_N2O_GLOBAL_FP}"
    || -z "${WORK_DIR}"
  ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: undefined dependencies"
    exit 43
  fi

  if [[ ! -r "${AQMEII_EDGAR_N2O_GLOBAL_FP}" ]] ; then
    if [[ ! -r "${AQMEII_EDGAR_N2O_GLOBAL_ZIP_FP}" ]] ; then
      for CMD in \
        "${WGET_TO_FILE} ${AQMEII_EDGAR_N2O_GLOBAL_ZIP_FP} ${AQMEII_EDGAR_N2O_GLOBAL_ZIP_URI}" \
        "unzip ${AQMEII_EDGAR_N2O_GLOBAL_ZIP_FP} -d ${WORK_DIR}" \
      ; do
        echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
        eval "${CMD}"
        if [[ $? -ne 0 ]] ; then
          echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
          exit 44
        fi
      done
    fi

    ## if we still don't have them, die ...
    if [[ ! -r "${AQMEII_EDGAR_N2O_GLOBAL_ZIP_FP}" ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: could not download EDGAR annual emissions archive='${AQMEII_EDGAR_N2O_GLOBAL_ZIP_FP}'"
      exit 45
    elif [[ ! -r "${AQMEII_EDGAR_N2O_GLOBAL_FP}" ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: could not unzip EDGAR annual emissions='${AQMEII_EDGAR_N2O_GLOBAL_FP}'"
      exit 46
    fi
  fi
} # end function get_EDGAR_N2O

function get_EDGAR_areas {
  if [[
    -z "${CURL_TO_FILE}"
    || -z "${AQMEII_EDGAR_AREAS_GLOBAL_URI}"
    || -z "${AQMEII_EDGAR_AREAS_GLOBAL_FP}"
    || -z "${WORK_DIR}"
  ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: undefined dependencies"
    exit 47
  fi

  if [[ ! -r "${AQMEII_EDGAR_AREAS_GLOBAL_FP}" ]] ; then
    for CMD in \
      "${CURL_TO_FILE} ${AQMEII_EDGAR_AREAS_GLOBAL_FP} ${AQMEII_EDGAR_AREAS_GLOBAL_URI}" \
    ; do
      echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
        exit 48
      fi
    done
  fi

  ## if we still don't have them, die ...
  if [[ ! -r "${AQMEII_EDGAR_AREAS_GLOBAL_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: could not download EDGAR annual emissions='${AQMEII_EDGAR_AREAS_GLOBAL_FP}'"
    exit 49
  fi
} # end function get_EDGAR_areas

function get_EPIC {
  if [[ ! -r "${EPIC_RAW_EMITT_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[1]}::${FUNCNAME[0]}: ERROR: cannot find raw EPIC emittivities='${EPIC_RAW_EMITT_FP}' (and it is too big to download)"
    exit 50
  fi
} # end function get_EPIC

### get the CMAQ "template" file, used for extent-setting and emissions-file creation
### TODO? use the EPIC input instead?
function get_template {
  if [[ -z "${TEMPLATE_INPUT_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: TEMPLATE_INPUT_FP not defined"
    exit 51
  fi
  if [[ ! -r "${TEMPLATE_INPUT_FP}" ]] ; then
    if [[ ! -r "${TEMPLATE_INPUT_GZ_FP}" ]] ; then
      for CMD in \
        "${WGET_TO_FILE} ${TEMPLATE_INPUT_GZ_FP} ${TEMPLATE_INPUT_GZ_URI}" \
      ; do
        echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
        eval "${CMD}"
        if [[ $? -ne 0 ]] ; then
          echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
          exit 52
        fi
      done
    fi
    if [[ -r "${TEMPLATE_INPUT_GZ_FP}" ]] ; then
      for CMD in \
        "gunzip ${TEMPLATE_INPUT_GZ_FP}" \
      ; do
        echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
        eval "${CMD}"
        if [[ $? -ne 0 ]] ; then
          echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
          exit 53
        fi
      done
    fi
  fi
  if [[ ! -r "${TEMPLATE_INPUT_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: regridding template=='${TEMPLATE_INPUT_FP}' not readable"
    exit 54
  fi
} # end function get_template

### Get BELD CSV coverage data
### TODO? use the EPIC input instead?
function get_BELD_CSV {
  if [[ -z "${BELD_CSV_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: BELD_CSV_FP not defined"
    exit 55
  fi
  if [[ ! -r "${BELD_CSV_FP}" ]] ; then
    if [[ ! -r "${BELD_CSV_GZ_FP}" ]] ; then
      for CMD in \
        "${WGET_TO_FILE} ${BELD_CSV_GZ_FP} ${BELD_CSV_GZ_URI}" \
      ; do
        echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
        eval "${CMD}"
        if [[ $? -ne 0 ]] ; then
          echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
          exit 56
        fi
      done
    fi
    if [[ -r "${BELD_CSV_GZ_FP}" ]] ; then
      for CMD in \
        "gunzip ${BELD_CSV_GZ_FP}" \
      ; do
        echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
        eval "${CMD}"
        if [[ $? -ne 0 ]] ; then
          echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
          exit 57
        fi
      done
    fi
  fi
  if [[ ! -r "${BELD_CSV_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: BELD coverages as CSV=='${BELD_CSV_FP}' not readable"
    exit 58
  fi
} # end function get_BELD_CSV

### Get AQMEII-NA gridcell map scale factors (MSFs).
function get_MSFs {
  if [[ -z "${AQMEIINA_MSFs_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: AQMEIINA_MSFs_FP not defined"
    exit 59
  fi

  if [[ -r "${AQMEIINA_MSFs_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: skipping ${FUNCNAME[0]}: output='${AQMEIINA_MSFs_FP}' is readable"
  else # make it
    if [[ ! -r "${AQMEIINA_MSFs_FP}" ]] ; then
      if [[ -z "${AQMEIINA_MSFs_GZ_FP}" ]] ; then
	echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: AQMEIINA_MSFs_GZ_FP not defined"
	exit 60
      fi
      if [[ ! -r "${AQMEIINA_MSFs_GZ_FP}" ]] ; then
	# TODO: pipe to gunzip
	for CMD in \
	  "${WGET_TO_FILE} ${AQMEIINA_MSFs_GZ_FP} ${AQMEIINA_MSFs_GZ_URI}" \
	; do
	  echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
	  eval "${CMD}" # comment this out for NOPing, e.g., to `source`
	  if [[ $? -ne 0 ]] ; then
	    echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
	    exit 61
	  fi
	done
      fi
      if [[ -r "${AQMEIINA_MSFs_GZ_FP}" ]] ; then
	for CMD in \
	  "gunzip ${AQMEIINA_MSFs_GZ_FP}" \
	; do
	  echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
	  eval "${CMD}" # comment this out for NOPing, e.g., to `source`
	  if [[ $? -ne 0 ]] ; then
	    echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
	    exit 62
	  fi
	done
      fi # if [[ -z "${AQMEIINA_MSFs_GZ_FP}" ]]
    fi # if [[ ! -r "${AQMEIINA_MSFs_FP}" ]]
  fi # if [[ -r "${AQMEIINA_MSFs_FP}" ]]

  if [[ ! -r "${AQMEIINA_MSFs_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: cannot read AQMEIINA_MSFs_FP=='${AQMEIINA_MSFs_FP}'"
    exit 63
  fi
} # end function get_MSFs

### Call NCL helper to get areas of output/AQMEII-NA gridcells.
### Depends on functions={get_get_AQMEIINA_areas, get_MSFs} above.
### Assumes `ncl` is in PATH: see bash_utilities.sh
### TODO: pass commandline args to NCL
### for now: just use envvars :-(
function get_AQMEIINA_areas {
# get_MSFs
# ncl # bail to console, or
  if [[ -z "${AQMEIINA_AREAS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: AQMEIINA_AREAS_FP not defined"
    exit 64
  fi

  if [[ -r "${AQMEIINA_AREAS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: skipping ${FUNCNAME[0]}: output='${AQMEIINA_AREAS_FP}' is readable"
  else # make it
    if [[ -z "${GET_AQMEIINA_AREAS_FP}" ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: GET_AQMEIINA_AREAS_FP not defined"
      exit 65
    fi
    if [[ ! -r "${GET_AQMEIINA_AREAS_FP}" ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: areas-getting helper='${GET_AQMEIINA_AREAS_FP} not readable"
      exit 66
    fi
    for CMD in \
      'get_MSFs' \
      "ncl -n ${GET_AQMEIINA_AREAS_FP}" \
    ; do
    cat <<EOM

${THIS_FN}::${FUNCNAME[0]}: about to run command='${CMD}'.

EOM
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
        exit 67
      fi
    done
  fi # if [[ -r "${AQMEIINA_AREAS_FP}" ]]

  if [[ -r "${AQMEIINA_AREAS_FP}" ]] ; then
    for CMD in \
      "ls -alt ${AQMEIINA_AREAS_FP}" \
      "ncdump -h ${AQMEIINA_AREAS_FP}" \
    ; do
      echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
      eval "${CMD}" # comment this out for NOPing, e.g., to `source`
      if [[ $? -ne 0 ]] ; then
        echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
        exit 68
      fi
    done
  else
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: failed to create AQMEII-NA areas file='${AQMEIINA_AREAS_FP}"
    exit 69
  fi
} # end function get_AQMEIINA_areas

# ----------------------------------------------------------------------
# action functions
# ----------------------------------------------------------------------

### Depends on function get_reunit_vis_EDGAR_global above.
### Assumes `Rscript` is in PATH: see bash_utilities.sh
### TODO: pass commandline args to R
### for now: just use envvars :-(
function reunit_vis_EDGAR_global {
  if [[ -z "${AQMEII_EDGAR_N2O_GLOBAL_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: AQMEII_EDGAR_N2O_GLOBAL_FP not defined"
    exit 70
  elif [[ ! -r "${AQMEII_EDGAR_N2O_GLOBAL_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: input file='${AQMEII_EDGAR_N2O_GLOBAL_FP}' not readable"
    exit 71
  fi
  if [[ -z "${AQMEII_EDGAR_N2O_GLOBAL_REUNIT_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: AQMEII_EDGAR_N2O_GLOBAL_REUNIT_FP not defined"
    exit 72
  fi
  if [[ -z "${AQMEII_EDGAR_N2O_GLOBAL_REUNIT_PDF_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: AQMEII_EDGAR_N2O_GLOBAL_REUNIT_PDF_FP not defined"
    exit 73
  fi

# eval "rm ${AQMEII_EDGAR_N2O_GLOBAL_REUNIT_FP}"
# R # bail to R console and copy script lines, or ...
  if [[ -r "${AQMEII_EDGAR_N2O_GLOBAL_REUNIT_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: skipping ${REUNIT_VIS_EDGAR_GLOBAL_FN}: output='${AQMEII_EDGAR_N2O_GLOBAL_REUNIT_FP}' is readable"
  else # make it
    for CMD in \
      "Rscript ${REUNIT_VIS_EDGAR_GLOBAL_FP}" \
    ; do
    cat <<EOM

About to run command='${CMD}'. WARNING: may seem to hang while processing!

EOM
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
        exit 74
      fi
    done

    if [[ ! -r "${AQMEII_EDGAR_N2O_GLOBAL_REUNIT_FP}" ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: output file='${AQMEII_EDGAR_N2O_GLOBAL_REUNIT_FP}' not readable"
      exit 75
    elif [[ ! -r "${AQMEII_EDGAR_N2O_GLOBAL_REUNIT_PDF_FP}" ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: plot file='${AQMEII_EDGAR_N2O_GLOBAL_REUNIT_PDF_FP}' not readable"
      exit 76
    else
#       "ncdump -h ${AQMEII_EDGAR_N2O_GLOBAL_REUNIT_FP}" \ # do this in script
      for CMD in \
        "${PDF_VIEWER} ${AQMEII_EDGAR_N2O_GLOBAL_REUNIT_PDF_FP} &" \
      ; do
        echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
        eval "${CMD}" # comment this out for NOPing, e.g., to `source`
        if [[ $? -ne 0 ]] ; then
          echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
          exit 77
        fi
      done
    fi # checking readability of outputs
  fi # checking readability of input
} # end function reunit_vis_EDGAR_global

### Depends on function get_regrid_vis_EDGAR_AQMEII above.
### Assumes `Rscript` is in PATH: see bash_utilities.sh
### TODO: pass commandline args to R
### for now: just use envvars :-(
function regrid_vis_EDGAR_AQMEII {
  if [[ -z "${AQMEII_EDGAR_N2O_GLOBAL_REUNIT_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: AQMEII_EDGAR_N2O_GLOBAL_REUNIT_FP not defined"
    exit 78
  elif [[ ! -r "${AQMEII_EDGAR_N2O_GLOBAL_REUNIT_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: input file='${AQMEII_EDGAR_N2O_GLOBAL_REUNIT_FP}' not readable"
    exit 79
  fi
  if [[ -z "${AQMEII_EDGAR_N2O_REGRID_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: AQMEII_EDGAR_N2O_REGRID_FP not defined"
    exit 80
  fi
  if [[ -z "${AQMEII_EDGAR_N2O_REGRID_PDF_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: AQMEII_EDGAR_N2O_REGRID_PDF_FP not defined"
    exit 81
  fi

# eval "rm ${AQMEII_EDGAR_N2O_REGRID_FP}"
# R # bail to R console and copy script lines, or ...
  if [[ -r "${AQMEII_EDGAR_N2O_REGRID_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: skipping ${REGRID_VIS_EDGAR_FN}: output='${AQMEII_EDGAR_N2O_REGRID_FP}' is readable"
  else # make it
    for CMD in \
      "Rscript ${REGRID_VIS_EDGAR_FP}" \
    ; do
    cat <<EOM

About to run command='${CMD}'. WARNING: may seem to hang while processing!

EOM
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
        exit 82
      fi
    done

    if [[ ! -r "${AQMEII_EDGAR_N2O_REGRID_FP}" ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: output file='${AQMEII_EDGAR_N2O_REGRID_FP}' not readable"
      exit 83
    elif [[ ! -r "${AQMEII_EDGAR_N2O_REGRID_PDF_FP}" ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: plot file='${AQMEII_EDGAR_N2O_REGRID_PDF_FP}' not readable"
      exit 84
    else
#       "ncdump -h ${AQMEII_EDGAR_N2O_REGRID_FP}" \ # do this in script
      for CMD in \
        "${PDF_VIEWER} ${AQMEII_EDGAR_N2O_REGRID_PDF_FP} &" \
      ; do
        echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
        eval "${CMD}" # comment this out for NOPing, e.g., to `source`
        if [[ $? -ne 0 ]] ; then
          echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
          exit 85
        fi
      done
    fi # checking readability of outputs
  fi # checking readability of input
} # end function regrid_vis_EDGAR_AQMEII

### Convert the raw BELD CSV to RDS (R serialization format)
function BELD_CSV_to_RDS {
#   echo -e "${THIS_FN}::${FUNCNAME[0]}: TODO!"

  if [[ -z "${BELD_RDS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: BELD_RDS_FP not defined"
    exit 86
  fi
  if [[ -r "${BELD_RDS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: skipping ${BELD_CONVERT_FN}: output='${BELD_RDS_FP}' is readable"
  else # make it

    if [[ -z "${BELD_CSV_FP}" ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: BELD_CSV_FP not defined"
      exit 87
    fi

    if [[ ! -r "${BELD_CSV_FP}" ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: BELD coverages as CSV=='${BELD_CSV_FP}' not readable"
      exit 88
    fi

    if [[ -z "${BELD_READ_FP}" ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: BELD_READ_FP not defined"
      exit 89
    fi

    if [[ ! -r "${BELD_READ_FP}" ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: BELD_READ_FP=='${BELD_READ_FP}' not readable"
      exit 90
    fi

    if [[ -z "${BELD_CONVERT_FP}" ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: BELD_CONVERT_FP not defined"
      exit 91
    fi

    if [[ ! -r "${BELD_CONVERT_FP}" ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: BELD_CONVERT_FP=='${BELD_CONVERT_FP}' not readable"
      exit 92
    fi

#     echo -e "${THIS_FN}::${FUNCNAME[0]}: about to write BELD CSV='${BELD_CSV_FP}' to RDS='${BELD_RDS_FP}'"
#     R
    for CMD in \
      "Rscript ${BELD_CONVERT_FP}" \
    ; do
    cat <<EOM

About to run command='${CMD}'. WARNING: may seem to hang while processing!

EOM
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
        exit 93
      fi
    done
  fi # end if [[ -r "${BELD_RDS_FP}" ]]

  for CMD in \
    "ls -alt ${WORK_DIR}/*.${RDS_EXT} | head" \
  ; do
    echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
    eval "${CMD}" # comment this out for NOPing, e.g., to `source`
    if [[ $? -ne 0 ]] ; then
      echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
      exit 94
    fi
  done
} # end function BELD_CSV_to_RDS

### From raw EPIC, "distill" or purify only the N2O emittivities, and demonotonicize them if necessary, and reunit them.
### Depends on function get_distill_EPIC above.
### Assumes `ncl` is in PATH: see bash_utilities.sh
### TODO: pass commandline args to NCL
### punt: just use envvars :-(
function distill_EPIC {
  if [[ -z "${AQMEII_EPIC_N2O_EMITT_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: AQMEII_EPIC_N2O_EMITT_FP not defined"
    exit 95
  fi
  if [[ -r "${AQMEII_EPIC_N2O_EMITT_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: skipping ${DISTILL_EPIC_FN}: output='${AQMEII_EPIC_N2O_EMITT_FP}' is readable"
  else # make it

    if [[ -z "${EPIC_RAW_EMITT_FP}" ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: EPIC_RAW_EMITT_FP not defined"
      exit 96
    fi

    if [[ ! -r "${EPIC_RAW_EMITT_FP}" ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: raw EPIC emittivities=='${EPIC_RAW_EMITT_FP}' not readable"
      exit 97
    fi

    if [[ -z "${DISTILL_EPIC_FP}" ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: DISTILL_EPIC_FP not defined"
      exit 98
    fi

    if [[ ! -r "${DISTILL_EPIC_FP}" ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: DISTILL_EPIC_FP=='${DISTILL_EPIC_FP}' not readable"
      exit 99
    fi

#     ncl # bail to NCL and copy script lines, or ...
    for CMD in \
      "ncl -n ${DISTILL_EPIC_FP}" \
    ; do
      cat <<EOM

About to run command="${CMD}"

EOM
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
        exit 100
      fi
    done
  fi # end if [[ -r "${AQMEII_EPIC_N2O_EMITT_FP}" ]] ; then

  if [[ -r "${AQMEII_EPIC_N2O_EMITT_FP}" ]] ; then
    for CMD in \
      "ls -alt ${EPIC_RAW_EMITT_FP}" \
      "ls -alt ${AQMEII_EPIC_N2O_EMITT_FP}" \
      "ncdump -h ${AQMEII_EPIC_N2O_EMITT_FP}" \
    ; do
      echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
      eval "${CMD}" # comment this out for NOPing, e.g., to `source`
      if [[ $? -ne 0 ]] ; then
        echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
        exit 101
      fi
    done
  else
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: could not create 'distilled' EPIC emittivities=='${AQMEII_EPIC_N2O_EMITT_FP}'"
    exit 102
  fi

} # end function distill_EPIC

### From "distilled" EPIC emittivities and BELD crop coverages, compute EPIC N2O emissions, and write and visualize them.
### Depends on function get_compute_EPIC above.
### Assumes `Rscript` is in PATH: see bash_utilities.sh
### TODO: pass commandline args to R
### punt: just use envvars :-(
function compute_EPIC {
  ### Have I already named the output?
  if [[ -z "${AQMEII_EPIC_N2O_EMISS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: AQMEII_EPIC_N2O_EMISS_FP not defined"
    exit 103
  fi

  ### Have I already made the output?
  if [[ -r "${AQMEII_EPIC_N2O_EMISS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: skipping ${COMPUTE_EPIC_FN}: output='${AQMEII_EPIC_N2O_EMISS_FP}' is readable"
  else # make the output

    ### input 1 of 2: EPIC emittivities
    if [[ -z "${AQMEII_EPIC_N2O_EMITT_FP}" ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: AQMEII_EPIC_N2O_EMITT_FP not defined"
      exit 104
    fi
    if [[ ! -r "${AQMEII_EPIC_N2O_EMITT_FP}" ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: distilled EPIC N2O emittivities=='${AQMEII_EPIC_N2O_EMITT_FP}' not readable"
      exit 105
    fi

    ### input 2 of 2: BELD crop coverages
    if [[ -z "${BELD_RDS_FP}" ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: BELD_RDS_FP not defined"
      exit 106
    fi
    if [[ ! -r "${BELD_RDS_FP}" ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: BELD coverages in RDS=='${BELD_RDS_FP}' not readable"
      exit 107
    fi

    ### helper
    if [[ -z "${COMPUTE_EPIC_FP}" ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: COMPUTE_EPIC_FP not defined"
      exit 108
    fi
    if [[ ! -r "${COMPUTE_EPIC_FP}" ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: COMPUTE_EPIC_FP=='${COMPUTE_EPIC_FP}' not readable"
      exit 109
    fi

#     echo -e "${THIS_FN}::${FUNCNAME[0]}: about to compute EPIC emissions='${AQMEII_EPIC_N2O_EMISS_FP}'"
#     R
    for CMD in \
      "Rscript ${COMPUTE_EPIC_FP}" \
    ; do
    cat <<EOM

About to run command='${CMD}'. WARNING: may seem to hang while processing!

EOM
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
        exit 110
      fi
    done
  fi # end if [[ -r "${AQMEII_EPIC_N2O_EMISS_FP}" ]]

  if [[ -r "${AQMEII_EPIC_N2O_EMISS_FP}" ]] ; then
# done in R::visualize.layer
#       "ncdump -h ${AQMEII_EPIC_N2O_EMISS_FP}" \
    for CMD in \
      "ls -alt ${AQMEII_EPIC_N2O_EMISS_FP}" \
      "ls -alt ${AQMEII_EPIC_N2O_EMISS_PDF_FP}" \
      "${PDF_VIEWER} ${AQMEII_EPIC_N2O_EMISS_PDF_FP} &" \
    ; do
      echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
      eval "${CMD}" # comment this out for NOPing, e.g., to `source`
      if [[ $? -ne 0 ]] ; then
        echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
        exit 111
      fi
    done
  else
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: could not create EPIC emissions=='${AQMEII_EPIC_N2O_EMISS_FP}'"
    exit 112
  fi

} # end function compute_EPIC

### Combine reunit-ed EPIC and EDGAR N2O emissions, write and visualize the result.
### Depends on function get_combine_emis above.
### Assumes `Rscript` is in PATH: see bash_utilities.sh
### TODO: pass commandline args to R
### punt: just use envvars :-(
function combine_emis {
  ### Have I already named the output?
  if [[ -z "${AQMEII_BOTH_N2O_EMISS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: AQMEII_BOTH_N2O_EMISS_FP not defined"
    exit 113
  fi

  ### Have I already made the output?
  if [[ -r "${AQMEII_BOTH_N2O_EMISS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: skipping ${COMBINE_EMIS_FN}: output='${AQMEII_BOTH_N2O_EMISS_FP}' is readable"
  else # make the output

    ### input 1 of 2: EPIC emissions over AQMEII
    if [[ -z "${AQMEII_EPIC_N2O_EMISS_FP}" ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: AQMEII_EPIC_N2O_EMISS_FP not defined"
      exit 114
    fi
    if [[ ! -r "${AQMEII_EPIC_N2O_EMISS_FP}" ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: EPIC N2O emissions over AQMEII=='${AQMEII_EPIC_N2O_EMISS_FP}' not readable"
      exit 115
    fi

    ### input 2 of 2: EDGAR emissions over AQMEII
    if [[ -z "${AQMEII_EDGAR_N2O_REGRID_FP}" ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: AQMEII_EDGAR_N2O_REGRID_FP not defined"
      exit 116
    fi
    if [[ ! -r "${AQMEII_EDGAR_N2O_REGRID_FP}" ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: EDGAR emissions over AQMEII=='${AQMEII_EDGAR_N2O_REGRID_FP}' not readable"
      exit 117
    fi

    ### helper
    if [[ -z "${COMBINE_EMIS_FP}" ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: COMBINE_EMIS_FP not defined"
      exit 118
    fi
    if [[ ! -r "${COMBINE_EMIS_FP}" ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: COMBINE_EMIS_FP=='${COMBINE_EMIS_FP}' not readable"
      exit 119
    fi

#     echo -e "${THIS_FN}::${FUNCNAME[0]}: about to bypass ${COMBINE_EMIS_FP}"
#     R
    for CMD in \
      "Rscript ${COMBINE_EMIS_FP}" \
    ; do
    cat <<EOM

About to run command='${CMD}'. WARNING: may seem to hang while processing!

EOM
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
        exit 120
      fi
    done
  fi # end if [[ -r "${AQMEII_BOTH_N2O_EMISS_FP}" ]]

  if [[ -r "${AQMEII_BOTH_N2O_EMISS_FP}" ]] ; then
# done in R::visualize.layer
#       "ncdump -h ${AQMEII_BOTH_N2O_EMISS_FP}" \
    for CMD in \
      "ls -alt ${AQMEII_BOTH_N2O_EMISS_FP}" \
      "ls -alt ${AQMEII_BOTH_N2O_EMISS_PDF_FP}" \
      "${PDF_VIEWER} ${AQMEII_BOTH_N2O_EMISS_PDF_FP} &" \
    ; do
      echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
      eval "${CMD}" # comment this out for NOPing, e.g., to `source`
      if [[ $? -ne 0 ]] ; then
        echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
        exit 121
      fi
    done
  else
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: could not create combined emissions=='${AQMEII_BOTH_N2O_EMISS_FP}'"
    exit 122
  fi

} # end function combine_emis

### Create (even more) CMAQ-friendly 25-hour *.ncf files.
### Depends on function get_make_hourlies above.
### Assumes `ncl` is in PATH: see bash_utilities.sh
### TODO: pass commandline args to NCL
### punt: just use envvars :-(
function make_hourlies {
  if [[ -z "${AQMEII_HOURLY_N2O_EMISS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: AQMEII_HOURLY_N2O_EMISS_FP not defined"
    exit 123
  fi
  if [[ -z "${AQMEII_HOURLY_N2O_EMISS_FP_CMAQ}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: AQMEII_HOURLY_N2O_EMISS_FP_CMAQ not defined"
    exit 124
  fi
  if [[ -r "${AQMEII_HOURLY_N2O_EMISS_FP_CMAQ}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: skipping ${MAKE_HOURLIES_FN}: output='${AQMEII_HOURLY_N2O_EMISS_FP_CMAQ}' is readable"
  else # make it ...

    # ... for which we need our input
    if [[ -z "${AQMEII_BOTH_N2O_EMISS_FP}" ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: AQMEII_BOTH_N2O_EMISS_FP not defined"
      exit 125
    fi

    if [[ ! -r "${AQMEII_BOTH_N2O_EMISS_FP}" ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: raw EPIC emittivities=='${AQMEII_BOTH_N2O_EMISS_FP}' not readable"
      exit 126
    fi

    if [[ -z "${MAKE_HOURLIES_FP}" ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: MAKE_HOURLIES_FP not defined"
      exit 127
    fi

    if [[ ! -r "${MAKE_HOURLIES_FP}" ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: MAKE_HOURLIES_FP=='${MAKE_HOURLIES_FP}' not readable"
      exit 128
    fi

#     ncl # bail to NCL and copy script lines, or ...
    for CMD in \
      "ncl -n ${MAKE_HOURLIES_FP}" \
      "mv ${AQMEII_HOURLY_N2O_EMISS_FP} ${AQMEII_HOURLY_N2O_EMISS_FP_CMAQ}" \
    ; do
      cat <<EOM

About to run command="${CMD}"

EOM
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
        exit 129
      fi
    done
  fi # end if [[ -r "${AQMEII_HOURLY_N2O_EMISS_FP_CMAQ}" ]] ; then

  if [[ -r "${AQMEII_HOURLY_N2O_EMISS_FP_CMAQ}" ]] ; then
    # get/run VERDI command here rather than NCL, because we move the NCL-happy output before the NCL-launched process can get to it
    AQMEII_HOURLY_N2O_EMISS_FP_CMAQ_FQN="$(readlink -f ${AQMEII_HOURLY_N2O_EMISS_FP_CMAQ})" # VERDI really, really wants absolute paths
    AQMEII_HOURLY_N2O_EMISS_DATAVAR_NAME_VERDI="${AQMEII_HOURLY_N2O_EMISS_DATAVAR_NAME}[1]"
    AQMEII_HOURLY_N2O_EMISS_VERDI_CMD="verdi -f ${AQMEII_HOURLY_N2O_EMISS_FP_CMAQ_FQN} -s ${AQMEII_HOURLY_N2O_EMISS_DATAVAR_NAME_VERDI} -gtype tile &"

    for CMD in \
      "ls -alt ${AQMEII_HOURLY_N2O_EMISS_FP_CMAQ}" \
      "ncdump -h ${AQMEII_HOURLY_N2O_EMISS_FP_CMAQ}" \
      "${AQMEII_HOURLY_N2O_EMISS_VERDI_CMD}" \
    ; do
      echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
      eval "${CMD}" # comment this out for NOPing, e.g., to `source`
      if [[ $? -ne 0 ]] ; then
        echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
        exit 130
      fi
    done
  else
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: could not create hourly combined emissions=='${AQMEII_HOURLY_N2O_EMISS_FP_CMAQ}'"
    exit 131
  fi

} # end function make_hourlies

### Compare masses emitted by our various products to each other and to the EDGAR global ag-soil emissions.
### Depends on function get_check_conservation above.
### Assumes `ncl` is in PATH: see bash_utilities.sh
### TODO: pass commandline args to NCL
### for now: just use envvars :-(
function check_conservation {

  if [[ ! -r "${AQMEII_EDGAR_N2O_GLOBAL_REUNIT_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: cannot read EPIC emissions=='${AQMEII_EDGAR_N2O_GLOBAL_REUNIT_PDF_FP}'"
    exit 132
  fi

  if [[ ! -r "${AQMEII_EDGAR_N2O_GLOBAL_REUNIT_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: cannot read EPIC emissions=='${AQMEII_EDGAR_N2O_GLOBAL_REUNIT_PDF_FP}'"
    exit 133
  fi

  if [[ ! -r "${AQMEII_EPIC_N2O_EMISS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: cannot read EPIC emissions=='${AQMEII_EPIC_N2O_EMISS_FP}'"
    exit 134
  fi

  if [[ ! -r "${AQMEII_HOURLY_N2O_EMISS_FP_CMAQ}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: cannot read hourly combined emissions=='${AQMEII_HOURLY_N2O_EMISS_FP_CMAQ}'"
    exit 135
  fi

# ncl==bail to NCL and copy script lines, or ...
#     "ncl" \
  for CMD in \
    "cp ${AQMEII_HOURLY_N2O_EMISS_FP_CMAQ} ${AQMEII_HOURLY_N2O_EMISS_FP_NCL}" \
    "ncl -n ${CHECK_CONSERVATION_FP}" \
    "rm ${AQMEII_HOURLY_N2O_EMISS_FP_NCL}" \
  ; do
    echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
    eval "${CMD}" # comment this out for NOPing, e.g., to `source`
  done
} # end function check_conservation

function teardown {
  for CMD in \
    "ls -alt ${WORK_DIR}/*.${NETCDF_EXT_NCL} | head" \
    "ls -alt ${WORK_DIR}/*.${NETCDF_EXT_CMAQ} | head" \
    "ls -alt ${WORK_DIR}/*.pdf | head" \
  ; do
    echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
    eval "${CMD}" # comment this out for NOPing, e.g., to `source`
    if [[ $? -ne 0 ]] ; then
      echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
      exit 136
    fi
  done
} # end function teardown

# ----------------------------------------------------------------------
# payload
# ----------------------------------------------------------------------

#   'setup' \                 # paths, helpers, etc
#   'reunit_vis_EDGAR_global' # reunit EDGAR flux rate -> mol rate, visualize after
#   'regrid_vis_EDGAR_AQMEII' # regrid EDGAR global -> AQMEII, visualize after
#   'BELD_CSV_to_RDS' \       # Convert the raw BELD CSV to RDS
#   'distill_EPIC' \          # Separate EPIC N2O emittivities and demonotonicize
#   'compute_EPIC' \          # Compute EPIC N2O emissions, write, and visualize
#   'combine_emis' \          # EPIC values replace EDGAR where both exist
#   'make_hourlies' \         # create our real output: hourly emissions for CMAQ
#   'check_conservation' \    # of mass from inputs to outputs, and check EPIC vs EDGAR values
#   'teardown' \
# should always
# * begin with `setup` to do `module add`
# * end with `teardown` for tidy and testing (e.g., plot display)
for CMD in \
  'setup' \
  'reunit_vis_EDGAR_global' \
  'regrid_vis_EDGAR_AQMEII' \
  'BELD_CSV_to_RDS' \
  'distill_EPIC' \
  'compute_EPIC' \
  'combine_emis' \
  'make_hourlies' \
  'check_conservation' \
  'teardown' \
; do
  echo -e "\n$ ${THIS_FN}::main loop::${CMD}\n"
  eval "${CMD}" # comment this out for NOPing, e.g., to `source`
  if [[ $? -ne 0 ]] ; then
    echo -e "$ ${THIS_FN}::main loop::${CMD}: ERROR: failed or not found\n"
    exit 137
  fi
done

# ----------------------------------------------------------------------
# debugging
# ----------------------------------------------------------------------
