### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

# R code to
# * reunit an EDGAR global/unprojected inventory from EDGAR (via this project, since there's no direct URI)
# * visualize it

# If running manually in R console, remember to run setup actions: `source ./AQMEII_driver.sh`

# Code slightly edited from EDGAR-4.2_minus_soil_and_biomass/sum_reunit.r
# TODO: refactor, package

# ----------------------------------------------------------------------
# constants
# ----------------------------------------------------------------------

# all the following env vars must be set and exported in driver script
# TODO: pass as args!

### miscellany

## kludge for my clumsy namespacing
my_this_fn <- Sys.getenv('REUNIT_VIS_EDGAR_GLOBAL_FN')
this_fn <- my_this_fn

work_dir <- Sys.getenv('WORK_DIR')

## unit conversion
molar_mass_n2o <- as.numeric(Sys.getenv('MOLAR_MASS_N2O')) # units=g/mol
g.per.kg <- 1000 # grams/kilogram, unitless

## projection
global_proj4 <- Sys.getenv('GLOBAL_PROJ4')

## visualization
sigdigs <- as.numeric(Sys.getenv('OUTPUT_SIGNIFICANT_DIGITS'))

# helpers
stats_funcs_fp <- Sys.getenv('STATS_FUNCS_FP')
vis_funcs_fp <- Sys.getenv('VIS_FUNCS_FP')

# plotting
pdf_er <- Sys.getenv('PDF_VIEWER')
pdf_height <- as.numeric(Sys.getenv('SINGLE_FRAME_PLOT_HEIGHT'))
pdf_width <- as.numeric(Sys.getenv('SINGLE_FRAME_PLOT_WIDTH'))

### raw EDGAR input data

## N2O inventory
in_fp <- Sys.getenv('AQMEII_EDGAR_N2O_GLOBAL_FP')
raster_rotate <- as.logical( Sys.getenv('AQMEII_EDGAR_N2O_GLOBAL_ROTATE_LONS'))
# in_band <- Sys.getenv('AQMEII_EDGAR_N2O_GLOBAL_REGRID_BAND')
in_datavar_name <- Sys.getenv('AQMEII_EDGAR_N2O_GLOBAL_DV_NAME')

## gridcell areas
areas_fp <- Sys.getenv('AQMEII_EDGAR_AREAS_GLOBAL_FP')
areas_datavar_name <- Sys.getenv('AQMEII_EDGAR_AREAS_GLOBAL_DV_NAME')

### reunit-ed EDGAR data (flux rate -> mole rate)

out_fp <- Sys.getenv('AQMEII_EDGAR_N2O_GLOBAL_REUNIT_FP')
out_datavar_name <- Sys.getenv('AQMEII_EDGAR_N2O_GLOBAL_REUNIT_DV_NAME')
out_datavar_type <- Sys.getenv('AQMEII_EDGAR_N2O_GLOBAL_REUNIT_DV_TYPE')
out_datavar_units <- Sys.getenv('AQMEII_EDGAR_N2O_GLOBAL_REUNIT_DV_UNITS')
out_datavar_long_name <- Sys.getenv('AQMEII_EDGAR_N2O_GLOBAL_REUNIT_DV_LONG_NAME')
out_datavar_coord_x_name <- Sys.getenv('AQMEII_EDGAR_N2O_GLOBAL_REUNIT_DV_COORD_X_NAME')
out_datavar_coord_y_name <- Sys.getenv('AQMEII_EDGAR_N2O_GLOBAL_REUNIT_DV_COORD_Y_NAME')

### plotting reunit-ed EDGAR data

out_pdf_fp <- Sys.getenv('AQMEII_EDGAR_N2O_GLOBAL_REUNIT_PDF_FP')

# ----------------------------------------------------------------------
# functions
# ----------------------------------------------------------------------

# start debugging-------------------------------------------------------
cat(sprintf('%s: about to load stats script=%s\n', this_fn, stats_funcs_fp))
cat(sprintf('%s: about to load vis script=%s\n', this_fn, vis_funcs_fp))
#   end debugging-------------------------------------------------------

source(stats_funcs_fp) # in script, produces errant error=
#> netCDF.stats.to.stdout.r: no arguments supplied, exiting
source(vis_funcs_fp)

# ----------------------------------------------------------------------
# code
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# setup
# ----------------------------------------------------------------------

# accelerate R graphics over SSH, per Adam Wilson
# http://planetflux.adamwilson.us/2012/03/r-graphics-via-ssh.html
X11.options(type="Xlib")

### setup maps

# get a global map
library(maps)
map.world.unproj <- maps::map('world', plot=FALSE)

# get a North American map
library(maptools)
data(wrld_simpl) # from maptools

# coordinate reference system:
global.crs <- sp::CRS(global_proj4)

cat(sprintf('%s: creating world map\n', this_fn)) # debugging
map.world.unproj.shp <-
  maptools::map2SpatialLines(map.world.unproj, proj4string=global.crs)
# summary(map.world.unproj.shp) # debugging

# ----------------------------------------------------------------------
# payload
# ----------------------------------------------------------------------

cat(sprintf('%s: creating input raster\n', this_fn)) # debugging
library(raster)
in.raster <- raster::raster(in_fp, varname=in_datavar_name)

# # can't rotate here: makes extents mismatch with areas
# if (raster_rotate) {
#   in.raster <- rotate(
#     in.raster,      # )
#     overwrite=TRUE) # else levelplot does one layer per page?
# }

# # start debugging-----------------------------------------------------
# in.raster
# # class       : RasterLayer 
# # dimensions  : 1800, 3600, 6480000  (nrow, ncol, ncell)
# # resolution  : 0.1, 0.1  (x, y)
# # extent      : -9.507305e-10, 360, -90, 90  (xmin, xmax, ymin, ymax)
# # coord. ref. : +proj=longlat +datum=WGS84 
# # data source : .../v42_N2O_2008_IPCC_4C_4D.0.1x0.1.nc 
# # names       : Emissions.of.N2O.. 
# # zvar        : emi_n2o

# in.data <- values(in.raster)
# in.data.n <- length(in.data)
# in.data.obs <- c(in.data[!is.na(in.data)])
# in.data.obs.n <- length(in.data.obs)

# stats.to.stdout(
#   data.nonNA=in.data.obs,
#   data.nonNA.n=in.data.obs.n,
#   data.raw.n=in.data.n,
#   sig.digs=sigdigs,
#   title='in.raster data:')
# # in.raster data:
# #       cells=6480000
# #       obs=6480000
# #       min=0
# #       q1=0
# #       med=0
# #       mean=2.23e-13
# #       q3=0
# #       max=3.85e-10
# #       sum=1.44e-06
# #   end debugging-----------------------------------------------------

### "massify" input: convert flux-rate values to molar-mass-rate values
cat(sprintf( # debugging
  '%s: converting input raster from flux rate to molar-mass rate (may take awhile! TODO: add progress control)\n',
  this_fn))

## flux rate (kg/m^2/s) * gridcell area -> mass rate (kg/s)
# fortunately, gridcell areas are provided by EDGAR
areas.raster <- raster::raster(areas_fp, varname=areas_datavar_name)

# # start debugging-----------------------------------------------------
# areas.data <- values(areas.raster)
# areas.data.n <- length(areas.data)
# areas.data.obs <- c(areas.data[!is.na(areas.data)])
# areas.data.obs.n <- length(areas.data.obs)

# stats.to.stdout(
#   data.nonNA=areas.data.obs,
#   data.nonNA.n=areas.data.obs.n,
#   data.raw.n=areas.data.n,
#   sig.digs=sigdigs,
#   title='areas.raster data:')
# # areas.raster data:
# #       cells=6480000
# #       obs=6480000
# #       min=1.08e+05
# #       q1=4.74e+07
# #       med=8.74e+07
# #       mean=7.87e+07
# #       q3=1.14e+08
# #       max=1.24e+08
# #       sum=5.1e+14
# #   end debugging-----------------------------------------------------

# re `prod`:
# * does cell-by-cell matrix multiply, not linear-algebra-style
# * is generic method
mass.raster <- prod(in.raster, areas.raster)

## mass rate (kg/s) -> molar rate (mol/s)
# (kg/s) * (g/kg) * (mol/g) -> (mol/s)
out.raster <- (mass.raster * g.per.kg) / molar_mass_n2o

### 0-360 longitudes -> -180-+180
if (raster_rotate) {
  out.raster <- rotate(
    out.raster,      # )
    overwrite=TRUE) # else levelplot does one layer per page?
}

# # start debugging-----------------------------------------------------
# out.raster
# # class       : RasterLayer 
# # dimensions  : 1800, 3600, 6480000  (nrow, ncol, ncell)
# # resolution  : 0.1, 0.1  (x, y)
# # extent      : -180, 180, -90, 90  (xmin, xmax, ymin, ymax)
# # coord. ref. : +proj=longlat +datum=WGS84 
# # data source : in memory
# # names       : layer 
# # values      : 0, 1.00291  (min, max)

# out.data <- values(out.raster)
# out.data.n <- length(out.data)
# out.data.obs <- c(out.data[!is.na(out.data)])
# out.data.obs.n <- length(out.data.obs)

# stats.to.stdout(
#   data.nonNA=out.data.obs,
#   data.nonNA.n=out.data.obs.n,
#   data.raw.n=out.data.n,
#   sig.digs=sigdigs,
#   title='out.raster data:')
# # out.raster data:
# #       cells=6480000
# #       obs=6480000
# #       min=0
# #       q1=0
# #       med=0
# #       mean=0.000518
# #       q3=0
# #       max=1
# #       sum=3.35e+03
# #   end debugging-----------------------------------------------------

cat(sprintf('%s: writing summed-and-massed raster to path=%s ...\n', # debugging
  this_fn, out_fp))
writeRaster(
  x=out.raster, filename=out_fp, varname=out_datavar_name,
  format="CDF", overwrite=TRUE,
# let raster choose default, since "there shouldn't be any"
#  NAflag=out_datavar_na,
  # datavar type will truncate unless set?
  datatype=out_datavar_type,
  # netCDF-specific arguments
  varunit=out_datavar_units, longname=out_datavar_long_name,
  xname=out_datavar_coord_x_name, yname=out_datavar_coord_y_name
)
# class       : RasterLayer 
# dimensions  : 1800, 3600, 6480000  (nrow, ncol, ncell)
# resolution  : 0.1, 0.1  (x, y)
# extent      : -180, 180, -90, 90  (xmin, xmax, ymin, ymax)
# coord. ref. : +proj=longlat +datum=WGS84 
# data source : /home/rtd/code/regridding/AQMEII_ag_soil/v42_N2O_2008_IPCC_4C_4D.0.1x0.1_reunit.nc 
# names       : Emissions.of.N2O.. 
# zvar        : emi_n2o 

cat(sprintf('%s: plotting output=%s to %s...\n', # debugging
  this_fn, out_fp, out_pdf_fp))
# Why does '=' fail and '<-' succeed in the arg list?
visualize.layer(
  nc.fp=out_fp,
  datavar.name=out_datavar_name,
  sigdigs=sigdigs,
  layer=out.raster,
#  map.list=list(map.world.unproj.shp),
  map.list <- list(map.world.unproj.shp),
  pdf.fp=out_pdf_fp,
  pdf.height=pdf_height,
  pdf.width=pdf_width
)
# For ./v42_N2O_2008_IPCC_4C_4D.0.1x0.1_reunit.nc var=emi_n2o
#       cells=6480000
#       obs=6480000
#       min=0
#       q1=0
#       med=0
#       mean=0.000518
#       q3=0
#       max=1
#       sum=3.35e+03
