#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

# This script clones and runs code either from
# * the `git` repository @
# https://tlroche@bitbucket.org/tlroche/aqmeii_ag_soil
#   You probably wanna check its README before running this.

# * the current folder (for code you haven't committed).

# If you decide to run this, you should first

# * un/comment var=ACCESS to reflect your available/desired file-transfer protocol
# * if using ACCESS='file', edit FILE_LIST below
# * edit var=REPO_ROOT to state where to create your repo clone
# * remember to enable netCDF-4 on your system, e.g.
# > module add netcdf-4.1.2 # on terrae (a cluster on which I work)

# and run this like
# START="$(date)" ; echo -e "START=${START}" ; rm -fr /tmp/aqmeii_ag_soil/ ; ./uber_driver.sh ; END="$(date)" ; echo -e "  END=${END}" ; echo -e "START=${START}"

THIS=$0
THIS_FN="$(basename ${THIS})"

REPO_ROOT='/tmp' # or location of your choice: repo/workspace created in subdir
PROJECT_NAME='aqmeii_ag_soil'  # TODO: get from commandline
REPO_DIR="${REPO_ROOT}/${PROJECT_NAME}"
# what actually does the work
DRIVER_FN='AQMEII_driver.sh'   # TODO: get from commandline

# # following are "booleans": comment out or change value if !TRUE
# # Does our platform allow git/ssh? (e.g., terrae does not)
# CAN_GIT_SSH='TRUE'
# # Does our platform have HTTP certificates? (e.g., terrae does not)
# HAVE_HTTP_CERTS='TRUE'
# if latter is false, git must not check for certificates
if [[ -z "${HAVE_HTTP_CERTS}" || "${HAVE_HTTP_CERTS}" != 'TRUE' ]] ; then
  # set for ${DRIVER_FN}
  export GIT_CLONE_PREFIX='env GIT_SSL_NO_VERIFY=true'
else
  export GIT_CLONE_PREFIX=''
fi

# TODO: get from commandline
ACCESS='file'       # use uncommitted code in current directory
# ACCESS=='file' uses no repository
# ACCESS='git/ssh'    # outgoing ssh works (preferred if available)
# REPO_URI_GIT="git@bitbucket.org:tlroche/${PROJECT_NAME}.git"
# ACCESS='http+cert'  # outgoing ssh blocked, but you have certificates
# REPO_URI_HTTP="https://tlroche@bitbucket.org/tlroche/${PROJECT_NAME}.git"
# ACCESS='http-cert'  # (terrae) outgoing ssh blocked, no certs installed
# also uses REPO_URI_HTTP

# TODO: get/use from ${DRIVER_FN}
EPIC_INPUT_FP='./5yravg_20111219.nc' # too big to download :-(

# if [[ "${ACCESS}" == 'file' ]], you'll need
#           ./area_0.1x0.1.nc \ # convenience only: download from EDGAR is slow
FILE_LIST="./${DRIVER_FN} \
           ${EPIC_INPUT_FP} \
           ./area_0.1x0.1.nc \
           ./BELD_CSV_to_RDS.r \
           ./check_conservation.ncl \
           ./combine_EDGAR_and_EPIC_emissions.r \
           ./compute_EPIC_emissions.r \
           ./distill_EPIC_emittivities.ncl \
           ./make_hourlies.ncl \
           ./read_BELD_CSV.r \
           ./regrid_vis.r \
           ./reunit_vis_EDGAR_global.r "
# Note: driver will download area_0.1x0.1.nc, but I copy it here for speed.
# TODO: get members(${FILE_LIST}) from ${DRIVER_FN}

if [[ -z "${REPO_DIR}" ]] ; then
  echo -e "${THIS_FN}:ERROR: REPO_DIR not defined"
  exit 1
fi
if [[ -d "${REPO_DIR}" ]] ; then
  echo -e "${THIS_FN}:ERROR? repo dir='${REPO_DIR}' exists: move or delete it before running this script"
  exit 2
fi

### USER BEWARE: following `if`s should probably be a `case`

if [[ "${ACCESS}" == 'file' ]] ; then
  if [[ ! -x "./${DRIVER_FN}" ]] ; then
    echo -e "${THIS_FN}:ERROR: ACCESS==file, but driver=='${DRIVER_FN}' not executable"
    exit 3
  else
    for CMD in \
      "mkdir -p ${REPO_DIR}" \
      "cp ${FILE_LIST} ${REPO_DIR}/ " \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
          echo -e "${THIS_FN}::access=file::${CMD}: ERROR: failed\n"
          exit 4
      fi
    done
  fi
fi

if   [[ "${ACCESS}" == 'git/ssh' ]] ; then
  if [[ -n "${CAN_GIT_SSH}" && "${CAN_GIT_SSH}" == 'TRUE' ]] ; then
    REPO_URI="${REPO_URI_GIT}"
  else
    echo -e "${THIS_FN}: ERROR: cannot use access==git/ssh on this platform\n"
    exit 5
  fi
elif [[ "${ACCESS}" == 'http+cert' ]] ; then
  if [[ -n "${HAVE_HTTP_CERTS}" && "${HAVE_HTTP_CERTS}" == 'TRUE' ]] ; then
    REPO_URI="${REPO_URI_HTTP}"
  else
    echo -e "${THIS_FN}: ERROR: cannot use access==http+cert on this platform\n"
    exit 6
  fi
elif [[ "${ACCESS}" == 'http-cert' ]] ; then
  REPO_URI="${REPO_URI_HTTP}"
fi

if [[ -z "${ACCESS}" ]] ; then
    echo -e "${THIS_FN}: ERROR: ACCESS not defined: did you uncomment one of the choices above?"
    exit 7
elif [[ "${ACCESS}" != 'file' ]] ; then
# deal with EPIC input after creating repo
#    "cp ~/code/regridding/aqmeii-na_n2o_integration/compute_EPIC_emissions.r ${REPO_DIR}/ " \
  for CMD in \
    "pushd ${REPO_ROOT}" \
    "${GIT_CLONE_PREFIX} git clone ${REPO_URI}" \
    "popd" \
    "cp ${EPIC_INPUT_FP} ${REPO_DIR}/ " \
  ; do
    echo -e "$ ${CMD}"
    eval "${CMD}"
    if [[ $? -ne 0 ]] ; then
        echo -e "${THIS_FN}::access != file::${CMD}: ERROR: failed\n"
        exit 8
    fi
  done
fi

pushd ${REPO_DIR}
./${DRIVER_FN}
