### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

# R code to
# * 2D-regrid an EDGAR global/unprojected inventory to AQMEII-NA, an LCC-projected subdomain
# * visualize it

# If running manually in R console, remember to run setup actions: `source ./AQMEII_driver.sh`

# Code closely resembles ./reunit_vis_EDGAR_global.r, GFED-3.1_global_to_AQMEII-NA/vis_regrid_vis.r
# TODO: refactor, package

# ----------------------------------------------------------------------
# constants
# ----------------------------------------------------------------------

# all the following env vars must be set and exported in driver script
# TODO: pass as args!

### miscellany

## kludge for my clumsy namespacing
my_this_fn <- Sys.getenv('REGRID_VIS_EDGAR_FN')
this_fn <- my_this_fn

work_dir <- Sys.getenv('WORK_DIR')

## projection
global_proj4 <- Sys.getenv('GLOBAL_PROJ4')

## visualization
sigdigs <- as.numeric(Sys.getenv('OUTPUT_SIGNIFICANT_DIGITS'))

# helpers
stats_funcs_fp <- Sys.getenv('STATS_FUNCS_FP')
vis_funcs_fp <- Sys.getenv('VIS_FUNCS_FP')

# plotting
pdf_er <- Sys.getenv('PDF_VIEWER')
pdf_height <- as.numeric(Sys.getenv('SINGLE_FRAME_PLOT_HEIGHT'))
pdf_width <- as.numeric(Sys.getenv('SINGLE_FRAME_PLOT_WIDTH'))

### input: reunit-ed EDGAR global data

in_fp <- Sys.getenv('AQMEII_EDGAR_N2O_GLOBAL_REUNIT_FP')
in_datavar_name <- Sys.getenv('AQMEII_EDGAR_N2O_GLOBAL_REUNIT_DV_NAME')
in_datavar_type <- Sys.getenv('AQMEII_EDGAR_N2O_GLOBAL_REUNIT_DV_TYPE')
in_datavar_units <- Sys.getenv('AQMEII_EDGAR_N2O_GLOBAL_REUNIT_DV_UNITS')
in_datavar_long_name <- Sys.getenv('AQMEII_EDGAR_N2O_GLOBAL_REUNIT_DV_LONG_NAME')
in_datavar_coord_x_name <- Sys.getenv('AQMEII_EDGAR_N2O_GLOBAL_REUNIT_DV_COORD_X_NAME')
in_datavar_coord_y_name <- Sys.getenv('AQMEII_EDGAR_N2O_GLOBAL_REUNIT_DV_COORD_Y_NAME')
in_raster_rotate <- as.logical( Sys.getenv('AQMEII_EDGAR_N2O_GLOBAL_REUNIT_ROTATE_LONS'))

### input: CMAQ AQMEII-NA template file

template_input_fp <- Sys.getenv('TEMPLATE_INPUT_FP')
template_datavar_name <- Sys.getenv('TEMPLATE_DATAVAR_NAME')

### output: EDGAR data regridded from global/unprojected to AQMEII-NA, an LCC-projected subdomain

out_fp <- Sys.getenv('AQMEII_EDGAR_N2O_REGRID_FP')
out_datavar_name <- Sys.getenv('AQMEII_EDGAR_N2O_REGRID_DV_NAME')
out_datavar_type <- Sys.getenv('AQMEII_EDGAR_N2O_REGRID_DV_TYPE')
out_datavar_units <- Sys.getenv('AQMEII_EDGAR_N2O_REGRID_DV_UNITS')
out_datavar_long_name <- Sys.getenv('AQMEII_EDGAR_N2O_REGRID_DV_LONG_NAME')
out_datavar_dim_x_name <- Sys.getenv('AQMEII_EDGAR_N2O_REGRID_DIM_X_NAME')
out_datavar_dim_y_name <- Sys.getenv('AQMEII_EDGAR_N2O_REGRID_DIM_Y_NAME')
out_datavar_dim_z_name <- Sys.getenv('AQMEII_EDGAR_N2O_REGRID_DIM_TSTEP_NAME')
out_datavar_dim_z_units <- Sys.getenv('AQMEII_EDGAR_N2O_REGRID_DIM_TSTEP_UNITS')

### plotting regridded EDGAR data

out_pdf_fp <- Sys.getenv('AQMEII_EDGAR_N2O_REGRID_PDF_FP')

# ----------------------------------------------------------------------
# functions
# ----------------------------------------------------------------------

# start debugging-------------------------------------------------------
cat(sprintf('%s: about to load stats script=%s\n', this_fn, stats_funcs_fp))
cat(sprintf('%s: about to load vis script=%s\n', this_fn, vis_funcs_fp))
#   end debugging-------------------------------------------------------

source(stats_funcs_fp) # in script, produces errant error=
#> netCDF.stats.to.stdout.r: no arguments supplied, exiting
source(vis_funcs_fp)

# ----------------------------------------------------------------------
# code
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# setup
# ----------------------------------------------------------------------

# accelerate R graphics over SSH, per Adam Wilson
# http://planetflux.adamwilson.us/2012/03/r-graphics-via-ssh.html
X11.options(type="Xlib")

### visualization

### input visualization
global.crs <- sp::CRS(global_proj4)

### output visualization

# output coordinate reference system:
# use package=M3 to get CRS from template file
library(M3)
out.proj4 <- M3::get.proj.info.M3(template_input_fp)
# cat(sprintf('out.proj4=%s\n', out.proj4)) # debugging
# out.proj4=+proj=lcc +lat_1=33 +lat_2=45 +lat_0=40 +lon_0=-97 +a=6370000 +b=6370000
out.crs <- sp::CRS(out.proj4)

# ----------------------------------------------------------------------
# payload
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# load input: reunit-ed global EDGAR
# ----------------------------------------------------------------------

cat(sprintf('%s: creating input raster\n', this_fn)) # debugging
library(raster)
in.raster <- raster::raster(in_fp, varname=in_datavar_name)

if (in_raster_rotate) {
  in.raster <- rotate(
    in.raster,      # )
    overwrite=TRUE) # else levelplot does one layer per page?
}

# # start debugging-----------------------------------------------------
# in.raster
# # class       : RasterLayer 
# # dimensions  : 1800, 3600, 6480000  (nrow, ncol, ncell)
# # resolution  : 0.1, 0.1  (x, y)
# # extent      : -180, 180, -90, 90  (xmin, xmax, ymin, ymax)
# # coord. ref. : +proj=longlat +datum=WGS84 
# # data source : .../v42_N2O_2008_IPCC_4C_4D.0.1x0.1_reunit.nc 
# # names       : Emissions.of.N2O.. 
# # zvar        : emi_n2o 

# in.data <- values(in.raster)
# in.data.n <- length(in.data)
# in.data.obs <- c(in.data[!is.na(in.data)])
# in.data.obs.n <- length(in.data.obs)

# stats.to.stdout(
#   data.nonNA=in.data.obs,
#   data.nonNA.n=in.data.obs.n,
#   data.raw.n=in.data.n,
#   sig.digs=sigdigs,
#   title='in.raster data:')
# # in.raster data:
# #     cells=6480000
# #     obs=6480000
# #     min=0
# #     q1=0
# #     med=0
# #     mean=0.000518
# #     q3=0
# #     max=1
# #     sum=3.35e+03
# #   end debugging-----------------------------------------------------

# ----------------------------------------------------------------------
# load regridding template
# ----------------------------------------------------------------------

# # start debugging
# # long IOAPI schema and I don't care about its layers
# cmd <- sprintf('ncdump -h %s | head -n 13', template_input_fp)
# cat(sprintf('\n: template file: %s==\n', cmd))
# system(cmd)
# # netcdf emis_mole_all_20080101_12US1_cmaq_cb05_soa_2008ab_08c.EXTENTS_INPUT {
# # dimensions:
# #       TSTEP = UNLIMITED ; // (25 currently)
# #       LAY = 1 ;
# #       ROW = 299 ;
# #       COL = 459 ;
# # variables:
# #       float emi_n2o(TSTEP, LAY, ROW, COL) ;
# #               emi_n2o:long_name = "XYL             " ;
# #               emi_n2o:units = "moles/s         " ;
# #               emi_n2o:var_desc = "Model species XYL                                                               " ;
# # 
# # // global attributes:
# #   end debugging

# use package=M3 to get extents from template file (thanks CGN!)
extents.info <- M3::get.grid.info.M3(template_input_fp)
extents.xmin <- extents.info$x.orig
extents.xmax <- max(
  M3::get.coord.for.dimension(
    file=template_input_fp, dimension="col", position="upper", units="m")$coords)
extents.ymin <- extents.info$y.orig
extents.ymax <- max(
  M3::get.coord.for.dimension(
    file=template_input_fp, dimension="row", position="upper", units="m")$coords)
grid.res <- c(extents.info$x.cell.width, extents.info$y.cell.width) # units=m

template.extents <-
  raster::extent(extents.xmin, extents.xmax, extents.ymin, extents.ymax)
# template.extents # debugging
# # class       : Extent 
# # xmin        : -2556000 
# # xmax        : 2952000 
# # ymin        : -1728000 
# # ymax        : 1860000 

template.in.raster <-
  raster::raster(template_input_fp, varname=template_datavar_name)
template.raster <- raster::projectExtent(template.in.raster, crs=out.crs)
#> Warning message:
#> In projectExtent(template.in.raster, out.proj4) :
#>   158 projected point(s) not finite
# is that "projected point(s) not finite" warning important? Probably not, per Hijmans

# without this, extents aren't correct!
template.raster@extent <- template.extents
# should resemble the domain specification @
# https://github.com/TomRoche/cornbeltN2O/wiki/AQMEII-North-American-domain#wiki-EPA

# # start debugging
# cat('\ntemplate.raster==\n')
# template.raster
# # class       : RasterLayer 
# # dimensions  : 299, 459, 137241  (nrow, ncol, ncell)
# # resolution  : 12000, 12000  (x, y)
# # extent      : -2556000, 2952000, -1728000, 1860000  (xmin, xmax, ymin, ymax)
# # coord. ref. : +proj=lcc +lat_1=33 +lat_2=45 +lat_0=40 +lon_0=-97 +a=6370000 +b=6370000 
# #   end debugging

# ----------------------------------------------------------------------
# regrid
# ----------------------------------------------------------------------

cat(sprintf(  # debugging
  '\n%s: regridding global -> AQMEII-NA (may take awhile)\n', this_fn))
out.raster <-
  raster::projectRaster(
    # give a template with extents--fast, but gotta calculate extents
    from=in.raster, to=template.raster, crs=out.crs,
    # give a resolution instead of a template? no, that hangs
#    from=in.raster, res=grid.res, crs=out.proj4,
    method='bilinear', overwrite=TRUE, format='CDF',
    # args from writeRaster
#    NAflag=regrid.datavar.na,
    varname=out_datavar_name, 
    varunit=out_datavar_units,
    longname=out_datavar_long_name,
    xname=out_datavar_dim_x_name,
    yname=out_datavar_dim_y_name,
    # gotta have these, else `ncdump -h` ->
# dimensions:
#         COL = 459 ;
#         ROW = 299 ;
#         value = UNLIMITED ; // (12 currently)
# variables:
# ...
#         int value(value) ;
#                 value:units = "unknown" ;
#                 value:long_name = "value" ;
    zname=out_datavar_dim_z_name,
    zunit=out_datavar_dim_z_units,
    filename=out_fp)

# # start debugging-----------------------------------------------------
# out.raster
# # class       : RasterLayer 
# # dimensions  : 299, 459, 137241  (nrow, ncol, ncell)
# # resolution  : 12000, 12000  (x, y)
# # extent      : -2556000, 2952000, -1728000, 1860000  (xmin, xmax, ymin, ymax)
# # coord. ref. : +proj=lcc +lat_1=33 +lat_2=45 +lat_0=40 +lon_0=-97 +a=6370000 +b=6370000 
# # data source : .../v42_N2O_2008_IPCC_4C_4D.0.1x0.1_reunit_regrid.nc 
# # names       : N2O 
# # zvar        : N2O 

# netCDF.stats.to.stdout(
#   netcdf.fp=out_fp,
#   data.var.name=out_datavar_name,
#   stats.precision=sigdigs
# )
# # From ./v42_N2O_2008_IPCC_4C_4D.0.1x0.1_reunit_regrid.nc datavar=N2O:
# # 	cells=137241
# # 	obs=137241
# # 	min=0
# # 	q1=0
# # 	med=0.000157
# # 	mean=0.00189
# # 	q3=0.00312
# # 	max=0.0861
# # 	sum=259
# #   end debugging-----------------------------------------------------

# ----------------------------------------------------------------------
# visualize output/AQMEII monthly emissions
# ----------------------------------------------------------------------

cat(sprintf('%s: plotting output=%s to %s...\n', # debugging
  this_fn, out_fp, out_pdf_fp))

## get projected North American map
NorAm.shp <- project.NorAm.boundaries.for.CMAQ(
  units='m',
  extents.fp=template_input_fp,
  extents=template.extents,
  LCC.parallels=c(33,45),
  CRS=out.crs)
# # start debugging
# class(NorAm.shp)
# bbox(NorAm.shp)
# # compare bbox to (above)
# # > template.extents
# # class       : Extent 
# # xmin        : -2556000 
# # xmax        : 2952000 
# # ymin        : -1728000 
# # ymax        : 1860000 
# #   end debugging

# Why does '=' fail and '<-' succeed in the arg list?
visualize.layer(
  nc.fp=out_fp,
  datavar.name=out_datavar_name,
  sigdigs=sigdigs,
  layer=out.raster,
#  map.list=list(NorAm.shp),
  map.list <- list(NorAm.shp),
  pdf.fp=out_pdf_fp,
  pdf.height=pdf_height,
  pdf.width=pdf_width
)
