### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

# R code to compute EPIC emissions from EPIC emittivities and BELD crop coverages: see step 5 in ./README.md

# If running manually in R console, remember to run setup actions: `source ./AQMEII_driver.sh`

# ----------------------------------------------------------------------
# constants
# ----------------------------------------------------------------------

# all the following env vars must be set and exported in driver script
# TODO: pass as args!

### miscellany

## kludge for my clumsy namespacing
my_this_fn <- Sys.getenv('COMPUTE_EPIC_FN', unset=NA)
this_fn <- my_this_fn
work_dir <- Sys.getenv('WORK_DIR', unset=NA)

### visualization

pdf_er <- Sys.getenv('PDF_VIEWER', unset=NA)
sigdigs <- as.numeric(Sys.getenv('OUTPUT_SIGNIFICANT_DIGITS', unset=NA))

## projection
global_proj4 <- Sys.getenv('GLOBAL_PROJ4', unset=NA)

## helpers

R_utils_fp <- Sys.getenv('R_UTILS_FP', unset=NA)
stat_funcs_fp <- Sys.getenv('STATS_FUNCS_FP', unset=NA)
vis_funcs_fp <- Sys.getenv('VIS_FUNCS_FP', unset=NA)

### inputs

## BELD crop coverages

beld_fp <- Sys.getenv('BELD_RDS_FP', unset=NA) # path to serialized BELD array

## "distilled" EPIC N2O emittivities: reunit-ed, monotonic, VERDI-compliant

input_epic_fp <- Sys.getenv('AQMEII_EPIC_N2O_EMITT_FP', unset=NA) # path to input file
input_epic_dv_name <- Sys.getenv('AQMEII_EPIC_N2O_EMITT_DATAVAR_NAME', unset=NA)
input_epic_dim_tsteps_name <-  Sys.getenv('AQMEII_EPIC_N2O_EMITT_DIM_TSTEPS_NAME', unset=NA)
input_epic_dim_layers_name <-  Sys.getenv('AQMEII_EPIC_N2O_EMITT_DIM_LAYERS_NAME', unset=NA)
input_epic_dim_x_name <- Sys.getenv('AQMEII_EPIC_N2O_EMITT_DIM_X_NAME', unset=NA)
input_epic_dim_y_name <- Sys.getenv('AQMEII_EPIC_N2O_EMITT_DIM_Y_NAME', unset=NA)

# "fake" datavar=TFLAG, required by IOAPI (and more to the point (IIUC), VERDI)
input_epic_TFLAG_name <- Sys.getenv('AQMEII_EPIC_N2O_EMITT_TFLAG_NAME', unset=NA)
input_epic_dim_datetimes_name <- Sys.getenv('AQMEII_EPIC_N2O_EMITT_TFLAG_DIM_DATETIME_NAME', unset=NA)
input_epic_dim_vars_name <- Sys.getenv('AQMEII_EPIC_N2O_EMITT_TFLAG_DIM_VAR_NAME', unset=NA)

## outputs

## EPIC N2O emissions over AQMEII-NA (actually CONUS): reunit-ed, monotonic, VERDI-compliant

output_epic_fp <- Sys.getenv('AQMEII_EPIC_N2O_EMISS_FP', unset=NA) # path to output file

# file/global attributes
output_epic_attr_nlays = as.numeric(Sys.getenv('AQMEII_EPIC_N2O_EMISS_ATTR_NLAYS', unset=NA))
output_epic_attr_nlays_type = Sys.getenv('AQMEII_EPIC_N2O_EMISS_ATTR_NLAYS_TYPE', unset=NA)
output_epic_attr_varlist = Sys.getenv('AQMEII_EPIC_N2O_EMISS_ATTR_VARLIST', unset=NA)
output_epic_attr_varlist_type = Sys.getenv('AQMEII_EPIC_N2O_EMISS_ATTR_VARLIST_TYPE', unset=NA)
output_epic_attr_filedesc = Sys.getenv('AQMEII_EPIC_N2O_EMISS_ATTR_FILEDESC', unset=NA)
output_epic_attr_filedesc_type = Sys.getenv('AQMEII_EPIC_N2O_EMISS_ATTR_FILEDESC_TYPE', unset=NA)

# dimensions
output_epic_dim_datetimes_name <-  Sys.getenv('AQMEII_EPIC_N2O_EMISS_DIM_DATETIME_NAME', unset=NA)
output_epic_dim_tsteps_name <-  Sys.getenv('AQMEII_EPIC_N2O_EMISS_DIM_TSTEP_NAME', unset=NA)
output_epic_dim_vars_name <-  Sys.getenv('AQMEII_EPIC_N2O_EMISS_DIM_VAR_NAME', unset=NA)
output_epic_dim_x_name <- Sys.getenv('AQMEII_EPIC_N2O_EMISS_DIM_X_NAME', unset=NA)
output_epic_dim_x_units <- Sys.getenv('AQMEII_EPIC_N2O_EMISS_DIM_X_ATTR_UNITS', unset=NA)
output_epic_dim_x_long_name <- Sys.getenv('AQMEII_EPIC_N2O_EMISS_DIM_X_ATTR_LONG_NAME', unset=NA)
output_epic_dim_y_name <- Sys.getenv('AQMEII_EPIC_N2O_EMISS_DIM_Y_NAME', unset=NA)
output_epic_dim_y_units <- Sys.getenv('AQMEII_EPIC_N2O_EMISS_DIM_Y_ATTR_UNITS', unset=NA)
output_epic_dim_y_long_name <- Sys.getenv('AQMEII_EPIC_N2O_EMISS_DIM_Y_ATTR_LONG_NAME', unset=NA)
output_epic_dim_layers_name <-  Sys.getenv('AQMEII_EPIC_N2O_EMISS_DIM_LAYER_NAME', unset=NA)
output_epic_dim_layers_units <- Sys.getenv('AQMEII_EPIC_N2O_EMISS_DIM_LAYER_ATTR_UNITS', unset=NA)
output_epic_dim_layers_long_name <- Sys.getenv('AQMEII_EPIC_N2O_EMISS_DIM_LAYER_ATTR_LONG_NAME', unset=NA)

# datavars
output_epic_dv_name <- Sys.getenv('AQMEII_EPIC_N2O_EMISS_DATAVAR_NAME', unset=NA)
output_epic_dv_units <- Sys.getenv('AQMEII_EPIC_N2O_EMISS_DATAVAR_ATTR_UNITS', unset=NA)
output_epic_dv_long_name <- Sys.getenv('AQMEII_EPIC_N2O_EMISS_DATAVAR_ATTR_LONG_NAME', unset=NA)
output_epic_dv_var_desc <- Sys.getenv('AQMEII_EPIC_N2O_EMISS_DATAVAR_ATTR_VAR_DESC', unset=NA)
# "fake" datavar=TFLAG, required by IOAPI (and more to the point (IIUC), VERDI)
output_epic_TFLAG_name <- Sys.getenv('AQMEII_EPIC_N2O_EMISS_TFLAG_NAME', unset=NA)
# output_epic_TFLAG_var_desc <-  Sys.getenv('AQMEII_EPIC_N2O_EMISS_TFLAG_ATTR_VAR_DESC', unset=NA) # get/copy programmatically

# visualization

output_epic_pdf_fp <- Sys.getenv('AQMEII_EPIC_N2O_EMISS_PDF_FP', unset=NA) # path to plot file
output_epic_pdf_height <- as.numeric(Sys.getenv('SINGLE_FRAME_PLOT_HEIGHT', unset=NA))
output_epic_pdf_width <- as.numeric(Sys.getenv('SINGLE_FRAME_PLOT_WIDTH', unset=NA))

# ----------------------------------------------------------------------
# functions
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# external functions (referenced)
# ----------------------------------------------------------------------

source(R_utils_fp)
source(stat_funcs_fp) # in script, produces errant error=
#> netCDF.stats.to.stdout.r: no arguments supplied, exiting
source(vis_funcs_fp)

# ----------------------------------------------------------------------
# internal functions (defined)
# ----------------------------------------------------------------------

# Report statistics gathered in function sum.emissions.for.layers
report.failures <- function(
  gridcells.vec,    # gridcells with product=0 
  beld.not.epic,    # gridcells with BELD data but not EPIC data
  epic.not.beld,    # gridcells with EPIC data but not BELD data
  gridcells.n,      # total number of (spatial) gridcells (per layer)
  layers.n          # total number of layers
  ) {
  stopifnot(is.vector(gridcells.vec))
  stopifnot(gridcells.n > 0)
  stopifnot(layers.n > 0)
  if (length(gridcells.vec) > 0) {
    base::cat(base::sprintf('%s: of total spatial gridcells==%i and total data points==%i\n',
       this_fn, gridcells.n, gridcells.n * layers.n))
    base::cat(base::sprintf('\tdata points==%i have BELD data but not EPIC data\n',
       beld.not.epic))
    base::cat(base::sprintf('\tdata points==%i have EPIC data but not BELD data\n',
       epic.not.beld))
    base::cat(base::sprintf('\tcausing spatial gridcells==%i to have no emissions\n',
       length(gridcells.vec)))
    base::flush(stdout())
  }
} # end report.failures

# Compute sum of products of "BELD vector" and "EPIC vector," which may be NA.
# Returns list of

# * the payload:
#   list$sum: emissions: sum of products over layers
# * reporting data failures:
#   list$record.gridcells: where we have crop and BELD data, but sum there == 0
#   list$record.have.epic.not.beld: failures where BELD layer == 0 
#   list$record.have.beld.not.crop: failures where EPIC layer == 0
sum.emissions.for.layers <- function(
  epic.vec,           # crop emittivity data
  beld.vec,           # crop coverage data
  col,                # for debugging and statistics only
  row,
  record.gridcells,           # for statistics only
  record.have.epic.not.beld,
  record.have.beld.not.epic
) {
# start debugging
# epic.vec <- input.epic.vec # if self-stepping through source
#  base::cat(base::sprintf('class(epic.vec)==%s\n', class(epic.vec)))
#  base::cat(base::sprintf('length(epic.vec)==%i\n', length(epic.vec)))
#  print('dim(epic.vec)==') ; print(dim(epic.vec))
#  base::cat(base::sprintf('class(beld.vec)==%s\n', class(beld.vec)))
#  base::cat(base::sprintf('length(beld.vec)==%i\n', length(beld.vec)))
#  print('dim(beld.vec)==') ; print(dim(beld.vec))
#   end debugging

  beld.vec.len <- length(beld.vec)
  epic.vec.len <- length(epic.vec)
  stopifnot(epic.vec.len == beld.vec.len) # assert
  vec.len <- beld.vec.len # so just use one

  if (is.vec.na.or.zero(epic.vec)) {
# # start debugging-----------------------------------------------------
#    base::cat(base::sprintf(this_fn, ': no EPIC data for gridcell==[%3i,%3i]\n', col, row))
# #   end debugging-----------------------------------------------------
    # so can't compute a sum of products
    sum <- NA
  }

  if (is.vec.na.or.zero(beld.vec)) {
# # start debugging-----------------------------------------------------
#    base::cat(base::sprintf(this_fn, ': no BELD data for gridcell==[%3i,%3i]\n', col, row))
# #   end debugging-----------------------------------------------------
    # so can't compute a sum of products
    sum <- NA
  }

  if (!is.vec.na.or.zero(epic.vec) && !is.vec.na.or.zero(beld.vec)) {
# nope: matrix multiply does not DWIM with NA
#    result <- epic.vec %*% beld.vec # matrix multiply
    # thanks, KMF!
    sum <- sum(epic.vec * beld.vec, na.rm=TRUE)

# # start debugging-----------------------------------------------------

# This does not happen ...
#    if (is.na(result)) {
#      base::cat(base::sprintf('sum.emissions.for.layers: ERROR: for gridcell==[%3i,%3i], both EPIC and BELD data are not empty, but their product==NA\n', col, row))
#      base::cat('EPIC data==\n')
#      print(epic.vec)
#      base::cat('BELD data==\n')
#      print(beld.vec)
#      flush(stdout()) # otherwise, I get only the 'EPIC data==' prompt on terrae
#      return # halts? no
#    }

# ... but this *does* happen, a lot:
# TODO: trap these errors, halt, and examine
    if (sum == 0) {
#      base::cat(base::sprintf('sum.emissions.for.layers: ERROR: for gridcell==[%3i,%3i], both EPIC and BELD data are not empty, but their product==0\n', col, row))
#      base::cat('EPIC data==\n')
#      print(epic.vec)
#      base::cat('BELD data==\n')
#      print(beld.vec)
#      flush(stdout())
       record.gridcells[length(record.gridcells) +1] <- base::sprintf('[%3i,%3i]', col, row)
       # TODO: vectorize
       for (i.data in 1:vec.len) {
         epic <- epic.vec[i.data]
         beld <- beld.vec[i.data]
         if        (!is.val.na.or.zero(beld) && is.val.na.or.zero(epic)) {
           record.have.beld.not.epic <- record.have.beld.not.epic +1
         } else if (is.val.na.or.zero(beld) && !is.val.na.or.zero(epic)) {
           record.have.epic.not.beld <- record.have.epic.not.beld +1
         }
       }
    } # end if (sum == 0)
#   end debugging-----------------------------------------------------

  } # end if (!is.vec.na.or.zero(epic.vec) && !is.vec.na.or.zero(beld.vec))

  # note: explicit `return` halts execution!
  list(emissions=sum,
       gridcells.vec=record.gridcells,
       have.epic.not.beld=record.have.epic.not.beld,
       have.beld.not.epic=record.have.beld.not.epic
  )
} # end function sum.emissions.for.layers

# ----------------------------------------------------------------------
# code
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# setup
# ----------------------------------------------------------------------

# accelerate R graphics over SSH, per Adam Wilson http://planetflux.adamwilson.us/2012/03/r-graphics-via-ssh.html
X11.options(type="Xlib")

### visualization

## AQMEII-NA coordinate reference system:
# use package=M3 to get CRS from input
library(M3)
# aqme.proj4 <- M3::get.proj.info.M3(template_input_fp)
aqme.proj4 <- M3::get.proj.info.M3(input_epic_fp)
# base::cat(base::sprintf('aqme.proj4=%s\n', aqme.proj4)) # debugging
# aqme.proj4=+proj=lcc +lat_1=33 +lat_2=45 +lat_0=40 +lon_0=-97 +a=6370000 +b=6370000
aqme.crs <- sp::CRS(aqme.proj4)

## use package=M3 to get extents from template file (thanks CGN!)
input.epic.extents.info <- M3::get.grid.info.M3(input_epic_fp)
input.epic.extents.xmin <- input.epic.extents.info$x.orig
input.epic.extents.xmax <- max(
  M3::get.coord.for.dimension(
    file=input_epic_fp, dimension="col", position="upper", units="m")$coords)
input.epic.extents.ymin <- input.epic.extents.info$y.orig
input.epic.extents.ymax <- max(
  M3::get.coord.for.dimension(
    file=input_epic_fp, dimension="row", position="upper", units="m")$coords)
grid.res <- c(input.epic.extents.info$x.cell.width, input.epic.extents.info$y.cell.width) # units=m

library(raster)
input.epic.extents <-
  raster::extent(input.epic.extents.xmin, input.epic.extents.xmax, input.epic.extents.ymin, input.epic.extents.ymax)
# input.epic.extents # debugging
# class       : Extent 
# xmin        : -2556000 
# xmax        : 2952000 
# ymin        : -1728000 
# ymax        : 1860000 

### for (output) data-processing analysis
# Following are passed to sum.emissions.for.layers(...):

# record.gridcells is a vector of strings like '[col,row]',
#   recording gridcells where we have both EPIC and BELD data, but their product == 0
record.gridcells <- character(0)
# record.have.epic.not.beld records number of gridcells where BELD data is missing
record.have.epic.not.beld <- 0
# record.have.beld.not.epic records number of gridcells where EPIC data is missing
record.have.beld.not.epic <- 0

# ----------------------------------------------------------------------
# payload
# ----------------------------------------------------------------------

### Note on dimensionality in the following: the term 'dimension' has very different meanings in
### * base R, where a dimensions is usually just an int or vector of ints
### * R package=ncdf4, where a dimensions is an object of type=ncdim4

# ----------------------------------------------------------------------
# open inputs
# ----------------------------------------------------------------------

library(ncdf4)

input.epic.fh <- ncdf4::nc_open(input_epic_fp, write=FALSE, readunlim=FALSE)

### EPIC per-crop N2O emittivities, previously "distilled"
# input.epic.dv <- ncdf4::ncvar_get(input.epic.fh, varid=input_epic_dv_name)
# `ncdf4::ncvar_get` gets data, not metadata
input.epic.dv.arr <- ncdf4::ncvar_get(input.epic.fh, varid=input_epic_dv_name)
input.epic.dv <- input.epic.fh$var[[input_epic_dv_name]] # note double brackets!

## get input datavar type, missing value, long_name, units useful for output
# input.epic.dv.attr.list <- ncdf4::ncatt_get(
#   nc=input.epic.fh, varid=input_epic_dv_name, attname=NA, verbose=FALSE)
# input.epic.dv.mv <- input.epic.dv.attr.list$`_FillValue` # note required backquotes
# instead, more consistently (e.g., $prec cannot be gotten with `ncdf4::ncatt_get`)
input.epic.dv.type <- input.epic.dv$prec
input.epic.dv.mv <- input.epic.dv$missval
# get these from driver
# input.epic.dv.units <- input.epic.dv$units
# input.epic.dv.long_name <- input.epic.dv$long_name
# input.epic.dv.var_desc <- input.epic.dv$var_desc

### "fake" datavar=TFLAG, for IOAPI and (IIUC) VERDI
input.epic.TFLAG.arr <- ncdf4::ncvar_get(input.epic.fh, varid=input_epic_TFLAG_name)
input.epic.TFLAG <- input.epic.fh$var[[input_epic_TFLAG_name]]
input.epic.TFLAG.type <- input.epic.TFLAG$prec
input.epic.TFLAG.mv <- input.epic.TFLAG$missval

# ----------------------------------------------------------------------
# get input and output dimensionalities
# ----------------------------------------------------------------------

### BELD dimensions set by data writer: see that code.

### Both EPIC input and output have same spatial grid, input also has layers=crops as leftmost dim (reversed by R)
# input.epic.dv.dims <- dim(input.epic.dv)
# > NULL

### EPIC input (emissivities)

## EPIC input dims

input.epic.dim.datetimes <- input.epic.fh$dim[[input_epic_dim_datetimes_name]]
input.epic.dim.layers <- input.epic.fh$dim[[input_epic_dim_layers_name]]
input.epic.dim.tsteps <- input.epic.fh$dim[[input_epic_dim_tsteps_name]]
input.epic.dim.vars <- input.epic.fh$dim[[input_epic_dim_vars_name]]
input.epic.dim.x <- input.epic.fh$dim[[input_epic_dim_x_name]]
input.epic.dim.y <- input.epic.fh$dim[[input_epic_dim_y_name]]

input.epic.dim.datetimes.n <- input.epic.dim.datetimes$len
input.epic.dim.layers.n <- input.epic.dim.layers$len
input.epic.dim.tsteps.n <- input.epic.dim.tsteps$len
input.epic.dim.vars.n <- input.epic.dim.vars$len
input.epic.dim.x.n <- input.epic.dim.x$len
input.epic.dim.y.n <- input.epic.dim.y$len

## EPIC input "real" datavar

input.epic.dv.dims <- dim(input.epic.dv.arr)
input.epic.dv.dims.n <- length(input.epic.dv.dims)
# spatial cells only: ignore layers, tsteps
input.epic.dv.cells.n <- input.epic.dim.y.n * input.epic.dim.x.n

## EPIC input TFLAG

input.epic.TFLAG.dims <- dim(input.epic.TFLAG.arr)
input.epic.TFLAG.dims.n <- length(input.epic.TFLAG.dims)

### EPIC output (emissions)

output.epic.dim.datetimes.n <- input.epic.dim.datetimes.n
output.epic.dim.layers.n <- 1 # in output, `layer`=verticality, !=crop
output.epic.dim.tsteps.n <- input.epic.dim.tsteps.n
output.epic.dim.vars.n <- input.epic.dim.vars.n
output.epic.dim.x.n <- input.epic.dim.x.n
output.epic.dim.y.n <- input.epic.dim.y.n

output.epic.dv.cells.n <- input.epic.dv.cells.n
output.epic.dv.dims <-
  c(output.epic.dim.x.n, output.epic.dim.y.n, output.epic.dim.layers.n, output.epic.dim.tsteps.n)
output.epic.dv.dims.n <- length(output.epic.dv.dims)

# ----------------------------------------------------------------------
# read inputs
# ----------------------------------------------------------------------

### BELD input, previously serialized
input.beld.arr <- base::readRDS(beld_fp)

# # start debugging-----------------------------------------------------
# base::cat(base::sprintf('\n%s: dim(input.beld.arr)==', this_fn))
# print(dim(input.beld.arr))
# stats.to.stdout(
#   data=input.beld.arr,
#   sig.digs=sigdigs,
#   title=base::sprintf('\n%s: BELD input coverages=', this_fn)
# )
# #   end debugging-----------------------------------------------------

### EPIC input

## TODO: "Pierce-style read" (see help(ncvar_get)#Examples): protects against data overload (somewhat)
## but more complicated to implement. Copy code from (github)ioapi-hack-R/computeCropSum.r
## Normally one reads one timestep at a time, but EPIC input has no timesteps--
## other than the implicit/degenerate model year ... which, for VERDI-compliance, it appears we must use :-(
## So gotta deal with implicit/degenerate TSTEP==1 ...

input.epic.dv.read.start <- rep(1, input.epic.dv.dims.n)   # start=(1,1,1,...)
input.epic.dv.read.count <- input.epic.dv.dims             # count=(n,n,n,...)

# so gotta handle TSTEP=1: remembering timelike dim is always the LAST dimension!
if      (input.epic.dv.dims.n < 3) {
  # TODO: throw
#   cat(sprintf('\n%s: ERROR: dimensionality=%i < 3 of EPIC input datavar=%s\n',
#     this_fn, input.epic.dv.dims.n, input_epic_dv_name))
  stop(sprintf('%s: ERROR: dimensionality=%i < 3 of EPIC input datavar=%s',
    this_fn, input.epic.dv.dims.n, input_epic_dv_name))

} else if (input.epic.dv.dims.n == 3) {
  if (input.epic.dim.tsteps.n != 1) {
    stop(sprintf('%s: ERROR: dimensionality=3 of EPIC input datavar=%s, but dim(tsteps)=%i',
      this_fn, input_epic_dv_name, input.epic.dim.tsteps.n))
  }
  # append tsteps
  input.epic.dv.dims <- c(input.epic.dv.dims, 1)
  input.epic.dv.read.count <- input.epic.dv.dims
  input.epic.dv.read.start <- c(input.epic.dv.read.start, 1)
  input.epic.dv.dims.n <- 4

# start debugging-------------------------------------------
  cat(sprintf('\n%s: dimensionality=%i (was 3) of EPIC input datavar=%s, dimensions are',
    this_fn, input.epic.dv.dims.n, input_epic_dv_name))
  print(input.epic.dv.dims)
#   end debugging-------------------------------------------

# start debugging-------------------------------------------
} else if (input.epic.dv.dims.n == 4) {
  cat(sprintf('\n%s: dimensionality=4 of EPIC input datavar=%s, dimensions are',
    this_fn, input_epic_dv_name))
  print(input.epic.dv.dims)
#   end debugging-------------------------------------------

} else {
  # TODO: throw
  stop(sprintf('%s: ERROR: dimensionality=%i > 4 of EPIC input datavar=%s',
    this_fn, input.epic.dv.dims.n, input_epic_dv_name))
} # end testing input.epic.dv.dims.n

### Get input data
input.epic.dv.arr <-
  ncdf4::ncvar_get(input.epic.fh, varid=input_epic_dv_name, start=input.epic.dv.read.start, count=input.epic.dv.read.count)
## Once again, I'm caught between IOAPI (which demands tstep=1) and netCDF (which strips tstep=1)
if ((length(dim(input.epic.dv.arr)) == 3) && (input.epic.dv.dims.n == 4)) {
  dim(input.epic.dv.arr) <- c(dim(input.epic.dv.arr), 1)
}

# # start debugging-----------------------------------------------------
# base::cat(base::sprintf('\n%s: dim(input.epic.dv.arr)==', this_fn))
# print(dim(input.epic.dv.arr))
# stats.to.stdout(
#   data=input.epic.dv.arr,
#   sig.digs=sigdigs,
#   title=base::sprintf('\n%s: EPIC input emittivities=', this_fn)
# )

# # compute_EPIC_emissions.r: EPIC input emittivities=
# #         cells=5764122
# #         obs=369846
# #         min=0
# #         q1=0
# #         med=0.000314
# #         mean=0.00375
# #         q3=0.0021
# #         max=7.04
# #         sum=1.39e+03
# #   end debugging-----------------------------------------------------

### input TFLAG

## ... gotta deal with implicit/degenerate TSTEP==1 again!
## Remembering timelike dim is always the LAST dimension!
if      (input.epic.TFLAG.dims.n < 1) {
  # TODO: throw
  stop(sprintf('%s: ERROR: dimensionality=%i < 1 of EPIC input datavar=%s',
    this_fn, input.epic.TFLAG.dims.n, input_epic_TFLAG_name))

} else if (input.epic.TFLAG.dims.n == 1) {
  if ((input.epic.dim.tsteps.n != 1) && (input.epic.dim.var.n != 1)) {
    stop(sprintf('%s: ERROR: dimensionality=1 of EPIC input datavar=%s, but dim(TSTEP)=%i and dim(VAR)=%i',
      this_fn, input_epic_TFLAG_name, input.epic.dim.tsteps.n, input.epic.dim.vars.n))
  }
  # append TSTEP, VAR -> (TSTEP, VAR, DATE-TIME) ... but reversed :-(
  input.epic.TFLAG.dims <- c(input.epic.TFLAG.dims, 1, 1)
  input.epic.TFLAG.dims.n <- 3
# start debugging-------------------------------------------
  cat(sprintf('\n%s: dimensionality=%i (was 1) of EPIC input datavar=%s, dimensions are',
    this_fn, input.epic.TFLAG.dims.n, input_epic_TFLAG_name))
  print(input.epic.TFLAG.dims)
#   end debugging-------------------------------------------

} else if (input.epic.TFLAG.dims.n == 2) {
  # which |dim|==1 ???
  if        (input.epic.dim.tsteps.n == 1) {
    input.epic.TFLAG.dims <-
      c(input.epic.dim.datetime.n, input.epic.dim.vars.n, 1)
  } else if (input.epic.dim.var.n == 1) {
    input.epic.TFLAG.dims <-
      c(input.epic.dim.datetime.n, 1, input.epic.dim.tsteps.n)
  }
  input.epic.TFLAG.dims.n <- 3
# start debugging-------------------------------------------
  cat(sprintf('\n%s: dimensionality=%i (was 2) of EPIC input datavar=%s, dimensions are',
    this_fn, input.epic.TFLAG.dims.n, input_epic_TFLAG_name))
  print(input.epic.TFLAG.dims)
#   end debugging-------------------------------------------

} else if (input.epic.TFLAG.dims.n == 3) {

  # life is good ...
  cat(sprintf('\n%s: dimensionality=3 of EPIC input datavar=%s, dimensions are',
    this_fn, input_epic_TFLAG_name))
  print(input.epic.TFLAG.dims)

} else {
  # TODO: throw
  stop(sprintf('%s: ERROR: dimensionality=%i > 3 of EPIC input datavar=%s',
    this_fn, input.epic.TFLAG.dims.n, input_epic_TFLAG_name))
} # end testing input.epic.TFLAG.dims.n
input.epic.TFLAG.read.start <- rep(1, input.epic.TFLAG.dims.n)
input.epic.TFLAG.read.count <- input.epic.TFLAG.dims

# ----------------------------------------------------------------------
# setup output data container(s)
# ----------------------------------------------------------------------

### TFLAG

output.epic.TFLAG.dims <- input.epic.TFLAG.dims # (TSTEP, VAR, DATE-TIME), except ...
output.epic.TFLAG.dims.n <- length(output.epic.TFLAG.dims)
output.epic.TFLAG.arr <- input.epic.TFLAG.arr
output.epic.TFLAG.write.start <- input.epic.TFLAG.read.start
output.epic.TFLAG.write.count <- input.epic.TFLAG.read.count

# # start debugging-------------------------------------------
# cat(sprintf('%s: output.epic.TFLAG.dims==', this_fn))
# print(output.epic.TFLAG.dims)
# cat(sprintf('%s: output.epic.TFLAG.write.start==', this_fn))
# print(output.epic.TFLAG.write.start)
# cat(sprintf('%s: output.epic.TFLAG.write.count==', this_fn))
# print(output.epic.TFLAG.write.count)
# #   end debugging-------------------------------------------

### the real datavar

output.epic.dv.arr <- array(NA, output.epic.dv.dims)
output.epic.dv.write.start <- rep(1, output.epic.dv.dims.n)   # start=(1,1,1,...)
output.epic.dv.write.count <- output.epic.dv.dims             # count=(n,n,n,...)

# # start debugging-----------------------------------------------------
# base::cat(base::sprintf('\n%s: output.epic.dv.write.start==', this_fn))
# print(output.epic.dv.write.start)
# base::cat(base::sprintf('%s: output.epic.dv.write.count==', this_fn))
# print(output.epic.dv.write.count)
# #   end debugging-----------------------------------------------------

# ----------------------------------------------------------------------
# compute output from inputs
# ----------------------------------------------------------------------

# start debugging-----------------------------------------------------
base::cat(base::sprintf('\n%s: about to compute scalar product of BELD crop coverages and EPIC crop emittivities\n', this_fn))
#   end debugging-----------------------------------------------------

### Process each gridcell. TODO: as dual `apply`?

for (i.tstep in 1:input.epic.dim.tsteps.n) {
# i.tstep <- 1

  input.epic.dv.arr.tstep <- input.epic.dv.arr[,,,i.tstep]
#   for (i.lay in 1:input.epic.dim.layers.n) {
    for (i.col in 1:input.epic.dim.x.n) {
      for (i.row in 1:input.epic.dim.y.n) {
        # both input.beld.vec and input.epic.vec extend along LAY
        input.beld.vec <- input.beld.arr[i.col,i.row,]       # vector of crop coverages
        input.epic.vec <- input.epic.dv.arr.tstep[i.col,i.row,] # vector of crop emittivities

        emissions.list <- sum.emissions.for.layers(
          epic.vec=input.epic.vec,
          beld.vec=input.beld.vec,
          col=i.col,
          row=i.row,
          record.gridcells=record.gridcells,
          record.have.epic.not.beld=record.have.epic.not.beld,
          record.have.beld.not.epic=record.have.beld.not.epic
        )
#        output.epic.dv.arr[i.col, i.row, i.lay, i.tstep] <-
# ASSERT: output has only one layer=LAY!
        output.epic.dv.arr[i.col, i.row, 1, i.tstep] <-
          emissions.list$emissions # scalar product of input vectors
        # sum.emissions.for.layers "rewrites" statistical/reporting inputs
        record.gridcells <- emissions.list$gridcells.vec
        record.have.epic.not.beld <- emissions.list$have.epic.not.beld
        record.have.beld.not.epic <- emissions.list$have.beld.not.epic

      } # end for rows
    } # end for cols
#   } # end for layers
} # end for tsteps

# start debugging-----------------------------------------------------
base::cat(base::sprintf('\n%s: dim(output.epic.dv.arr)==', this_fn))
print(dim(output.epic.dv.arr))
stats.to.stdout(
  data=output.epic.dv.arr,
  sig.digs=sigdigs,
  title=base::sprintf('\n%s: EPIC output emissions=', this_fn)
)

# compute_EPIC_emissions.r: EPIC output emissions=
#         cells=137241
#         obs=40764
#         min=0
#         q1=1.02e-05
#         med=0.000171
#         mean=0.00138
#         q3=0.00111
#         max=0.283
#         sum=56.2
#   end debugging-----------------------------------------------------

# ----------------------------------------------------------------------
# remove outlier from output
# ----------------------------------------------------------------------

# Very big gap between q3=0.00111 and max=0.283: gotta suspect an outlier/error.
# What are our results minus that max?
output.epic.dv.arr.max <- max(output.epic.dv.arr, na.rm=TRUE)
output.epic.dv.arr.max.coords <- # returns 2D: useful to humans, not for R
  as.vector(which(output.epic.dv.arr >= output.epic.dv.arr.max, arr.ind = TRUE))
output.epic.dv.arr.max.index <- # returns 1D: R can use to lookup/change the value
  which(output.epic.dv.arr >= output.epic.dv.arr.max)

# # start debugging-----------------------------------------------------
# base::cat(base::sprintf('\n%s: max(output.epic.dv.arr)==%e\n',
#   this_fn, output.epic.dv.arr.max))
# base::cat(base::sprintf('\n%s: coords max(output.epic.dv.arr)==', this_fn))
# print(output.epic.dv.arr.max.coords)

# # compute_EPIC_emissions.r: max(output.epic.dv.arr)==2.829876e-01
# # compute_EPIC_emissions.r: coords max(output.epic.dv.arr)==[1]  93 235
# #   end debugging-----------------------------------------------------

# results minus that max
output.epic.dv.arr.minus.max <- output.epic.dv.arr
output.epic.dv.arr.minus.max[output.epic.dv.arr.max.index] <- NA

# start debugging-----------------------------------------------------
stats.to.stdout(
  data=output.epic.dv.arr.minus.max,
  sig.digs=sigdigs,
  title=base::sprintf('\n%s: EPIC output emissions minus (spurious?) max=%.3g:',
    this_fn, output.epic.dv.arr.max)
)

# compute_EPIC_emissions.r: EPIC output emissions minus (spurious?) max=0.283:
#         cells=137241
#         obs=40763
#         min=0
#         q1=1.02e-05
#         med=0.000171
#         mean=0.00137
#         q3=0.00111
#         max=0.0757
#         sum=56
#   end debugging-----------------------------------------------------

# Much more plausible! remove the outlier

# start debugging-----------------------------------------------------
base::cat(base::sprintf(
  '\n%s: WARNING: removing presumed-spurious max=%e from output\n',
  this_fn, output.epic.dv.arr.max))
#   end debugging-----------------------------------------------------

output.epic.dv.arr <- output.epic.dv.arr.minus.max

# ----------------------------------------------------------------------
# copy/mod IOAPI/fake datavar=TFLAG
# ----------------------------------------------------------------------

input.epic.TFLAG <- input.epic.fh$var[[input_epic_TFLAG_name]] # note double brackets!
# input.epic.TFLAG.type <- input.epic.TFLAG$prec
# arrggghhhh
input.epic.TFLAG.type <- get.ncdf4.prec(input.epic.TFLAG)
input.epic.TFLAG.mv <- input.epic.TFLAG$missval
# get these from driver? no
input.epic.TFLAG.units <- input.epic.TFLAG$units
input.epic.TFLAG.long_name <- input.epic.TFLAG$longname
# input.epic.TFLAG.var_desc <- input.epic.TFLAG$var_desc
input.epic.TFLAG.var_desc <- ncdf4::ncatt_get(input.epic.fh, input_epic_TFLAG_name)$var_desc
## and its dimensions? no, do that @ writetime, since dimensions are really about files

output.epic.TFLAG.var_desc <- input.epic.TFLAG.var_desc

# ----------------------------------------------------------------------
# define and write output
# ----------------------------------------------------------------------

### Note on creating output, adapted from ncdf4.pdf (numbering added):
# > If you want to WRITE data to a new netCDF file, the procedure is to
# 1. define output dimensions: call `ncdim_def(...)`
# 2. define output datavar(s): call `ncvar_def(...)` to define each variable
# 3. create output file: call `nc_create(...)`
# 4. write data to output datavar(s): call `ncvar_put(...)` to write to each datavar
# 5. close output file: call `nc_close(...)`
# 6. (extra credit)remove output filehandle from the workspace/environment: call `rm(..)`

### 1. define output dimensions
cat(sprintf('\n%s: 1. define output dimensions\n', this_fn)) # debugging

## instead, copy input dimensions? I can't see how to :-(

## 1.1. get values for output dimensions
cat(sprintf('\n%s: 1.1. get values for output dimensions\n', this_fn)) # debugging

# For non-grid dimensions, can use ncdf4

# input.epic.dim.tsteps <- input.epic.fh$dim[[input_epic_dim_tsteps_name]]
input.epic.dim.tsteps.vals <- input.epic.dim.tsteps$vals
# input.epic.dim.tsteps.units <- input.epic.dim.tsteps$units
# ncdf4 (formatting added):
# > [if] create_dimvar was specified to be FALSE, [then]
# > the unit string MUST be empty ('') and
# > the dimension values MUST be simple integers from 1 to the length of the dimension (e.g., 1:len)
input.epic.dim.tsteps.units <- ''

# input.epic.dim.datetimes <- input.epic.fh$dim[[input_epic_dim_datetimes_name]]
input.epic.dim.datetimes.vals <- input.epic.dim.datetimes$vals
# input.epic.dim.datetimes.units <- input.epic.dim.datetimes$units
input.epic.dim.datetimes.units <- ''

# input.epic.dim.layers <- input.epic.fh$dim[[input_epic_dim_layers_name]]
input.epic.dim.layers.vals <- input.epic.dim.layers$vals
# input.epic.dim.layers.units <- input.epic.dim.layers$units
input.epic.dim.layers.units <- ''

# input.epic.dim.vars <- input.epic.fh$dim[[input_epic_dim_vars_name]]
input.epic.dim.vars.vals <- input.epic.dim.vars$vals
# input.epic.dim.vars.units <- input.epic.dim.vars$units
input.epic.dim.vars.units <- ''

# For grid dimensions, I could use ncdf4, but, to overlay plots with "the usual" map,
# we need dimensions uncentered and with units=m
library(M3)
input.epic.dim.x.vals <- M3::get.coord.for.dimension(
  file=input_epic_fp, dimension="col", position="upper", units="m")$coords
input.epic.dim.y.vals <- M3::get.coord.for.dimension(
  file=input_epic_fp, dimension="row", position="upper", units="m")$coords

## 1.2. Create output dimension definitions
cat(sprintf('\n%s: 1.2. Create output dimension definitions\n', this_fn)) # debugging

output.epic.dim.x <- ncdim_def(
  name=output_epic_dim_x_name,
  units=output_epic_dim_x_units,
  vals=input.epic.dim.x.vals,
  unlim=FALSE,
  create_dimvar=TRUE,
# IOAPI does not create coordvars, but we will, for the grid (unless VERDI chokes)
#   create_dimvar=FALSE,
  calendar=NA,
  longname=output_epic_dim_x_long_name)

output.epic.dim.y <- ncdim_def(
  name=output_epic_dim_y_name,
  units=output_epic_dim_y_units,
  vals=input.epic.dim.y.vals,
  unlim=FALSE,
  create_dimvar=TRUE,
# IOAPI does not create coordvars, but we will, for the grid (unless VERDI chokes)
#   create_dimvar=FALSE,
  calendar=NA,
  longname=output_epic_dim_y_long_name)

# output=input
output.epic.dim.datetimes <- ncdim_def(
  name=input_epic_dim_datetimes_name,
  units=input.epic.dim.datetimes.units,
  vals=input.epic.dim.datetimes.vals,
  unlim=FALSE,
  create_dimvar=FALSE,
  calendar=NA) # long_name omitted

# output(LAY) != input(LAY)
output.epic.dim.layers <- ncdim_def(
  name=input_epic_dim_layers_name,
  units=input.epic.dim.layers.units,
#  vals=input.epic.dim.layers.vals, # 1:42, not what output wants
#  vals=c(1),
#  vals=1,
# dunno how this differs from the above, but below makes ncdf4 happy, and above did not
  vals=input.epic.dim.layers.vals[1],
  unlim=FALSE,
  create_dimvar=FALSE,
  calendar=NA) # long_name omitted

# output=input
output.epic.dim.tsteps <- ncdim_def(
  name=input_epic_dim_tsteps_name,
  units=input.epic.dim.tsteps.units,
  vals=input.epic.dim.tsteps.vals,
  unlim=FALSE,
  create_dimvar=FALSE,
  calendar=NA) # long_name omitted

# output=input
output.epic.dim.vars <- ncdim_def(
  name=input_epic_dim_vars_name,
  units=input.epic.dim.vars.units,
  vals=input.epic.dim.vars.vals,
  unlim=FALSE,
  create_dimvar=FALSE,
  calendar=NA) # long_name omitted

### 2. define output datavar(s)
cat(sprintf('\n%s: 2. define output datavar(s)\n', this_fn)) # debugging

## Try to make TFLAG the first "datavar"? Not sure it matters, but in the IOAPI files I've `ncdump`ed, it always is.

output.epic.TFLAG <- ncdf4::ncvar_def(
  name=output_epic_TFLAG_name,
  units=input.epic.TFLAG.units,
# (TSTEP, VAR, DATE-TIME) is how it looks in `ncdump`, but ncdf4 wants ...
  dim=list(output.epic.dim.datetimes, output.epic.dim.vars, output.epic.dim.tsteps),
  prec=input.epic.TFLAG.type,
# ncdf4::ncatt_put vomits on being asked to create datavar of type=integer with a missing_value:
# > [1] "ncatt_put_inner: prec to create: integer"
# > Error in R_nc4_put_att_logical: asked to put a NA value, but the variable's type is not a double or float, which are the only two types that have a defined NaN value
# > [1] "Error in ncatt_put, while writing attribute _FillValue with value NA"
# > Error in ncatt_put_inner(ncid2use, newvar$id, "_FillValue", v$missval,  : 
# >   Error return from C call R_nc4_put_att_logical for attribute _FillValue
# > Calls: <Anonymous> -> ncvar_add -> ncatt_put_inner
# so don't try!
#   missval=input.epic.TFLAG.mv,
  longname=input.epic.TFLAG.long_name,
  shuffle=FALSE,
  compression=NA,
  chunksizes=NA)

output.epic.dv <- ncdf4::ncvar_def(
  name=output_epic_dv_name,
  units=output_epic_dv_units,
# (TSTEP, LAY, ROW, COL) is how it looks in `ncdump`, but ncdf4 wants ...
  dim=list(output.epic.dim.x, output.epic.dim.y, output.epic.dim.layers, output.epic.dim.tsteps),
  missval=input.epic.dv.mv,
  longname=output_epic_dv_long_name,
  prec=input.epic.dv.type,
  shuffle=FALSE,
  compression=NA,
  chunksizes=NA)

### 3. create output file
cat(sprintf('\n%s: 3. create output file\n', this_fn)) # debugging

# # start debugging-----------------------------------------------------
# base::cat(base::sprintf('\n%s: about to attempt\n', this_fn))
# base::cat(base::sprintf('output.epic.fh <- ncdf4::nc_create(\n'))
# base::cat(base::sprintf('  filename=%s\n', output_epic_fp))
# base::cat(base::sprintf('  vars=output.epic.dv == %s\n', output_epic_dv_name))
# #   end debugging-----------------------------------------------------

output.epic.fh <- ncdf4::nc_create(
  filename=output_epic_fp,
#   vars=output.epic.dv,               # can also be vector or list? no, just a list
# Try to make TFLAG the first "datavar"? Not sure it matters, but in the IOAPI files I've `ncdump`ed, it always is.
  vars=list(output.epic.TFLAG, output.epic.dv),
  force_v4=FALSE,
# # to debug, toggle verbose
#   verbose=TRUE)
  verbose=FALSE)

## 3.1. create output file/global attributes
cat(sprintf('\n%s: 3.1. create output file/global attributes\n', this_fn)) # debugging

## copy attributes from EPIC input, then overwrite {NLAYS, VAR-LIST, FILEDESC}

## How to copy all global attributes from a file? This is trivial with NCL, but I'm not seeing API in ncdf4.
## However, print.ncdf4 (which is an S4 method, since ncdf4 is an S4 class) can find this, so we need to get the function definition for print.ncdf4.
## Getting the function definition would be trivial if print.ncdf4 was S3, but it's not.
## After much thrashing, and some guidance from http://www.r-bloggers.com/s4-classes-in-r-printing-function-definition-and-getting-help/
## I discover howto get the function definition: `package:::methodname`, e.g., `ncdf4:::print.ncdf4`
## which shows the secret ncdf4 syntax for getting/walking all the file/global attributes:
## > atts <- ncatt_get(nc, 0) # where `nc`==filehandle
## > natts <- length(atts)
## > if (natts > 0) {
## >   print(paste("    ", natts, " global attributes:", sep = ""))
## >   nms <- names(atts)
## >   for (ia in 1:natts) 
## >     print(paste("        ", nms[ia], ": ", atts[[ia]], sep = ""))
## > }

input.epic.attrs <- ncdf4::ncatt_get(nc=input.epic.fh, varid=0,
  attname=NA) # this is what gets us a list of all file/global attributes
input.epic.attrs.n <- length(input.epic.attrs)
if (input.epic.attrs.n > 0) {
  input.epic.attrs.names <- names(input.epic.attrs)
  for (i.attrs in 1:input.epic.attrs.n) {
    attr.name <- input.epic.attrs.names[i.attrs]
#     # start debugging-------------------------------------------------
#     cat(sprintf('\n%s: 3.1. copying file/global attribute=%s\n', this_fn, attr.name)) # debugging
#     #   end debugging-------------------------------------------------
    if        (attr.name == 'FILEDESC') {
      attr.val <- output_epic_attr_filedesc
      attr.type <- output_epic_attr_filedesc_type
    } else if (attr.name == 'NLAYS') {
      attr.val <- output_epic_attr_nlays
      attr.type <- output_epic_attr_nlays_type
    } else if (attr.name == 'VAR-LIST') {
      attr.val <- output_epic_attr_varlist
      attr.type <- output_epic_attr_varlist_type
    } else {
      attr.val <- input.epic.attrs[[i.attrs]] # copy from input
      attr.type <- typeof(attr.val)
    }
    ncdf4::ncatt_put(
      nc=output.epic.fh,
      varid=0,            # for file/global attributes
      attname=attr.name,
      attval=attr.val,
      prec=attr.type,
      definemode=FALSE,
# # to debug, toggle verbose
#       verbose=TRUE)
      verbose=FALSE)
  } # end for (i.attrs in 1:input.epic.attrs.n)
} # end if (input.epic.attrs.n > 0)

### 4. write data to output datavar
cat(sprintf('\n%s: 4. write data to output datavar\n', this_fn)) # debugging

## 4.1. write datavar data
cat(sprintf('\n%s: 4.1. write datavar data\n', this_fn)) # debugging

# # start debugging-----------------------------------------------------
# base::cat(base::sprintf('\n%s: about to attempt\n', this_fn))
# base::cat(base::sprintf('ncdf4::ncvar_put(\n'))
# base::cat(base::sprintf('  nc=output.epic.fh == %s\n', output_epic_fp))
# base::cat(base::sprintf('  varid=%s\n', output_epic_dv_name))
# base::cat(base::sprintf('  vals=output.epic.dv.arr == '))
# print(dim(output.epic.dv.arr))
# #   end debugging-----------------------------------------------------

ncdf4::ncvar_put(
  nc=output.epic.fh,
#   varid=output_epic_TFLAG_name,
  varid=output.epic.TFLAG,
  vals=output.epic.TFLAG.arr,
  start=output.epic.TFLAG.write.start,
  count=output.epic.TFLAG.write.count,
# # to debug, toggle verbose
#   verbose=TRUE)
  verbose=FALSE)

ncdf4::ncvar_put(
  nc=output.epic.fh,
#  varid=output.epic.dv,
  varid=output_epic_dv_name,
  vals=output.epic.dv.arr,
  start=output.epic.dv.write.start,
  count=output.epic.dv.write.count,
# # to debug, toggle verbose
#   verbose=TRUE)
  verbose=FALSE)

## 4.2. write datavar metadata (i.e., additional attributes)
cat(sprintf('\n%s: 4.2. write datavar metadata (i.e., additional attributes)\n', this_fn)) # debugging

ncdf4::ncatt_put(
  nc=output.epic.fh,
  varid=output.epic.TFLAG,
  attname='var_desc',
  attval=output.epic.TFLAG.var_desc,
  prec='text',
  definemode=FALSE,
# # to debug, toggle verbose
#   verbose=TRUE)
  verbose=FALSE)

ncdf4::ncatt_put(
  nc=output.epic.fh,
  varid=output.epic.dv,
  attname='var_desc',
  attval=output_epic_dv_var_desc,
  prec='text',
  definemode=FALSE,
# # to debug, toggle verbose
#   verbose=TRUE)
  verbose=FALSE)

### 5. close output file

ncdf4::nc_close(output.epic.fh)

### 6. remove output filehandle from the workspace/environment

rm(output.epic.fh)

# ----------------------------------------------------------------------
# close inputs
# ----------------------------------------------------------------------

### close RDS? apparently R "takes care of this"

### close EPIC
## close filehandle(s)
ncdf4::nc_close(input.epic.fh)
## remove the filehandle(s) (not files!) from the workspace/environment
rm(input.epic.fh)

# ----------------------------------------------------------------------
# report data failures (to stdout, no retval)
# ----------------------------------------------------------------------

cat('\n\n')
report.failures(
  gridcells.vec=record.gridcells,          # gridcells with product=0 
  beld.not.epic=record.have.beld.not.epic, # gridcells with BELD data but not EPIC data
  epic.not.beld=record.have.epic.not.beld, # gridcells with EPIC data but not BELD data
  gridcells.n=input.epic.dv.cells.n,       # total number of (spatial) gridcells (per layer)
  layers.n=input.epic.dim.layers.n         # total number of layers
)

#----------------------------------------------------------------------
# verify VERDI compatibility? output=EPIC emissions
#----------------------------------------------------------------------

  ### TODO: integrate VERDI into bash_utilities.sh
  ### plot our datavar of interest with VERDI
  output_epic_dv_name_VERDI <- sprintf('%s[1]', output_epic_dv_name)
  ## VERDI really wants an FQP
  output_epic_fp_VERDI <- system(sprintf('readlink -f %s', output_epic_fp), intern=TRUE)
  output_epic_dv_VERDI_cmd <-
    sprintf('verdi -f %s -s %s -gtype tile &',
      output_epic_fp_VERDI, output_epic_dv_name_VERDI)
  cat(sprintf("\n%s: debugging IOAPI: about to run '%s'\n", this_fn, output_epic_dv_VERDI_cmd))
#   cat(sprintf("\n%s: not debugging IOAPI: skipping '%s'\n", this_fn, output_epic_dv_VERDI_cmd))
  system(output_epic_dv_VERDI_cmd)

# ----------------------------------------------------------------------
# visualize output emissions
# ----------------------------------------------------------------------

output.epic.raster <- raster::raster(output_epic_fp, varname=output_epic_dv_name)
# something whacks the CRS, so
output.epic.raster@crs <- aqme.crs

# # start debugging-----------------------------------------------------
# cat('\noutput.epic.raster==\n')
# output.epic.raster
# # class       : RasterLayer 
# # dimensions  : 299, 459, 137241  (nrow, ncol, ncell)
# # resolution  : 12000, 12000  (x, y)
# # extent      : -2550000, 2958000, -1722000, 1866000  (xmin, xmax, ymin, ymax)
# # coord. ref. : +proj=lcc +lat_1=33 +lat_2=45 +lat_0=40 +lon_0=-97 +a=6370000 +b=6370000 
# # data source : .../5yravg_20111219_pure_emis.nc 
# # names       : N2O 
# # zvar        : N2O 
# 
# cat('output.epic.raster@extent==\n')
# output.epic.raster@extent
# # class       : Extent 
# # xmin        : -2550000 
# # xmax        : 2958000 
# # ymin        : -1722000 
# # ymax        : 1866000 
# #   end debugging-----------------------------------------------------

# TODO: if using output as args below, `project.NorAm.boundaries.for.CMAQ` fails with
# > Grid type 0 cannot be handled by this function.
# unless one adds IOAPI global attrs to output

### get projected North American map
NorAm.shp <- project.NorAm.boundaries.for.CMAQ(
  units='m',
#   extents.fp=input_epic_fp,
  extents.fp=output_epic_fp,
#   extents=input.epic.extents,
  extents=output.epic.raster@extent,
  LCC.parallels=c(33,45),
  CRS=aqme.crs)

# # start debugging
# class(NorAm.shp)
# bbox(NorAm.shp)
# # compare bbox to (above)
# # > input.epic.extents
# # class       : Extent 
# # xmin        : -2556000 
# # xmax        : 2952000 
# # ymin        : -1728000 
# # ymax        : 1860000 
# #   end debugging

visualize.layer(
# Why does '=' fail and '<-' succeed in ONE PLACE in the arg list?
  nc.fp=output_epic_fp,
  datavar.name=output_epic_dv_name,
  sigdigs=sigdigs,
  layer=output.epic.raster,
  map.list <- list(NorAm.shp),
  pdf.fp=output_epic_pdf_fp,
  pdf.height=output_epic_pdf_height,
  pdf.width=output_epic_pdf_width
)
