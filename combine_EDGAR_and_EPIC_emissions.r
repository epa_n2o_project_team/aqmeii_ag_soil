### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

# R code to combine emissions from EPIC and EDGAR, with preference given to the former. See ./README.md

# If running manually in R console, remember to run setup actions: `source ./AQMEII_driver.sh`

# ----------------------------------------------------------------------
# constants
# ----------------------------------------------------------------------

# all the following env vars must be set and exported in driver script
# TODO: pass as args!

### miscellany

## kludge for my clumsy namespacing
my_this_fn <- Sys.getenv('COMBINE_EMIS_FN', unset=NA)
this_fn <- my_this_fn
work_dir <- Sys.getenv('WORK_DIR', unset=NA)

### visualization

pdf_er <- Sys.getenv('PDF_VIEWER', unset=NA)
sigdigs <- as.numeric(Sys.getenv('OUTPUT_SIGNIFICANT_DIGITS', unset=NA))

## projection
global_proj4 <- Sys.getenv('GLOBAL_PROJ4', unset=NA)

## helpers

R_utils_fp <- Sys.getenv('R_UTILS_FP', unset=NA)
stat_funcs_fp <- Sys.getenv('STATS_FUNCS_FP', unset=NA)
vis_funcs_fp <- Sys.getenv('VIS_FUNCS_FP', unset=NA)

### inputs

## EDGAR N2O emissions regridded over AQMEII-NA

input_edgar_fp <- Sys.getenv('AQMEII_EDGAR_N2O_REGRID_FP', unset=NA) # path to output file

# file/global attributes
input_edgar_attr_nlays = as.numeric(Sys.getenv('AQMEII_EDGAR_N2O_REGRID_ATTR_NLAYS', unset=NA))
input_edgar_attr_nlays_type = Sys.getenv('AQMEII_EDGAR_N2O_REGRID_ATTR_NLAYS_TYPE', unset=NA)
input_edgar_attr_varlist = Sys.getenv('AQMEII_EDGAR_N2O_REGRID_ATTR_VARLIST', unset=NA)
input_edgar_attr_varlist_type = Sys.getenv('AQMEII_EDGAR_N2O_REGRID_ATTR_VARLIST_TYPE', unset=NA)
input_edgar_attr_filedesc = Sys.getenv('AQMEII_EDGAR_N2O_REGRID_ATTR_FILEDESC', unset=NA)
input_edgar_attr_filedesc_type = Sys.getenv('AQMEII_EDGAR_N2O_REGRID_ATTR_FILEDESC_TYPE', unset=NA)

# dimensions
input_edgar_dim_layers_name <-  Sys.getenv('AQMEII_EDGAR_N2O_REGRID_DIM_LAYERS_NAME', unset=NA)
input_edgar_dim_layers_units <- Sys.getenv('AQMEII_EDGAR_N2O_REGRID_DIM_LAYERS_ATTR_UNITS', unset=NA)
input_edgar_dim_layers_long_name <- Sys.getenv('AQMEII_EDGAR_N2O_REGRID_DIM_LAYERS_ATTR_LONG_NAME', unset=NA)
input_edgar_dim_x_name <- Sys.getenv('AQMEII_EDGAR_N2O_REGRID_DIM_X_NAME', unset=NA)
input_edgar_dim_x_units <- Sys.getenv('AQMEII_EDGAR_N2O_REGRID_DIM_X_ATTR_UNITS', unset=NA)
input_edgar_dim_x_long_name <- Sys.getenv('AQMEII_EDGAR_N2O_REGRID_DIM_X_ATTR_LONG_NAME', unset=NA)
input_edgar_dim_y_name <- Sys.getenv('AQMEII_EDGAR_N2O_REGRID_DIM_Y_NAME', unset=NA)
input_edgar_dim_y_units <- Sys.getenv('AQMEII_EDGAR_N2O_REGRID_DIM_Y_ATTR_UNITS', unset=NA)
input_edgar_dim_y_long_name <- Sys.getenv('AQMEII_EDGAR_N2O_REGRID_DIM_Y_ATTR_LONG_NAME', unset=NA)

# datavars
input_edgar_dv_name <- Sys.getenv('AQMEII_EDGAR_N2O_REGRID_DATAVAR_NAME', unset=NA)
input_edgar_dv_units <- Sys.getenv('AQMEII_EDGAR_N2O_REGRID_DATAVAR_ATTR_UNITS', unset=NA)
input_edgar_dv_long_name <- Sys.getenv('AQMEII_EDGAR_N2O_REGRID_DATAVAR_ATTR_LONG_NAME', unset=NA)
input_edgar_dv_var_desc <- Sys.getenv('AQMEII_EDGAR_N2O_REGRID_DATAVAR_ATTR_VAR_DESC', unset=NA)

## EPIC N2O emissions over AQMEII-NA (actually CONUS): reunit-ed, monotonic, VERDI-compliant

input_epic_fp <- Sys.getenv('AQMEII_EPIC_N2O_EMISS_FP', unset=NA) # path to output file

# file/global attributes
input_epic_attr_nlays = as.numeric(Sys.getenv('AQMEII_EPIC_N2O_EMISS_ATTR_NLAYS', unset=NA))
input_epic_attr_nlays_type = Sys.getenv('AQMEII_EPIC_N2O_EMISS_ATTR_NLAYS_TYPE', unset=NA)
input_epic_attr_varlist = Sys.getenv('AQMEII_EPIC_N2O_EMISS_ATTR_VARLIST', unset=NA)
input_epic_attr_varlist_type = Sys.getenv('AQMEII_EPIC_N2O_EMISS_ATTR_VARLIST_TYPE', unset=NA)
input_epic_attr_filedesc = Sys.getenv('AQMEII_EPIC_N2O_EMISS_ATTR_FILEDESC', unset=NA)
input_epic_attr_filedesc_type = Sys.getenv('AQMEII_EPIC_N2O_EMISS_ATTR_FILEDESC_TYPE', unset=NA)

# dimensions
input_epic_dim_datetimes_name <-  Sys.getenv('AQMEII_EPIC_N2O_EMISS_DIM_DATETIME_NAME', unset=NA)
input_epic_dim_layers_name <-  Sys.getenv('AQMEII_EPIC_N2O_EMISS_DIM_LAYER_NAME', unset=NA)
input_epic_dim_layers_units <- Sys.getenv('AQMEII_EPIC_N2O_EMISS_DIM_LAYER_ATTR_UNITS', unset=NA)
input_epic_dim_layers_long_name <- Sys.getenv('AQMEII_EPIC_N2O_EMISS_DIM_LAYER_ATTR_LONG_NAME', unset=NA)
input_epic_dim_tsteps_name <-  Sys.getenv('AQMEII_EPIC_N2O_EMISS_DIM_TSTEP_NAME', unset=NA)
input_epic_dim_vars_name <-  Sys.getenv('AQMEII_EPIC_N2O_EMISS_DIM_VAR_NAME', unset=NA)
input_epic_dim_x_name <- Sys.getenv('AQMEII_EPIC_N2O_EMISS_DIM_X_NAME', unset=NA)
input_epic_dim_x_units <- Sys.getenv('AQMEII_EPIC_N2O_EMISS_DIM_X_ATTR_UNITS', unset=NA)
input_epic_dim_x_long_name <- Sys.getenv('AQMEII_EPIC_N2O_EMISS_DIM_X_ATTR_LONG_NAME', unset=NA)
input_epic_dim_y_name <- Sys.getenv('AQMEII_EPIC_N2O_EMISS_DIM_Y_NAME', unset=NA)
input_epic_dim_y_units <- Sys.getenv('AQMEII_EPIC_N2O_EMISS_DIM_Y_ATTR_UNITS', unset=NA)
input_epic_dim_y_long_name <- Sys.getenv('AQMEII_EPIC_N2O_EMISS_DIM_Y_ATTR_LONG_NAME', unset=NA)

# datavars
input_epic_dv_name <- Sys.getenv('AQMEII_EPIC_N2O_EMISS_DATAVAR_NAME', unset=NA)
input_epic_dv_units <- Sys.getenv('AQMEII_EPIC_N2O_EMISS_DATAVAR_ATTR_UNITS', unset=NA)
input_epic_dv_long_name <- Sys.getenv('AQMEII_EPIC_N2O_EMISS_DATAVAR_ATTR_LONG_NAME', unset=NA)
input_epic_dv_var_desc <- Sys.getenv('AQMEII_EPIC_N2O_EMISS_DATAVAR_ATTR_VAR_DESC', unset=NA)
# "fake" datavar=TFLAG, required by IOAPI (and more to the point (IIUC), VERDI)
input_epic_TFLAG_name <- Sys.getenv('AQMEII_EPIC_N2O_EMISS_TFLAG_NAME', unset=NA)
# input_epic_TFLAG_var_desc <-  Sys.getenv('AQMEII_EPIC_N2O_EMISS_TFLAG_ATTR_VAR_DESC', unset=NA) # get/copy programmatically

## outputs

## combined N2O emissions, VERDI-compliant

output_both_fp <- Sys.getenv('AQMEII_BOTH_N2O_EMISS_FP', unset=NA) # path to output file

# file/global attributes
output_both_attr_nlays = as.numeric(Sys.getenv('AQMEII_BOTH_N2O_EMISS_ATTR_NLAYS', unset=NA))
output_both_attr_nlays_type = Sys.getenv('AQMEII_BOTH_N2O_EMISS_ATTR_NLAYS_TYPE', unset=NA)
output_both_attr_varlist = Sys.getenv('AQMEII_BOTH_N2O_EMISS_ATTR_VARLIST', unset=NA)
output_both_attr_varlist_type = Sys.getenv('AQMEII_BOTH_N2O_EMISS_ATTR_VARLIST_TYPE', unset=NA)
output_both_attr_filedesc = Sys.getenv('AQMEII_BOTH_N2O_EMISS_ATTR_FILEDESC', unset=NA)
output_both_attr_filedesc_type = Sys.getenv('AQMEII_BOTH_N2O_EMISS_ATTR_FILEDESC_TYPE', unset=NA)

# dimensions
output_both_dim_datetimes_name <-  Sys.getenv('AQMEII_BOTH_N2O_EMISS_DIM_DATETIME_NAME', unset=NA)
output_both_dim_tsteps_name <-  Sys.getenv('AQMEII_BOTH_N2O_EMISS_DIM_TSTEP_NAME', unset=NA)
output_both_dim_vars_name <-  Sys.getenv('AQMEII_BOTH_N2O_EMISS_DIM_VAR_NAME', unset=NA)
output_both_dim_layers_name <-  Sys.getenv('AQMEII_BOTH_N2O_EMISS_DIM_LAYERS_NAME', unset=NA)
output_both_dim_layers_units <- Sys.getenv('AQMEII_BOTH_N2O_EMISS_DIM_LAYERS_ATTR_UNITS', unset=NA)
output_both_dim_layers_long_name <- Sys.getenv('AQMEII_BOTH_N2O_EMISS_DIM_LAYERS_ATTR_LONG_NAME', unset=NA)
output_both_dim_x_name <- Sys.getenv('AQMEII_BOTH_N2O_EMISS_DIM_X_NAME', unset=NA)
output_both_dim_x_units <- Sys.getenv('AQMEII_BOTH_N2O_EMISS_DIM_X_ATTR_UNITS', unset=NA)
output_both_dim_x_long_name <- Sys.getenv('AQMEII_BOTH_N2O_EMISS_DIM_X_ATTR_LONG_NAME', unset=NA)
output_both_dim_y_name <- Sys.getenv('AQMEII_BOTH_N2O_EMISS_DIM_Y_NAME', unset=NA)
output_both_dim_y_units <- Sys.getenv('AQMEII_BOTH_N2O_EMISS_DIM_Y_ATTR_UNITS', unset=NA)
output_both_dim_y_long_name <- Sys.getenv('AQMEII_BOTH_N2O_EMISS_DIM_Y_ATTR_LONG_NAME', unset=NA)

# datavars
output_both_dv_name <- Sys.getenv('AQMEII_BOTH_N2O_EMISS_DATAVAR_NAME', unset=NA)
output_both_dv_units <- Sys.getenv('AQMEII_BOTH_N2O_EMISS_DATAVAR_ATTR_UNITS', unset=NA)
output_both_dv_long_name <- Sys.getenv('AQMEII_BOTH_N2O_EMISS_DATAVAR_ATTR_LONG_NAME', unset=NA)
output_both_dv_var_desc <- Sys.getenv('AQMEII_BOTH_N2O_EMISS_DATAVAR_ATTR_VAR_DESC', unset=NA)
# "fake" datavar=TFLAG, required by IOAPI (and more to the point (IIUC), VERDI)
output_both_TFLAG_name <- Sys.getenv('AQMEII_BOTH_N2O_EMISS_TFLAG_NAME', unset=NA)
# output_both_TFLAG_var_desc <-  Sys.getenv('AQMEII_BOTH_N2O_EMISS_TFLAG_ATTR_VAR_DESC', unset=NA) # get/copy programmatically

# visualization

output_both_pdf_fp <- Sys.getenv('AQMEII_BOTH_N2O_EMISS_PDF_FP', unset=NA) # path to plot file
output_both_pdf_height <- as.numeric(Sys.getenv('SINGLE_FRAME_PLOT_HEIGHT', unset=NA))
output_both_pdf_width <- as.numeric(Sys.getenv('SINGLE_FRAME_PLOT_WIDTH', unset=NA))

# ----------------------------------------------------------------------
# functions
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# external functions (referenced)
# ----------------------------------------------------------------------

source(R_utils_fp)
source(stat_funcs_fp) # in script, produces errant error=
#> netCDF.stats.to.stdout.r: no arguments supplied, exiting
source(vis_funcs_fp)

# ----------------------------------------------------------------------
# internal functions (defined)
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# code
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# setup
# ----------------------------------------------------------------------

# accelerate R graphics over SSH, per Adam Wilson http://planetflux.adamwilson.us/2012/03/r-graphics-via-ssh.html
X11.options(type="Xlib")

### visualization

## AQMEII-NA coordinate reference system:
# use package=M3 to get CRS from input
library(M3)
# aqme.proj4 <- M3::get.proj.info.M3(template_input_fp)
aqme.proj4 <- M3::get.proj.info.M3(input_epic_fp)
# base::cat(base::sprintf('aqme.proj4=%s\n', aqme.proj4)) # debugging
# aqme.proj4=+proj=lcc +lat_1=33 +lat_2=45 +lat_0=40 +lon_0=-97 +a=6370000 +b=6370000
aqme.crs <- sp::CRS(aqme.proj4)

## use package=M3 to get extents from input
input.epic.extents.info <- M3::get.grid.info.M3(input_epic_fp)
input.epic.extents.xmin <- input.epic.extents.info$x.orig
input.epic.extents.xmax <- max(
  M3::get.coord.for.dimension(
    file=input_epic_fp, dimension="col", position="upper", units="m")$coords)
input.epic.extents.ymin <- input.epic.extents.info$y.orig
input.epic.extents.ymax <- max(
  M3::get.coord.for.dimension(
    file=input_epic_fp, dimension="row", position="upper", units="m")$coords)
grid.res <- c(input.epic.extents.info$x.cell.width, input.epic.extents.info$y.cell.width) # units=m

library(raster)
input.epic.extents <-
  raster::extent(input.epic.extents.xmin, input.epic.extents.xmax, input.epic.extents.ymin, input.epic.extents.ymax)
# input.epic.extents # debugging
# class       : Extent 
# xmin        : -2556000 
# xmax        : 2952000 
# ymin        : -1728000 
# ymax        : 1860000 

# ----------------------------------------------------------------------
# payload
# ----------------------------------------------------------------------

### Note on dimensionality in the following: the term 'dimension' has very different meanings in
### * base R, where a dimensions is usually just an int or vector of ints
### * R package=ncdf4, where a dimensions is an object of type=ncdim4

# ----------------------------------------------------------------------
# open inputs
# ----------------------------------------------------------------------

library(ncdf4)

### EDGAR N2O emissions

input.edgar.fh <- ncdf4::nc_open(input_edgar_fp, write=FALSE, readunlim=FALSE)
input.edgar.arr <- ncdf4::ncvar_get(input.edgar.fh, varid=input_edgar_dv_name)
input.edgar.dv <- input.edgar.fh$var[[input_edgar_dv_name]] # note double brackets!
input.edgar.dv.type <- input.edgar.dv$prec
input.edgar.dv.mv <- input.edgar.dv$missval
input.edgar.dim.x <- input.edgar.fh$dim[[input_edgar_dim_x_name]]
input.edgar.dim.y <- input.edgar.fh$dim[[input_edgar_dim_y_name]]

### EPIC N2O emissions

input.epic.fh <- ncdf4::nc_open(input_epic_fp, write=FALSE, readunlim=FALSE)
# input.epic.dv <- ncdf4::ncvar_get(input.epic.fh, varid=input_epic_dv_name)
# `ncdf4::ncvar_get` gets data, not metadata
input.epic.dv.arr <- ncdf4::ncvar_get(input.epic.fh, varid=input_epic_dv_name)
input.epic.dv <- input.epic.fh$var[[input_epic_dv_name]] # note double brackets!

## get input datavar type, missing value, long_name, units useful for output
# input.epic.dv.attr.list <- ncdf4::ncatt_get(
#   nc=input.epic.fh, varid=input_epic_dv_name, attname=NA, verbose=FALSE)
# input.epic.dv.mv <- input.epic.dv.attr.list$`_FillValue` # note required backquotes
# instead, more consistently (e.g., $prec cannot be gotten with `ncdf4::ncatt_get`)
input.epic.dv.type <- input.epic.dv$prec
input.epic.dv.mv <- input.epic.dv$missval
# get these from driver
# input.epic.dv.units <- input.epic.dv$units
# input.epic.dv.long_name <- input.epic.dv$long_name
# input.epic.dv.var_desc <- input.epic.dv$var_desc

## "fake" datavar=TFLAG, for IOAPI and (IIUC) VERDI
input.epic.TFLAG.arr <- ncdf4::ncvar_get(input.epic.fh, varid=input_epic_TFLAG_name)
input.epic.TFLAG.dims <- dim(input.epic.TFLAG.arr)
input.epic.TFLAG.dims.n <- length(input.epic.TFLAG.dims)
input.epic.TFLAG <- input.epic.fh$var[[input_epic_TFLAG_name]]
input.epic.TFLAG.type <- input.epic.TFLAG$prec
input.epic.TFLAG.mv <- input.epic.TFLAG$missval

### get input dimensions useful for output (i.e., of type=ncdf4, not type=list)
# # print(input.epic.fh$dim)
# output.epic.dim.y <- input.epic.fh$dim[2]
# output.epic.dim.x <- input.epic.fh$dim[3]
input.epic.dim.y <- input.epic.fh$dim[[input_epic_dim_y_name]]
input.epic.dim.x <- input.epic.fh$dim[[input_epic_dim_x_name]]
input.epic.dim.tsteps <- input.epic.fh$dim[[input_epic_dim_tsteps_name]]
input.epic.dim.layers <- input.epic.fh$dim[[input_epic_dim_layers_name]]
input.epic.dim.vars <- input.epic.fh$dim[[input_epic_dim_vars_name]]
input.epic.dim.datetimes <- input.epic.fh$dim[[input_epic_dim_datetimes_name]]

# ----------------------------------------------------------------------
# get input and output dimensionalities
# ----------------------------------------------------------------------

### ASSERT: EDGAR input regridded over AQMEII-NA, EPIC input (native over AQMEII-NA), and output should all have same spatiotemporality:
### * COL = 459, ROW = 299
### * no layers
### * no timesteps
### * float N2O(ROW, COL) in mol/s
### missing_value may differ, however!
### and prefer metadata from EPIC (it's more IOAPI-ish)

### EDGAR input

input.edgar.dv.dims <- dim(input.edgar.arr)
input.edgar.dv.dims.n <- length(input.edgar.dv.dims)
input.edgar.dim.x.n <- input.edgar.dim.x$len
input.edgar.dim.y.n <- input.edgar.dim.y$len
input.edgar.dv.cells.n <- input.edgar.dim.y.n * input.edgar.dim.x.n # spatial only

### Both EPIC input and output have same spatial grid, input also has layers=1 and tsteps (probably=1)

### EPIC input

## EPIC input dims

input.epic.dim.datetimes <- input.epic.fh$dim[[input_epic_dim_datetimes_name]]
input.epic.dim.layers <- input.epic.fh$dim[[input_epic_dim_layers_name]]
input.epic.dim.tsteps <- input.epic.fh$dim[[input_epic_dim_tsteps_name]]
input.epic.dim.vars <- input.epic.fh$dim[[input_epic_dim_vars_name]]
input.epic.dim.x <- input.epic.fh$dim[[input_epic_dim_x_name]]
input.epic.dim.y <- input.epic.fh$dim[[input_epic_dim_y_name]]

input.epic.dim.datetimes.n <- input.epic.dim.datetimes$len
input.epic.dim.layers.n <- input.epic.dim.layers$len
input.epic.dim.tsteps.n <- input.epic.dim.tsteps$len
input.epic.dim.vars.n <- input.epic.dim.vars$len
input.epic.dim.x.n <- input.epic.dim.x$len
input.epic.dim.y.n <- input.epic.dim.y$len

## EPIC input "real" datavar

input.epic.dv.dims <- dim(input.epic.dv.arr)
input.epic.dv.dims.n <- length(input.epic.dv.dims)
# spatial cells only: ignore layers, tsteps
input.epic.dv.cells.n <- input.epic.dim.y.n * input.epic.dim.x.n

## EPIC input TFLAG

input.epic.TFLAG.dims <- dim(input.epic.TFLAG.arr)
input.epic.TFLAG.dims.n <- length(input.epic.TFLAG.dims)

### combined output

output.both.dim.datetimes.n <- input.epic.dim.datetimes.n
output.both.dim.layers.n <- input.epic.dim.layers.n
output.both.dim.tsteps.n <- input.epic.dim.tsteps.n
output.both.dim.vars.n <- input.epic.dim.vars.n
output.both.dim.x.n <- input.epic.dim.x.n
output.both.dim.y.n <- input.epic.dim.y.n

output.both.dv.cells.n <- input.epic.dv.cells.n
output.both.dv.dims <-
  c(output.both.dim.x.n, output.both.dim.y.n, output.both.dim.layers.n, output.both.dim.tsteps.n)
output.both.dv.dims.n <- length(output.both.dv.dims)

# ----------------------------------------------------------------------
# read inputs
# ----------------------------------------------------------------------

### EDGAR input

## TODO: "Pierce-style read" (see help(ncvar_get)#Examples): protects against data overload (somewhat) but more complicated to implement.
## Normally one reads one timestep at a time, but EDGAR input has no timesteps (other than the implicit/degenerate model year).

### Still need to set read start point and read count (both by datavar dimensions)?
input.edgar.read.start <- rep(1, input.edgar.dv.dims.n)   # start=(1,1,1,...)
input.edgar.read.count <- input.edgar.dv.dims             # count=(n,n,n,...)

### Get input data
input.edgar.arr <-
  ncdf4::ncvar_get(input.edgar.fh, varid=input_edgar_dv_name, start=input.edgar.read.start, count=input.edgar.read.count)

##### TODO: why do I need to reverse EDGAR rows horizontally? #####
# Yet if I do not mirror EDGAR around center horizontally, I get ghosting of Mexico in Canada, and ghosting of Canada (and Vermont hotspot) offshore
# so for now: reverse EDGAR rows horizontally (by reversing (only) the row indices)
input.edgar.arr <- input.edgar.arr[,input.edgar.dim.y.n:1]

# start debugging-----------------------------------------------------
base::cat(base::sprintf('\n%s: dim(input.edgar.arr)==', this_fn))
print(dim(input.edgar.arr))
stats.to.stdout(
  data=input.edgar.arr,
  sig.digs=sigdigs,
  title=base::sprintf('\n%s: EDGAR input emissions=', this_fn)
)

# combine_EDGAR_and_EPIC_emissions.r: EDGAR input emissions=
#         cells=137241
#         obs=137241
#         min=0
#         q1=0
#         med=0.000157
#         mean=0.00189
#         q3=0.00312
#         max=0.0861
#         sum=259
#   end debugging-----------------------------------------------------

### EPIC input

## TODO: "Pierce-style read" (see help(ncvar_get)#Examples): protects against data overload (somewhat)
## but more complicated to implement. Copy code from (github)ioapi-hack-R/computeCropSum.r
## Normally one reads one timestep at a time, but EPIC input has no timesteps--
## other than the implicit/degenerate model year ... which, for VERDI-compliance, it appears we must use :-(
## So gotta deal with implicit/degenerate IOAPI dimensions ...

# so gotta handle both LAY=1 and TSTEP=1: remembering timelike dim is always the LAST dimension!
if      (input.epic.dv.dims.n < 2) {
  # TODO: throw
  stop(sprintf('%s: ERROR: dimensionality=%i < 2 of EPIC input datavar=%s, dimensions are',
    this_fn, input.epic.dv.dims.n, input_epic_dv_name))
  print(input.epic.dv.dims)

} else if (input.epic.dv.dims.n == 2) {
  if ((input.epic.dim.tsteps.n != 1) && (input.epic.dim.lay.n != 1)) {
    stop(sprintf('%s: ERROR: dimensionality=1 of EPIC input datavar=%s, but dim(TSTEP)=%i and dim(LAY)=%i',
      this_fn, input_epic_TFLAG_name, input.epic.dim.tsteps.n, input.epic.dim.lays.n))
  }
  # append TSTEP, LAY -> (TSTEP, LAY, ROW, COL) ... but reversed :-(
  input.epic.dv.dims <- c(input.epic.dv.dims, 1, 1)
  input.epic.dv.dims.n <- 4
  input.epic.dv.read.start <- rep(1, input.epic.dv.dims.n)   # start=(1,1,1,...)
  input.epic.dv.read.count <- input.epic.dv.dims             # count=(n,n,n,...)

# start debugging-------------------------------------------
  cat(sprintf('\n%s: dimensionality=%i (was 2) of EPIC input datavar=%s, dimensions are',
    this_fn, input.epic.dv.dims.n, input_epic_dv_name))
  print(input.epic.dv.dims)
#   end debugging-------------------------------------------

} else if (input.epic.dv.dims.n == 3) {
  if        (input.epic.dim.tsteps.n == 1) {
    # append TSTEPs
    input.epic.dv.dims <- c(input.epic.dv.dims, 1)
  } else if  (input.epic.dim.lays.n == 1) {
    # insert LAYs
    input.epic.dv.dims <-
      c(input.epic.dim.x.n, input.epic.dim.y.n, 1, input.epic.dim.tsteps.n)
  } else {
    stop(sprintf('%s: ERROR: dimensionality=3 of EPIC input datavar=%s, but dim(TSTEP)==%i and dim(LAY)==%i',
      this_fn, input_epic_dv_name, input.epic.dim.tsteps.n, input.epic.dim.lays.n))
  } # end testing LAYs, TSTEPs
  input.epic.dv.dims.n <- 4
  input.epic.dv.read.start <- rep(1, input.epic.dv.dims.n)
  input.epic.dv.read.count <- input.epic.dv.dims

# start debugging-------------------------------------------
  cat(sprintf('\n%s: dimensionality=%i (was 3) of EPIC input datavar=%s, dimensions are',
    this_fn, input.epic.dv.dims.n, input_epic_dv_name))
  print(input.epic.dv.dims)
#   end debugging-------------------------------------------

} else if (input.epic.dv.dims.n == 4) {
# start debugging-------------------------------------------
  cat(sprintf('\n%s: dimensionality=4 of EPIC input datavar=%s, dimensions are',
    this_fn, input_epic_dv_name))
  print(input.epic.dv.dims)
#   end debugging-------------------------------------------
  input.epic.dv.read.start <- rep(1, input.epic.dv.dims.n)
  input.epic.dv.read.count <- input.epic.dv.dims

} else {
  # TODO: throw
  stop(sprintf('%s: ERROR: dimensionality=%i > 4 of EPIC input datavar=%s, dimensions are',
    this_fn, input.epic.dv.dims.n, input_epic_dv_name))
  print(input.epic.dv.dims)
} # end testing input.epic.dv.dims.n

### Get input data
input.epic.dv.arr <-
  ncdf4::ncvar_get(input.epic.fh, varid=input_epic_dv_name, start=input.epic.dv.read.start, count=input.epic.dv.read.count)
## Once again, caught between IOAPI (which demands tstep=1) and netCDF (which strips tstep=1) :-(
dim(input.epic.dv.arr) <- input.epic.dv.dims

# start debugging-----------------------------------------------------
base::cat(base::sprintf('\n%s: dim(input.epic.dv.arr)==', this_fn))
print(dim(input.epic.dv.arr))
stats.to.stdout(
  data=input.epic.dv.arr,
  sig.digs=sigdigs,
  title=base::sprintf('\n%s: EPIC input emissions=', this_fn)
)

# combine_EDGAR_and_EPIC_emissions.r: EPIC input emissions=
#         cells=137241
#         obs=40763
#         min=0
#         q1=1.02e-05
#         med=0.000171
#         mean=0.00137
#         q3=0.00111
#         max=0.0757
#         sum=56         [after removing outlier]
#   end debugging-----------------------------------------------------

### input TFLAG

## ... gotta deal with implicit/degenerate TSTEP==1 again!
## Remembering timelike dim is always the LAST dimension!
if      (input.epic.TFLAG.dims.n < 1) {
  # TODO: throw
  stop(sprintf('%s: ERROR: dimensionality=%i < 1 of EPIC input TFLAG=%s',
    this_fn, input.epic.TFLAG.dims.n, input_epic_TFLAG_name))

} else if (input.epic.TFLAG.dims.n == 1) {
  if ((input.epic.dim.tsteps.n != 1) && (input.epic.dim.var.n != 1)) {
    stop(sprintf('%s: ERROR: dimensionality=1 of EPIC input TFLAG=%s, but dim(TSTEP)=%i and dim(VAR)=%i',
      this_fn, input_epic_TFLAG_name, input.epic.dim.tsteps.n, input.epic.dim.vars.n))
  }
  # append TSTEP, VAR -> (TSTEP, VAR, DATE-TIME) ... but reversed :-(
  input.epic.TFLAG.dims <- c(input.epic.TFLAG.dims, 1, 1)
  input.epic.TFLAG.dims.n <- 3
# start debugging-------------------------------------------
  cat(sprintf('\n%s: dimensionality=%i (was 1) of EPIC input TFLAG=%s, dimensions are',
    this_fn, input.epic.TFLAG.dims.n, input_epic_TFLAG_name))
  print(input.epic.TFLAG.dims)
#   end debugging-------------------------------------------

} else if (input.epic.TFLAG.dims.n == 2) {
  # which |dim|==1 ???
  if        (input.epic.dim.tsteps.n == 1) {
    input.epic.TFLAG.dims <-
      c(input.epic.dim.datetime.n, input.epic.dim.vars.n, 1)
  } else if (input.epic.dim.var.n == 1) {
    input.epic.TFLAG.dims <-
      c(input.epic.dim.datetime.n, 1, input.epic.dim.tsteps.n)
  }
  input.epic.TFLAG.dims.n <- 3
# start debugging-------------------------------------------
  cat(sprintf('\n%s: dimensionality=%i (was 2) of EPIC input TFLAG=%s, dimensions are',
    this_fn, input.epic.TFLAG.dims.n, input_epic_TFLAG_name))
  print(input.epic.TFLAG.dims)
#   end debugging-------------------------------------------

} else if (input.epic.TFLAG.dims.n == 3) {

  # life is good ...
  cat(sprintf('\n%s: dimensionality=3 of EPIC input TFLAG=%s, dimensions are',
    this_fn, input_epic_TFLAG_name))
  print(input.epic.TFLAG.dims)

} else {
  # TODO: throw
  stop(sprintf('%s: ERROR: dimensionality=%i > 3 of EPIC input TFLAG=%s',
    this_fn, input.epic.TFLAG.dims.n, input_epic_TFLAG_name))
} # end testing input.epic.TFLAG.dims.n
input.epic.TFLAG.read.start <- rep(1, input.epic.TFLAG.dims.n)
input.epic.TFLAG.read.count <- input.epic.TFLAG.dims

# # ----------------------------------------------------------------------
# # compare EPIC to EDGAR
# # ----------------------------------------------------------------------

# ### TODO: make dimensions of EPIC input match EDGAR input
# epic.gt.edgar.arr <- input.epic.dv.arr - input.edgar.arr

# # start debugging-----------------------------------------------------
# base::cat(base::sprintf('\n%s: dim(epic.gt.edgar.arr)==', this_fn))
# print(dim(epic.gt.edgar.arr))
# stats.to.stdout(
#   data=epic.gt.edgar.arr,
#   sig.digs=sigdigs,
#   title=base::sprintf('\n%s: EPIC - EDGAR=', this_fn)
# )

# # combine_EDGAR_and_EPIC_emissions.r: EPIC - EDGAR=
# #         cells=137241
# #         obs=40764
# #         min=-0.0856
# #         q1=-0.00539
# #         med=-0.00229
# #         mean=-0.00249
# #         q3=-6.25e-06
# #         max=0.28
# #         sum=-102
# #   end debugging-----------------------------------------------------

# ### TODO: *plot* differences between EPIC input match EDGAR input

# ----------------------------------------------------------------------
# setup output data container(s)
# ----------------------------------------------------------------------

### TFLAG

output.both.TFLAG.dims <- input.epic.TFLAG.dims # (TSTEP, VAR, DATE-TIME), except ...
output.both.TFLAG.dims.n <- length(output.both.TFLAG.dims)
output.both.TFLAG.arr <- input.epic.TFLAG.arr
output.both.TFLAG.write.start <- input.epic.TFLAG.read.start
output.both.TFLAG.write.count <- input.epic.TFLAG.read.count

# # start debugging-------------------------------------------
# cat(sprintf('%s: output.both.TFLAG.dims==', this_fn))
# print(output.both.TFLAG.dims)
# cat(sprintf('%s: output.both.TFLAG.write.start==', this_fn))
# print(output.both.TFLAG.write.start)
# cat(sprintf('%s: output.both.TFLAG.write.count==', this_fn))
# print(output.both.TFLAG.write.count)
# #   end debugging-------------------------------------------

### the real datavar

output.both.dv.arr <- array(NA, output.both.dv.dims)
output.both.dv.write.start <- rep(1, output.both.dv.dims.n)   # start=(1,1,1,...)
output.both.dv.write.count <- output.both.dv.dims             # count=(n,n,n,...)

# # start debugging-----------------------------------------------------
# base::cat(base::sprintf('\n%s: output.both.write.start==', this_fn))
# print(output.both.write.start)
# base::cat(base::sprintf('%s: output.both.write.count==', this_fn))
# print(output.both.write.count)
# #   end debugging-----------------------------------------------------

# ----------------------------------------------------------------------
# combine inputs to create output
# ----------------------------------------------------------------------

# start debugging-----------------------------------------------------
base::cat(base::sprintf('\n%s: about to combine EDGAR and EPIC crop emissions\n', this_fn))
#   end debugging-----------------------------------------------------

## regarding copy criteria, see https://stat.ethz.ch/pipermail/r-help/2013-May/353195.html
## much more elegant than (all-the-way) nested `for` loops. thanks, Bill Dunlap!

### ASSERT: EDGAR is a simple spatial grid (ROW, COL), while EPIC is a grid of the same spatiality possibly embedded in LAY and TSTEP.

### type1: use values from EDGAR only where EPIC value is missing/NA

output.both.dv.arr.type1 <- input.epic.dv.arr
for (i.tstep in 1:output.both.dim.tsteps.n) {
  for (i.lay in 1:output.both.dim.layers.n) {
    input.epic.grid <- input.epic.dv.arr[,,i.lay,i.tstep]
    output.grid <- input.epic.grid
    # (EDGAR-4.2 is never NA, but I'll test anyway, for safety)
    copy.criterion.type1 <-
      is.na(input.epic.grid) & !is.na(input.edgar.arr)
    output.grid[copy.criterion.type1] <- input.edgar.arr[copy.criterion.type1]
    output.both.dv.arr.type1[,,i.lay,i.tstep] <- output.grid
  }
}

# start debugging-----------------------------------------------------
base::cat(base::sprintf('\n%s: dim(output.both.dv.arr.type1)==', this_fn))
print(dim(output.both.dv.arr.type1))
stats.to.stdout(
  data=output.both.dv.arr.type1,
  sig.digs=sigdigs,
  title=base::sprintf('\n%s: combined (type1) output emissions=', this_fn)
)

# type1 << EDGAR:

# combine_EDGAR_and_EPIC_emissions.r: combined (type1) output emissions=
#         cells=137241
#         obs=137241
#         min=0
#         q1=0
#         med=1.83e-05
#         mean=0.000884
#         q3=0.000673
#         max=0.0757
#         sum=121

# from above:

# combine_EDGAR_and_EPIC_emissions.r: EDGAR input emissions=
#         cells=137241
#         obs=137241
#         min=0
#         q1=0
#         med=0.000157
#         mean=0.00189
#         q3=0.00312
#         max=0.0861
#         sum=259

#   end debugging-----------------------------------------------------

### type2: use values from EDGAR only where EPIC value is zero or missing/NA (and EDGAR is not)

output.both.dv.arr.type2 <- input.epic.dv.arr
for (i.tstep in 1:output.both.dim.tsteps.n) {
  for (i.lay in 1:output.both.dim.layers.n) {
    input.epic.grid <- input.epic.dv.arr[,,i.lay,i.tstep]
    output.grid <- input.epic.grid
    # (EDGAR-4.2 is never NA, but I'll test anyway, for safety)
    copy.criterion.type2 <-
      is.arr.na.or.zero(input.epic.grid) & !is.arr.na.or.zero(input.edgar.arr)
    output.grid[copy.criterion.type2] <- input.edgar.arr[copy.criterion.type2]
    output.both.dv.arr.type2[,,i.lay,i.tstep] <- output.grid
  }
}

# start debugging-----------------------------------------------------
base::cat(base::sprintf('\n%s: dim(output.both.dv.arr.type2)==', this_fn))
print(dim(output.both.dv.arr.type2))
stats.to.stdout(
  data=output.both.dv.arr.type2,
  sig.digs=sigdigs,
  title=base::sprintf('\n%s: combined (type2) output emissions=', this_fn)
)

# type2 vs type1: type2 has many fewer obs, so although it has larger values except for {max, sum} ... it has the same damn sum

# combine_EDGAR_and_EPIC_emissions.r: combined (type2) output emissions=
#         cells=137241
#         obs=89288
#         min=1.02e-10
#         q1=2.28e-05
#         med=0.000269
#         mean=0.00136
#         q3=0.00176
#         max=0.0757
#         sum=121

# type2 << EDGAR: from above:

# combine_EDGAR_and_EPIC_emissions.r: EDGAR input emissions=
#         cells=137241
#         obs=137241
#         min=0
#         q1=0
#         med=0.000157
#         mean=0.00189
#         q3=0.00312
#         max=0.0861
#         sum=259

### type3: use values from EPIC only where EDGAR value is zero or NA

output.both.dv.arr.type3 <- input.epic.dv.arr # can't use EDGAR: dims don't match
for (i.tstep in 1:output.both.dim.tsteps.n) {
  for (i.lay in 1:output.both.dim.layers.n) {
    input.epic.grid <- input.epic.dv.arr[,,i.lay,i.tstep]
    output.grid <- input.edgar.arr
    # (EDGAR-4.2 is never NA, but I'll test anyway, for safety)
    copy.criterion.type3 <-
      is.arr.na.or.zero(input.edgar.arr) & !is.arr.na.or.zero(input.epic.grid)
    output.grid[copy.criterion.type3] <- input.epic.grid[copy.criterion.type3]
    output.both.dv.arr.type3[,,i.lay,i.tstep] <- output.grid
  }
}

# start debugging-----------------------------------------------------
base::cat(base::sprintf('\n%s: dim(output.both.dv.arr.type3)==', this_fn))
print(dim(output.both.dv.arr.type3))
stats.to.stdout(
  data=output.both.dv.arr.type3,
  sig.digs=sigdigs,
  title=base::sprintf('\n%s: combined (type3) output emissions=', this_fn)
)

# combine_EDGAR_and_EPIC_emissions.r: combined (type3) output emissions=
#         cells=137241
#         obs=137241
#         min=0
#         q1=0
#         med=0.000157
#         mean=0.00189
#         q3=0.00312
#         max=0.0861
#         sum=259

# type3 == EDGAR! from above:

# combine_EDGAR_and_EPIC_emissions.r: EDGAR input emissions=
#         cells=137241
#         obs=137241
#         min=0
#         q1=0
#         med=0.000157
#         mean=0.00189
#         q3=0.00312
#         max=0.0861
#         sum=259

### type4: use values from EPIC only where EPIC > EDGAR

output.both.dv.arr.type4 <- input.epic.dv.arr # can't use EDGAR: dims don't match
for (i.tstep in 1:output.both.dim.tsteps.n) {
  for (i.lay in 1:output.both.dim.layers.n) {
    input.epic.grid <- input.epic.dv.arr[,,i.lay,i.tstep]
    output.grid <- input.edgar.arr

    # we want to copy if either of
    # * is.na(EDGAR) & !is.na(EPIC)
    # * !is.na(EDGAR) & !is.na(EPIC) & (EPIC > EDGAR)
    # (EDGAR-4.2 is never NA, but I'll test anyway, for safety)
    copy.criterion.type4 <-
      (is.na(input.edgar.arr) & !is.na(input.epic.grid)) |
      (!is.na(input.edgar.arr) & !is.na(input.epic.grid) & (input.epic.grid > input.edgar.arr))
    output.grid[copy.criterion.type4] <- input.epic.grid[copy.criterion.type4]
    output.both.dv.arr.type4[,,i.lay,i.tstep] <- output.grid
  }
}

# start debugging-----------------------------------------------------
base::cat(base::sprintf('\n%s: dim(output.both.dv.arr.type4)==', this_fn))
print(dim(output.both.dv.arr.type4))
stats.to.stdout(
  data=output.both.dv.arr.type4,
  sig.digs=sigdigs,
  title=base::sprintf('\n%s: combined (type4) output emissions=', this_fn)
)

# combine_EDGAR_and_EPIC_emissions.r: combined (type4) output emissions=
#         cells=137241
#         obs=137241
#         min=0
#         q1=0
#         med=0.00016
#         mean=0.00199
#         q3=0.0032
#         max=0.0861
#         sum=273

# type4 > type3 == EDGAR: from above:

# combine_EDGAR_and_EPIC_emissions.r: EDGAR input emissions=
#         cells=137241
#         obs=137241
#         min=0
#         q1=0
#         med=0.000157
#         mean=0.00189
#         q3=0.00312
#         max=0.0861
#         sum=259

#   end debugging-----------------------------------------------------

### go with type4 for now:
output.both.dv.arr <- output.both.dv.arr.type4

# ----------------------------------------------------------------------
# copy/mod IOAPI/fake datavar=TFLAG
# ----------------------------------------------------------------------

input.epic.TFLAG <- input.epic.fh$var[[input_epic_TFLAG_name]] # note double brackets!
# input.epic.TFLAG.type <- input.epic.TFLAG$prec
# arrggghhhh
input.epic.TFLAG.type <- get.ncdf4.prec(input.epic.TFLAG)
input.epic.TFLAG.mv <- input.epic.TFLAG$missval
# get these from driver? no
input.epic.TFLAG.units <- input.epic.TFLAG$units
input.epic.TFLAG.long_name <- input.epic.TFLAG$longname
# input.epic.TFLAG.var_desc <- input.epic.TFLAG$var_desc
input.epic.TFLAG.var_desc <- ncdf4::ncatt_get(input.epic.fh, input_epic_TFLAG_name)$var_desc
## and its dimensions? no, do that @ writetime, since dimensions are really about files

output.both.TFLAG.var_desc <- input.epic.TFLAG.var_desc

# ----------------------------------------------------------------------
# define and write output
# ----------------------------------------------------------------------

### Note on creating output, adapted from ncdf4.pdf (numbering added):
# > If you want to WRITE data to a new netCDF file, the procedure is to
# 1. define output dimensions: call `ncdim_def(...)`
# 2. define output datavar(s): call `ncvar_def(...)` to define each variable
# 3. create output file: call `nc_create(...)`
# 4. write data to output datavar(s): call `ncvar_put(...)` to write to each datavar
# 5. close output file: call `nc_close(...)`
# 6. (extra credit)remove output filehandle from the workspace/environment: call `rm(..)`

### 1. define output dimensions
cat(sprintf('\n%s: 1. define output dimensions\n', this_fn)) # debugging

## instead, copy input dimensions? I can't see how to :-(

## 1.1. get values for output dimensions
cat(sprintf('\n%s: 1.1. get values for output dimensions\n', this_fn)) # debugging

# For non-grid dimensions, can use ncdf4

input.epic.dim.tsteps.vals <- input.epic.dim.tsteps$vals
# input.epic.dim.tsteps.units <- input.epic.dim.tsteps$units
# ncdf4 (formatting added):
# > [if] create_dimvar was specified to be FALSE, [then]
# > the unit string MUST be empty ('') and
# > the dimension values MUST be simple integers from 1 to the length of the dimension (e.g., 1:len)
input.epic.dim.tsteps.units <- ''

input.epic.dim.datetimes.vals <- input.epic.dim.datetimes$vals
input.epic.dim.datetimes.units <- ''

input.epic.dim.layers.vals <- input.epic.dim.layers$vals
input.epic.dim.layers.units <- ''

input.epic.dim.vars.vals <- input.epic.dim.vars$vals
input.epic.dim.vars.units <- ''

# For grid dimensions, I could use ncdf4, but, to overlay plots with "the usual" map,
# we need dimensions uncentered and with units=m
library(M3)
input.epic.dim.x.vals <- M3::get.coord.for.dimension(
  file=input_epic_fp, dimension="col", position="upper", units="m")$coords
input.epic.dim.y.vals <- M3::get.coord.for.dimension(
  file=input_epic_fp, dimension="row", position="upper", units="m")$coords

## 1.2. Create output dimension definitions
cat(sprintf('\n%s: 1.2. Create output dimension definitions\n', this_fn)) # debugging

output.both.dim.x <- ncdim_def(
  name=output_both_dim_x_name,
  units=output_both_dim_x_units,
  vals=input.epic.dim.x.vals,
  unlim=FALSE,
  create_dimvar=TRUE,
# IOAPI does not create coordvars, but we will, for the grid (unless VERDI chokes)
#   create_dimvar=FALSE,
  calendar=NA,
  longname=output_both_dim_x_long_name)

output.both.dim.y <- ncdim_def(
  name=output_both_dim_y_name,
  units=output_both_dim_y_units,
  vals=input.epic.dim.y.vals,
  unlim=FALSE,
  create_dimvar=TRUE,
# IOAPI does not create coordvars, but we will, for the grid (unless VERDI chokes)
#   create_dimvar=FALSE,
  calendar=NA,
  longname=output_both_dim_y_long_name)

# output=input
output.both.dim.datetimes <- ncdim_def(
  name=input_epic_dim_datetimes_name,
  units=input.epic.dim.datetimes.units,
  vals=input.epic.dim.datetimes.vals,
  unlim=FALSE,
  create_dimvar=FALSE,
  calendar=NA) # long_name omitted

# output(LAY) == input(LAY)
output.both.dim.layers <- ncdim_def(
  name=input_epic_dim_layers_name,
  units=input.epic.dim.layers.units,
  vals=input.epic.dim.layers.vals,
#  vals=c(1),
#  vals=1,
# dunno how this differs from the above, but below makes ncdf4 happy, and above did not
#  vals=input.epic.dim.layers.vals[1],
  unlim=FALSE,
  create_dimvar=FALSE,
  calendar=NA) # long_name omitted

# output=input
output.both.dim.tsteps <- ncdim_def(
  name=input_epic_dim_tsteps_name,
  units=input.epic.dim.tsteps.units,
  vals=input.epic.dim.tsteps.vals,
  unlim=FALSE,
  create_dimvar=FALSE,
  calendar=NA) # long_name omitted

# output=input
output.both.dim.vars <- ncdim_def(
  name=input_epic_dim_vars_name,
  units=input.epic.dim.vars.units,
  vals=input.epic.dim.vars.vals,
  unlim=FALSE,
  create_dimvar=FALSE,
  calendar=NA) # long_name omitted

### 2. define output datavar(s)
cat(sprintf('\n%s: 2. define output datavar(s)\n', this_fn)) # debugging

## Try to make TFLAG the first "datavar"? Not sure it matters, but in the IOAPI files I've `ncdump`ed, it always is.

output.both.TFLAG <- ncdf4::ncvar_def(
  name=output_both_TFLAG_name,
  units=input.epic.TFLAG.units,
# (TSTEP, VAR, DATE-TIME) is how it looks in `ncdump`, but ncdf4 wants ...
  dim=list(output.both.dim.datetimes, output.both.dim.vars, output.both.dim.tsteps),
  prec=input.epic.TFLAG.type,
# ncdf4::ncatt_put vomits on being asked to create datavar of type=integer with a missing_value:
# > [1] "ncatt_put_inner: prec to create: integer"
# > Error in R_nc4_put_att_logical: asked to put a NA value, but the variable's type is not a double or float, which are the only two types that have a defined NaN value
# > [1] "Error in ncatt_put, while writing attribute _FillValue with value NA"
# > Error in ncatt_put_inner(ncid2use, newvar$id, "_FillValue", v$missval,  : 
# >   Error return from C call R_nc4_put_att_logical for attribute _FillValue
# > Calls: <Anonymous> -> ncvar_add -> ncatt_put_inner
# so don't try!
#   missval=input.epic.TFLAG.mv,
  longname=input.epic.TFLAG.long_name,
  shuffle=FALSE,
  compression=NA,
  chunksizes=NA)

output.both.dv <- ncdf4::ncvar_def(
  name=output_both_dv_name,
  units=output_both_dv_units,
# (TSTEP, LAY, ROW, COL) is how it looks in `ncdump`, but ncdf4 wants ...
  dim=list(output.both.dim.x, output.both.dim.y, output.both.dim.layers, output.both.dim.tsteps),
  missval=input.epic.dv.mv,
  longname=output_both_dv_long_name,
  prec=input.epic.dv.type,
  shuffle=FALSE,
  compression=NA,
  chunksizes=NA)

input.epic.dim.x.vals <- input.epic.dim.x$vals
input.epic.dim.y.vals <- input.epic.dim.y$vals
# ## To overlay plots with "the usual" map, need dimension centered and units=m
# library(M3)
# # TODO:? Do we need to do this again? was already done for EPIC emissions.
# input.epic.dim.x.vals <- M3::get.coord.for.dimension(
#   file=input_epic_fp, dimension="col", position="upper", units="m")$coords
# input.epic.dim.y.vals <- M3::get.coord.for.dimension(
#   file=input_epic_fp, dimension="row", position="upper", units="m")$coords

output.both.dim.x <- ncdim_def(
  name=output_both_dim_x_name,
#   units='',
  units=output_both_dim_x_units,
  vals=input.epic.dim.x.vals,
  unlim=FALSE,
  create_dimvar=TRUE,
#   create_dimvar=FALSE,
  calendar=NA,
  longname=output_both_dim_x_long_name)

output.both.dim.y <- ncdim_def(
  name=output_both_dim_y_name,
#   units='',
  units=output_both_dim_y_units,
  vals=input.epic.dim.y.vals,
  unlim=FALSE,
  create_dimvar=TRUE,
#   create_dimvar=FALSE,
  calendar=NA,
  longname=output_both_dim_y_long_name)

### 2. define output datavar(s)
cat(sprintf('\n%s: 2. define output datavar(s)\n', this_fn)) # debugging

## Try to make TFLAG the first "datavar"? Not sure it matters, but in the IOAPI files I've `ncdump`ed, it always is.

output.both.TFLAG <- ncdf4::ncvar_def(
  name=output_both_TFLAG_name,
  units=input.epic.TFLAG.units,
# (TSTEP, VAR, DATE-TIME) is how it looks in `ncdump`, but ncdf4 wants ...
  dim=list(output.both.dim.datetimes, output.both.dim.vars, output.both.dim.tsteps),
  prec=input.epic.TFLAG.type,
# ncdf4::ncatt_put vomits on being asked to create datavar of type=integer with a missing_value:
# > [1] "ncatt_put_inner: prec to create: integer"
# > Error in R_nc4_put_att_logical: asked to put a NA value, but the variable's type is not a double or float, which are the only two types that have a defined NaN value
# > [1] "Error in ncatt_put, while writing attribute _FillValue with value NA"
# > Error in ncatt_put_inner(ncid2use, newvar$id, "_FillValue", v$missval,  : 
# >   Error return from C call R_nc4_put_att_logical for attribute _FillValue
# > Calls: <Anonymous> -> ncvar_add -> ncatt_put_inner
# so don't try!
#   missval=input.epic.TFLAG.mv,
  longname=input.epic.TFLAG.long_name,
  shuffle=FALSE,
  compression=NA,
  chunksizes=NA)

output.both.dv <- ncdf4::ncvar_def(
  name=output_both_dv_name,
  units=output_both_dv_units,
#  dim=list(output.both.dim.y, output.both.dim.x),
# (TSTEP, LAY, ROW, COL) is how it looks in `ncdump`, but ncdf4 wants the reverse
  dim=list(output.both.dim.x, output.both.dim.y, output.both.dim.layers, output.both.dim.tsteps),
  missval=input.epic.dv.mv,
  longname=output_both_dv_long_name,
  prec=input.epic.dv.type,
  shuffle=FALSE,
  compression=NA,
  chunksizes=NA)

### 3. create output file
cat(sprintf('\n%s: 3. create output file\n', this_fn)) # debugging

# # start debugging-----------------------------------------------------
# base::cat(base::sprintf('\n%s: about to attempt\n', this_fn))
# base::cat(base::sprintf('output.both.fh <- ncdf4::nc_create(\n'))
# base::cat(base::sprintf('  filename=%s\n', output_both_fp))
# base::cat(base::sprintf('  vars=output.both.dv == %s\n', output_both_dv_name))
# #   end debugging-----------------------------------------------------

output.both.fh <- ncdf4::nc_create(
  filename=output_both_fp,
#   vars=output.both.dv,               # can also be vector or list? no, just a list
# Try to make TFLAG the first "datavar"? Not sure it matters, but in the IOAPI files I've `ncdump`ed, it always is.
  vars=list(output.both.TFLAG, output.both.dv),
  force_v4=FALSE,
# # to debug, toggle verbose
#   verbose=TRUE)
  verbose=FALSE)

## 3.1. create output file/global attributes
cat(sprintf('\n%s: 3.1. create output file/global attributes\n', this_fn)) # debugging

## copy all attributes from EPIC input, except :FILEDESC
## (see extended discussion in comments to compute_EPIC_emissions.r)

input.epic.attrs <- ncdf4::ncatt_get(nc=input.epic.fh, 
  varid=0, attname=NA) # this is what gets us a list of all file/global attributes
input.epic.attrs.n <- length(input.epic.attrs)
if (input.epic.attrs.n > 0) {
  input.epic.attrs.names <- names(input.epic.attrs)
  for (i.attrs in 1:input.epic.attrs.n) {
    attr.name <- input.epic.attrs.names[i.attrs]
    if        (attr.name == 'FILEDESC') {
      attr.val <- output_both_attr_filedesc
      attr.type <- output_both_attr_filedesc_type
    } else {
      attr.val <- input.epic.attrs[[i.attrs]] # copy from input
      attr.type <- typeof(attr.val)
    }
    ncdf4::ncatt_put(
      nc=output.both.fh,
      varid=0,            # for file/global attributes
      attname=attr.name,
      attval=attr.val,
      prec=attr.type,
      definemode=FALSE,
# # to debug, toggle verbose
#       verbose=TRUE)
      verbose=FALSE)
  } # end for (i.attrs in 1:input.epic.attrs.n)
} # end if (input.epic.attrs.n > 0)

### 4. write data to output datavar
cat(sprintf('\n%s: 4. write data to output datavar\n', this_fn)) # debugging

## 4.1. write datavar data
cat(sprintf('\n%s: 4.1. write datavar data\n', this_fn)) # debugging

# # start debugging-----------------------------------------------------
# base::cat(base::sprintf('\n%s: about to attempt\n', this_fn))
# base::cat(base::sprintf('ncdf4::ncvar_put(\n'))
# base::cat(base::sprintf('  nc=output.both.fh == %s\n', output_both_fp))
# base::cat(base::sprintf('  varid=%s\n', output_both_dv_name))
# base::cat(base::sprintf('  vals=output.both.dv.arr == '))
# print(dim(output.both.dv.arr))
# #   end debugging-----------------------------------------------------

ncdf4::ncvar_put(
  nc=output.both.fh,
#   varid=output_both_TFLAG_name,
  varid=output.both.TFLAG,
  vals=output.both.TFLAG.arr,
  start=output.both.TFLAG.write.start,
  count=output.both.TFLAG.write.count,
# # to debug, toggle verbose
#   verbose=TRUE)
  verbose=FALSE)

ncdf4::ncvar_put(
  nc=output.both.fh,
#  varid=output.both.dv,
  varid=output_both_dv_name,
  vals=output.both.dv.arr,
  start=output.both.dv.write.start,
  count=output.both.dv.write.count,
# # to debug, toggle verbose
#   verbose=TRUE)
  verbose=FALSE)

## 4.2. write datavar metadata (i.e., additional attributes)
cat(sprintf('\n%s: 4.2. write datavar metadata (i.e., additional attributes)\n', this_fn)) # debugging

ncdf4::ncatt_put(
  nc=output.both.fh,
  varid=output.both.TFLAG,
  attname='var_desc',
  attval=output.both.TFLAG.var_desc,
  prec='text',
  definemode=FALSE,
# # to debug, toggle verbose
#   verbose=TRUE)
  verbose=FALSE)

ncdf4::ncatt_put(
  nc=output.both.fh,
  varid=output.both.dv,
  attname='var_desc',
  attval=output_both_dv_var_desc,
  prec='text',
  definemode=FALSE,
# # to debug, toggle verbose
#   verbose=TRUE)
  verbose=FALSE)

### 5. close output file

ncdf4::nc_close(output.both.fh)

### 6. remove output filehandle from the workspace/environment

rm(output.both.fh)

# ----------------------------------------------------------------------
# close inputs
# ----------------------------------------------------------------------

## close filehandle(s)
ncdf4::nc_close(input.edgar.fh)
ncdf4::nc_close(input.epic.fh)

## remove the filehandle(s) (not files!) from the workspace/environment
rm(input.edgar.fh)
rm(input.epic.fh)

#----------------------------------------------------------------------
# verify VERDI compatibility? output=combined emissions
#----------------------------------------------------------------------

  ### TODO: integrate VERDI into bash_utilities.sh
  ### plot our datavar of interest with VERDI
  output_both_dv_name_VERDI <- sprintf('%s[1]', output_both_dv_name)
  ## VERDI really wants an FQP
  output_both_fp_VERDI <- system(sprintf('readlink -f %s', output_both_fp), intern=TRUE)
  output_both_dv_VERDI_cmd <-
    sprintf('verdi -f %s -s %s -gtype tile &',
      output_both_fp_VERDI, output_both_dv_name_VERDI)
  cat(sprintf("\n%s: debugging IOAPI: about to run '%s'\n", this_fn, output_both_dv_VERDI_cmd))
#   cat(sprintf("\n%s: not debugging IOAPI: skipping '%s'\n", this_fn, output_both_dv_VERDI_cmd))
  system(output_both_dv_VERDI_cmd)

# ----------------------------------------------------------------------
# visualize output emissions
# ----------------------------------------------------------------------

output.both.raster <- raster::raster(output_both_fp, varname=output_both_dv_name)
# something whacks the CRS, so
output.both.raster@crs <- aqme.crs

# # start debugging-----------------------------------------------------
# cat('\noutput.both.raster==\n')
# output.both.raster
# # class       : RasterLayer 
# # dimensions  : 299, 459, 137241  (nrow, ncol, ncell)
# # resolution  : 12000, 12000  (x, y)
# # extent      : -2550000, 2958000, -1722000, 1866000  (xmin, xmax, ymin, ymax)
# # coord. ref. : +proj=lcc +lat_1=33 +lat_2=45 +lat_0=40 +lon_0=-97 +a=6370000 +b=6370000 
# # data source : .../5yravg_20111219_pure_emis_both.nc 
# # names       : N2O 
# # zvar        : N2O 

# cat('output.both.raster@extent==\n')
# output.both.raster@extent
# # class       : Extent 
# # xmin        : -2550000 
# # xmax        : 2958000 
# # ymin        : -1722000 
# # ymax        : 1866000 
# #   end debugging-----------------------------------------------------

# TODO: if using output as args below, `project.NorAm.boundaries.for.CMAQ` fails with
# > Grid type 0 cannot be handled by this function.
# unless one adds IOAPI global attrs to output

### get projected North American map
NorAm.shp <- project.NorAm.boundaries.for.CMAQ(
  units='m',
  extents.fp=output_both_fp,
  extents=output.both.raster@extent,
  LCC.parallels=c(33,45),
  CRS=aqme.crs)

# # start debugging
# class(NorAm.shp)
# bbox(NorAm.shp)
# # compare bbox to (above)
# # > input.epic.extents
# # class       : Extent 
# # xmin        : -2556000 
# # xmax        : 2952000 
# # ymin        : -1728000 
# # ymax        : 1860000 
# #   end debugging

# Why does '=' fail and '<-' succeed in ONE PLACE in the arg list?
visualize.layer(
  nc.fp=output_both_fp,
  datavar.name=output_both_dv_name,
  sigdigs=sigdigs,
  layer=output.both.raster,
  map.list <- list(NorAm.shp),
  pdf.fp=output_both_pdf_fp,
  pdf.height=output_both_pdf_height,
  pdf.width=output_both_pdf_width
)
