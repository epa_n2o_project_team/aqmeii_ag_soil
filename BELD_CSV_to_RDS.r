#!/usr/bin/env Rscript
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

# R code to drive the process of
# * reading a BELD CSV file
# * writing its contents as RDS (*the* R serialization format, directly consumable by R code)

# ----------------------------------------------------------------------
# constants
# ----------------------------------------------------------------------

# all the following env vars must be set and exported in driver script
# TODO: pass as args!

### miscellany

## kludge for my clumsy namespacing
my_this_fn <- Sys.getenv('BELD_CONVERT_FN')
this_fn <- my_this_fn

work_dir <- Sys.getenv('WORK_DIR')

### helper

beld_read_fp <- Sys.getenv('BELD_READ_FP')

### input

input_fp <- Sys.getenv('BELD_CSV_FP')
## TODO: read me from netCDF file (e.g., 5yravg.test.nc)
#                 COL, ROW, LAY
datavar_dims <- c(459, 299, 42)
## TODO: move to driver
# number of header lines (to ignore)
n_header_lines <- 2
# number of used fields in each CSV line == the number of EPIC layers, plus n_additional_fields
datavar_layers_n <- datavar_dims[3]
n_additional_fields <- 1
n_data_fields <- datavar_layers_n + n_additional_fields

## types of fields in each CSV line (ignoring string @ end)
## '0' signifies type=numeric
## fail! list of length=2
## list_line_types <- list(rep.int(0, times=n_data_fields), NULL)
## instead do with emacs (TODO: do with R!)
list_line_types <- list(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL)

### output

output_fp <- Sys.getenv('BELD_RDS_FP')

# ----------------------------------------------------------------------
# payload
# ----------------------------------------------------------------------

source(beld_read_fp) # supplies `beld.read` et al
beld.array <- beld.read(input_fp, datavar_dims, n_header_lines, n_data_fields, list_line_types)
base::saveRDS(object=beld.array, file=output_fp)
