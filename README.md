*(part of the [AQMEII-NA_N2O][AQMEII-NA_N2O wiki home] family of projects)*

**table of contents**

[TOC]

# open-source notice

Copyright 2013, 2014 Tom Roche <Tom_Roche@pobox.com>

This project's content is free software: you can redistribute it and/or modify it provided that you do so as follows:

* under the terms of the [GNU Affero General Public License][GNU Affero General Public License HTML @ GNU] as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
* preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

This project's content is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the [GNU Affero General Public License][GNU Affero General Public License local text] for more details.

![distributed under the GNU Affero General Public License](../../downloads/Affero_badge__agplv3-155x51.png)

[GNU Affero General Public License local text]: ./COPYING
[GNU Affero General Public License HTML @ GNU]: https://www.gnu.org/licenses/agpl.html

# definitions

## emittivity

In [EPIC][EPIC overview], the `emittivity` per gridcell per timestep per crop is the amount of N2O the crop would produce over the given timestep in the given gridcell if the full area of the gridcell was devoted to that crop over the full timestep.

# description

Uses [bash][bash @ wikipedia] to drive [NCL][NCL @ wikipedia] and [R][R @ wikipedia] code to

1. "reunit" an [EDGAR-4.2][EDGAR 4.2 overview] N2O emission inventory (in [netCDF][] format) for agricultural soils from (per-gridcell) flux rate (`kg/m^2/s`) to molar-mass rate (`mol/s`, which [CMAQ][CMAQ @ CMAS] wants anyway) prior to regridding.
    1. [... and visualize it][visualization of global EDGAR].
1. 2D-regrid the EDGAR inventory from global/unprojected to a projected subdomain ([AQMEII-NA][AQMEII-NA spatial domain]).
    1. [... and visualize it][visualization of AQMEII-NA EDGAR].
1. Process [BELD][BELD overview] crop-coverage data needed to process [EPIC][EPIC overview] data.
    1. Read a [BELD CSV file][epic_site_crops_0529_USA_2Ellen.csv.gz] with structure `{row=GRIDID, col=crop}`, where
        * each `crop` maps one-to-one to a layer in the EPIC netCDF
        * each `GRIDID` maps one-to-one to an individual gridcell in the EPIC netCDF
        * each `(GRIDID,crop)` tuple is the percentage (0 .. 100) of the gridcell area devoted to that crop (i.e., the crop's coverage of that gridcell).
    1. Write output (with format=, e.g., [RDS][R internals::Serialization Formats]) with structure `{col=AQMEII gridcell col, row=AQMEII gridcell row, layer=EPIC crop layer}` such that
        * the index of each `(col,row)` tuple matches the corresponding gridcell in the EPIC data.
        * the index of each `layer` matches the corresponding crop layer in the EPIC data.
        * each `(col,row,layer)` tuple is the coverage of that gridcell=`(col,row)` by that crop=`layer` as a proportion (0 .. 1).
1. Obtain EPIC N2O emittivities. 
    1. Read per-crop N2O [emittivity](#markdown-header-emittivity) from the raw EPIC netCDF. Unfortunately the latter is too large to provide in this project, since it contains many species other than N2O, so, first, we remove the data variables for the other species.
    1. "Demonotonicize" the N2O emittivities, if necessary. Consider the 2D spatial grid to be "flattened" into a 1D vector, and to be "desparsified" such that all missing values are removed. The current dataset has the unfortunate problem that, for each crop/layer, each estimation in the gridcell/vector was summed with all its predecessors, instead of being recorded separately. E.g., if the true estimations were `a[i]==u`, `a[j]==v`, and `a[k]==w` (for vector indices `i < j < k`), the data values given are `a[i]==u`, `a[j]==u+v`, and `a[k]==u+v+w`.
    1. "Reunit" the emittivities from a mass flux (explicitly `kg/ha`, implicitly `kg/ha/yr`) to a molar-mass rate (`mol/s`) consumable by [CMAQ][CMAQ @ CMAS].
1. *(completed)* Obtain EPIC N2O emissions:
    1. Obtain the `crop N2O-emittivity vector` from each gridcell's EPIC data: a vector of the N2O emittivities of all the crops over that gridcell during that timestep.
    1. Obtain the `crop coverage vector` from each gridcell's BELD data: a vector of the coverages of all the crops over that gridcell during that timestep.
    1. Write to output, for each gridcell, the scalar of EPIC N2O emissions for that gridcell (i.e., the scalar product of the crop N2O-emittivity vector and the crop coverage vector). I.e., for output dimensions=`(TSTEP, LAY, ROW, COL)`,
        1. ∀ `TSTEP`=0 (annual value)
        1. ∀ `LAY`=0 (corresponding to a surface/vertical layer--semantics of 'layer' revert back to "the usual")
        1. `(ROW, COL)` is the AQMEII-NA grid
        1. `value(TSTEP, LAY, ROW, COL)` is EPIC's estimate of the total emission of N2O (in `mol/s`) from soils planted in all crops modeled by EPIC in that gridcell over that timestep.
    1. [Visualize the EPIC emissions][visualization of AQMEII-NA EPIC].
1. Combine EDGAR and EPIC emissions over AQMEII-NA.
    1. *(algorithm may change)* For each gridcell in AQMEII-NA: if the EPIC emissions exceed the EDGAR emissions, use the EPIC value.
    1. [Visualize the combined AQMEII-NA emissions][visualization of AQMEII-NA EDGAR+EPIC emissions].
1. Create [CMAQ][CMAQ @ CMAS]-style emissions files (e.g., [this][hourly emissions for 1 Jan 2008], when `gunzip`ed) containing emissions for every hour in 2008 (the model year).
1. Check extent to which mass is conserved from global input to regional output.

Currently does not provide a clean or general-purpose (much less packaged) solution! but merely shows how to do these tasks using

* [bash][bash @ wikipedia] (tested with version=3.2.25)
* [NCL][NCL @ wikipedia] (tested with version=6.1.2)
* [R][R @ wikipedia] (tested with version=3.0.0) and packages including
    * [ncdf4][]
    * [raster][]
    * [rasterVis][]

Visualizations of datasets are provided

* statistically (summary statistics of datasets are generated and presented in console)
* by plots (saved as PDF, displayed by launching the user-configured PDF viewer)
* by [VERDI][] (which also serves as a check on the [IOAPI][]-compatibility of the generated netCDF)

[AQMEII-NA_N2O wiki home]: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/Home
[bash @ wikipedia]: http://en.wikipedia.org/wiki/Bash_%28Unix_shell%29
[R @ wikipedia]: http://en.wikipedia.org/wiki/R_%28programming_language%29
[NCL @ wikipedia]: http://en.wikipedia.org/wiki/NCAR_Command_Language
[netCDF]: http://en.wikipedia.org/wiki/NetCDF#Format_description
[EDGAR 4.2 overview]: http://edgar.jrc.ec.europa.eu/overview.php?v=42
[AQMEII-NA spatial domain]: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/AQMEII-NA_spatial_domain
[BELD overview]: http://dx.doi.org/10.2307/2269406
[EPIC overview]: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/EPIC
[epic_site_crops_0529_USA_2Ellen.csv.gz]: ../../downloads/epic_site_crops_0529_USA_2Ellen.csv.gz
[R internals::Serialization Formats]: http://cran.r-project.org/doc/manuals/R-ints.html#Serialization-Formats
[visualization of global EDGAR]: ../../downloads/v42_N2O_2008_IPCC_4C_4D.0.1x0.1_reunit.pdf
[visualization of AQMEII-NA EDGAR]: ../../downloads/v42_N2O_2008_IPCC_4C_4D.0.1x0.1_reunit_regrid.pdf
[visualization of AQMEII-NA EPIC]: ../../downloads/5yravg_20111219_pure_emis.pdf
[visualization of AQMEII-NA EDGAR+EPIC emissions]: ../../downloads/5yravg_20111219_pure_emis_both.pdf
[CMAQ @ CMAS]: http://www.cmaq-model.org/
[hourly emissions for 1 Jan 2008]: ../../downloads/emis_mole_N2O_2008_12US1_cmaq_cb05_soa_2008ab_08c.ncf.gz
[ncdf4]: http://cran.r-project.org/web/packages/ncdf4/
[raster]: http://cran.r-project.org/web/packages/raster/
[rasterVis]: http://cran.r-project.org/web/packages/rasterVis/
[VERDI]: http://www.verdi-tool.org/
[IOAPI]: http://www.baronams.com/products/ioapi/

# operation

To run this code,

1. `git clone` this repo. (For more details, see *[Cloning a git repository][]*.)
1. `cd` to its working directory (where you cloned it to).
1. Setup your applications and paths.
    1. [Clone regrid_utils][section=cloning regrid_utils] to a subdirectory of the working directory, copy [regrid_utils/bash_utilities.sh][bash_utilities.sh] up to the working directory, and open `./bash_utilities.sh` in an editor! You will probably need to edit its functions `setup_paths` and `setup_apps` to make it work on your platform. Notably you will want to point it to your PDF viewer and NCL and R executables.
    1. Open the [driver (bash) script][AQMEII_driver.sh] `./AQMEII_driver.sh` in an editor and ensure paths to your inputs are (or will be) correct.
1. Download inputs that cannot currently be automated by this project, saving them to the paths you denoted in [the driver][AQMEII_driver.sh]. Most required data can be downloaded by the driver (provided the URIs pointing to the data remain unchanged), but some cannot as of the time of this writing:
    1. CONUS EPIC [emittivities][definition of emittivity]. These unfortunately are currently much too large to save on bitbucket; we hope to find a home for them online before too long.
1. Run the driver:
    `$ ./AQMEII_driver.sh`
    This will download input, then run ...
    1. [reunit_vis_EDGAR_global.r][], which will prepare the [raw EDGAR input][global ag soil N2O from EDGAR in flux-rate units] (unfortunately not currently directly downloadable from EDGAR) for regridding, and [plot it][visualization of global EDGAR].
    1. [regrid_vis.r][], which will 2D-regrid the [reunit-ed global EDGAR][global ag soil N2O from EDGAR in mole-rate units] (from the previous step) to [AQMEII-NA][AQMEII-NA spatial domain], and [plot it][visualization of AQMEII-NA EDGAR].
    1. [BELD_CSV_to_RDS.r][], which will read [AQMEII-NA BELD coverage data][] and convert it to RDS for later use.
    1. [distill_EPIC_emittivities.ncl][], which will read the previously-saved CONUS EPIC emittivities and create more consumable [output][distilled CONUS EPIC N2O emittivities] by
        * removing species and processes other than N2O production
        * demonotonicizing the N2O emittivities, if necessary (see above)
        * reunit-ing the emittivities (see above)
    1. [compute_EPIC_emissions.r][], which calculates the scalar product of the BELD crop coverages and the EPIC emittivities, creating and [visualizing][visualization of AQMEII-NA EPIC] [CONUS-only, EPIC-only N2O emissions][CONUS EPIC N2O emissions]. **Note:** there appears to be a spurious maximum in the current version of EPIC, which is removed: see comments in the code, as well as visualization statistical output, for details.
    1. [combine_EDGAR_and_EPIC_emissions.r][], which (predicably) combines the [EPIC emissions from the previous step][visualization of AQMEII-NA EPIC] with the [reunit-ed, regridded EDGAR][visualization of AQMEII-NA EDGAR] from step 2 into [one dataset][AQMEII-NA combined EDGAR and EPIC emissions], and [visualizes it][visualization of AQMEII-NA EDGAR+EPIC emissions].
    1. [make_hourlies.ncl][], which outputs the [combined emissions in CMAQ format][AQMEII-NA combined EDGAR and EPIC emissions in CMAQ format]: i.e., 25 hourly (and in this case identical, since we have no temporality) grids.
    1. [check_conservation.ncl][] to investigate conservation of mass from inputs to outputs. Current output includes

                EDGAR global emissions (mol/s) from ./v42_N2O_2008_IPCC_4C_4D.0.1x0.1_reunit.nc
                |cells|=6.48e+06
                |obs|  =6.48e+06
                min    =0
                q1     =0
                med    =0
                mean   =5.177e-04
                q3     =0
                max    =1.003e+00
                sum    =3.352e+03

                EDGAR emissions (mol/s) over AQMEII-NA from ./v42_N2O_2008_IPCC_4C_4D.0.1x0.1_reunit_regrid.nc
                |cells|=137241
                |obs|  =137241
                min    =0
                q1     =0
                med    =1.575e-04
                mean   =1.890e-03
                q3     =3.121e-03
                max    =8.609e-02
                sum    =2.593e+02

                EPIC emissions (mol/s) over CONUS from ./5yravg_20111219_pure_emis.nc
                |cells|=137241
                |obs|  =40763
                min    =0
                q1     =1.021e-05
                med    =1.714e-04
                mean   =1.373e-03
                q3     =1.112e-03
                max    =7.571e-02
                sum    =5.596e+01

                EDGAR+EPIC emissions (mol/s) over AQMEII-NA from ./emis_mole_N2O_2008_12US1_cmaq_cb05_soa_2008ab_08c.nc
                |cells|=137241
                |obs|  =137241
                min    =0
                q1     =0
                med    =1.600e-04
                mean   =1.986e-03
                q3     =3.198e-03
                max    =8.609e-02
                sum    =2.725e+02

                Comparison of EDGAR emissions
                (units=molN2O/s)

                    EDGAR     global      EDGAR  AQMEII-NA  AQMEII-NA/
                   global        NAs  AQMEII-NA        NAs     global
                 3.35e+03          0   2.59e+02          0   7.74e-02

                EPIC vs EDGAR emissions over AQMEII-NA
                (units=molN2O/s)

                     EPIC       EPIC      EDGAR      EDGAR      EPIC/
                  (CONUS)        NAs  AQMEII-NA        NAs      EDGAR
                 5.60e+01      96478   2.59e+02          0   2.16e-01

                EDGAR vs EPIC+EDGAR emissions (both over AQMEII-NA)
                (units=molN2O/s)

                    EDGAR      EDGAR   combined   combined     EDGAR/
                AQMEII-NA        NAs  AQMEII-NA        NAs   combined
                 2.59e+02          0   2.72e+02          0   9.52e-01

                EPIC+EDGAR AQMEII-NA vs EDGAR global emissions
                (units=molN2O/s)

                 combined   combined      EDGAR      EDGAR  combined/
                AQMEII-NA        NAs     global        NAs      EDGAR
                 2.72e+02          0   3.35e+03          0   8.13e-02

[Cloning a git repository]: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/Cloning_a_git_repository
[section=cloning regrid_utils]: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/Cloning_a_git_repository#!example-cloning-regrid_utils
[bash_utilities.sh]: https://bitbucket.org/epa_n2o_project_team/regrid_utils/src/HEAD/bash_utilities.sh?at=master
[uber_driver.sh]: ../../src/HEAD/uber_driver.sh?at=master
[AQMEII_driver.sh]: ../../src/HEAD/AQMEII_driver.sh?at=master
[definition of emittivity]: #markdown-header-emittivity
[reunit_vis_EDGAR_global.r]: ../../src/HEAD/reunit_vis_EDGAR_global.r?at=master
[global ag soil N2O from EDGAR in flux-rate units]: ../../downloads/v42_N2O_2008_IPCC_4C_4D.0.1x0.1.zip
[regrid_vis.r]: ../../src/HEAD/regrid_vis.r?at=master
[global ag soil N2O from EDGAR in mole-rate units]: ../../downloads/v42_N2O_2008_IPCC_4C_4D.0.1x0.1_reunit.nc.gz
[BELD_CSV_to_RDS.r]: ../../src/HEAD/BELD_CSV_to_RDS.r?at=master
[AQMEII-NA BELD coverage data]: ../../downloads/epic_site_crops_0529_USA_2Ellen.csv.gz
[distill_EPIC_emittivities.ncl]: ../../src/HEAD/distill_EPIC_emittivities.ncl?at=master
[distilled CONUS EPIC N2O emittivities]: ../../downloads/5yravg_20111219_pure.nc.gz
[compute_EPIC_emissions.r]: ../../src/HEAD/compute_EPIC_emissions.r?at=master
[CONUS EPIC N2O emissions]: ../../downloads/5yravg_20111219_pure_emis.nc
[combine_EDGAR_and_EPIC_emissions.r]: ../../src/HEAD/combine_EDGAR_and_EPIC_emissions.r?at=master
[AQMEII-NA combined EDGAR and EPIC emissions]: ../../downloads/5yravg_20111219_pure_emis_both.nc
[make_hourlies.ncl]: ../../src/HEAD/make_hourlies.ncl?at=master
[AQMEII-NA combined EDGAR and EPIC emissions in CMAQ format]: ../../downloads/emis_mole_N2O_2008_12US1_cmaq_cb05_soa_2008ab_08c.ncf.gz
[check_conservation.ncl]: ../../src/HEAD/check_conservation.ncl?at=master

# TODOs

1. Retest with newest [`regrid_utils`][regrid_utils]! Currently, [`repo_diff.sh`][regrid_utils/repo_diff.sh] shows the following local `diff`s:
    * `IOAPI.ncl`
    * `string.ncl`
    * `summarize.ncl`
1. Move all these TODOs to [issue tracker][AQMEII_ag_soil issues].
1. `*.sh`: use bash booleans à la [`N2O_integration_driver.sh`][AQMEII-NA_N2O_integration/N2O_integration_driver.sh].
1. Handle {name, path to} VERDI in [`bash_utilities.sh`][bash_utilities.sh].
1. Generate plot of EPIC-EDGAR (i.e., show where EDGAR < EPIC, EDGAR > EPIC).
1. Add to README, wiki: links to overviews of BELD, EPIC.
1. Add to wiki: Simulation-of-N2O-Production-and-Transport-in-the-US-Cornbelt-Compared-to-Tower-Measurements#wiki-input-processing-EPIC-0509 (I have input processing instructions for other EIs, but not EPIC).
1. Move explicit constants `BELD_CSV_to_RDS.r` -> `AQMEII_driver.sh`.
1. Create common project for `regrid_resources` à la [`regrid_utils`][regrid_utils], so I don't hafta hunt down which resource is in which project.
1. (this project only?) Use the EPIC input as template.
1. All regrids: how to nudge off/onshore as required? e.g., soil or burning emissions should never be offshore, marine emissions should never be onshore.
1. All regrid maps: add Caribbean islands (esp Bahamas! for offshore burning), Canadian provinces, Mexican states.
1. Complain to ncl-talk about NCL "unsupported extensions," e.g., .`ncf` and `<null/>` (e.g., MCIP output).
1. Determine why `<-` assignment is occasionally required in calls to `visualize.*(...)`.
1. Fully document platform versions (e.g., linux, compilers, bash, NCL, R).
1. Test on
    * tlrPanP5 (which now has R package=ncdf4, but readAsciiTable of input .txt's is very slow compared to terrae)
    * HPCC (once problem with ncdf4 on amad1 is debugged: in process with JOB and KMF)

[AQMEII_ag_soil issues]: ../../issues
[regrid_utils]: https://bitbucket.org/epa_n2o_project_team/regrid_utils
[regrid_utils/repo_diff.sh]: https://bitbucket.org/epa_n2o_project_team/regrid_utils/src/HEAD/repo_diff.sh?at=master
[AQMEII-NA_N2O_integration/N2O_integration_driver.sh]: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o_integration/src/HEAD/N2O_integration_driver.sh?at=master
